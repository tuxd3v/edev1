# edev1
---

<img src="https://gitea.devuan.org/tuxd3v/edev1/raw/commit/d6481b2157eed3e12df9aee34d1e590f14c6533f/docs/dev-arm256.png"/>

Embedded Devuan Builder

#### Table of contents
* [Requirements](#requirements)
* [Characteristics](#characteristics)
* [ToolChains](#toolchains)
* [BootLoaders](#bootloaders)
* [Kernels](#kernels)
* [Builds](#builds)
* [Board-Configurations](#board-configurations)
* [Edev1-UML](#edev1-uml)
* [Credits](#credits)

## Requirements:
----
 * Lua Dependencies
   ```lua
   apt-get install lua5.3 lua5.3-dev
   update-alternatives --install /usr/bin/lua lua /usr/bin/lua5.3 1
   update-alternatives --install /usr/bin/luac luac /usr/bin/luac5.3 1
   ```
   * lnewt dependency for the UI.
     ```lua
     apt-get install luarocks git libnewt-dev gcc make
     luarocks build https://raw.githubusercontent.com/tuxd3v/lnewt/master/lnewt-master-0.rockspec
     ```
 * Edev1 Dependencies( maybe something more is needed.. ) 
   ```lua
   apt-get install u-boot-tools swig python-dev python3-dev flex bison make devscripts build-essential libssl-dev bc quilt git qemu-user-static binfmt-support debootstrap
   ```

### Characteristics:
----
Embedded {OS rootfs, Kernel, BootLoader } Builder

relative path | description
------------ | -------------
./reference   | Place were the { code, configurations, patchs } resides
./ToolChains  | Place were ToolChains are installed
./BootLoaders | Place were BootLoaders are installed
./Kernels     | Place were Kernels are installed
./Graphics    | Place were Graphics code stacks are installed( not yet Active )
./Builds      | Place were code built end

## ToolChains:
----
 The module that describes toolchains, are 'reference/tools/**toolchains.lua**'.

 Edev1 assumes that you are crosscompiling from x86/amd64 to a guest arch..
 Crosscompilers can be automatically downloaded, from the menu..

 To select host x86 or amd64 crosscompiler packages
 You need to select the correct host arch for you, in 'reference/tools/**toolchains.lua**' module.
 There are presently 3 types of Crosscompilers for table 'arm':
 * arm32( <= armv6 )
   * baremetal0( used to compile PowerManagement code for some microcontrollers( like cortex-m0 ) builtin in some SoC's( RK3399 is one of this SoC's ).. )
   * baremetal( used to compile bootloaders )
   * linuxgnu( used to compile kernels )
 * aarch32( armv7 )
   * baremetal( used to compile bootloaders )
   * linuxgnu( used to compile kernels )
 * aarch64( >= armv8 )
   * baremetal( used to compile bootloaders )
   * linuxgnu( used to compile kernels )

 exe: So let's say your sbc **target is armv7**, and you are on **amd64 host**, you need to go to 'reference/tools/**toolchains.lua**' module,
 Comment the i686 packages, and uncomment the amd64 ones in tables arm.aarch32( for baremetal and kernel compilation, both.. )..
 You need to do that, for each diferent target arch you want to compile code for..

## BootLoaders:
----
 The module that describes bootloaders, are 'reference/tools/**bootloaders.lua**'.

 Currently only Uboot is suported as bootloader.Trusted Execution Environments{ ARM Trusted Firmware, OPTEE }, have its description created, but only ARM Trusted Firmware is working correctly.
 So for the moment, only uboot/ARM Trusted Firmware is automated to download, install, and compile processes..

## Kernels:
----
 The module that describes kernels, are 'reference/tools/**kernels.lua**'.

 There are 2 very well defined kernel versions:
 * lts - Long Term Support
 * current - Current stable version
 * mainline - This option exists, so that you can compile a mainline version, **but the code download( and install in correct place ), is manual**, also the folder name needs to be '$PWD/Kernels/**linux-mainline**', for compilation process to succced.

## Builds:
----
 Were is expected that your compiled code lands..

## Board Configurations:
----
 The Boards configuration modules are present at '**reference/hw/boards/**'.

 They describe the hardware, patchs for software( kernel| bootloader|ArmTrusted Firmware ),toolchains to use, current kernel to build for,
 OS configs that should be put inplace, etc..
 Each board has a config module..

 Some that are working corectly are:
 * 'reference/hw/boards/**OLime2.lua**'
 * 'reference/hw/boards/**OnePlus.lua**'
 * 'reference/hw/boards/**RockPi4A.lua**'
 * 'reference/hw/boards/**RPro64.lua**'
 * 'reference/hw/boards/**RPi1.lua**'
 * 'reference/hw/boards/**nPiR2S.lua**'
 * 'reference/hw/boards/**m2Zero.lua**'
 * 'reference/hw/boards/**neoAir.lua**'

 Other Modules that we need to have in account are:
 * 'reference/hw/**boards.lua**'
 * 'reference/hw/**cpus.lua**'

## Edev1 UML:
----
 ![A preview of a initial UML..](https://gitea.devuan.org/tuxd3v/edev1/src/branch/master/docs/edev1.png)

### Credits
----
Edev1       : tuxd3v

