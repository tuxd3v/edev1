local bootstrap	= {
	os = require "os",
	bootStrap_firstStage	= function( self, keyring, variant, exclude, include, arch, release, execdir, apt_server )
		cmd = "/usr/sbin/debootstrap --keyring " .. keyring .. " --variant=" .. variant .. " --exclude=\'" .. exclude .. "\' --include=\'"  .. include .. "\' --arch=" .. arch .. " --foreign --verbose " .. release .. " " .. execdir .. " " .. apt_server
		print( "\27[1;35m--- DebootStrap: 1st_debootstrap_Stage Starting ..\27[0m" )
		print( "\27[1;35m--  \27[0m" .. cmd )
		return self.os.execute( cmd )
	end,
	-- Make a backup of 1st CrossDebootstrap Stage to be used by 2nd stage..later..
	backup_Stage	= function( self, backup_dir, compressed_file )
		cmd = "tar -C " .. backup_dir .. " -czf " .. compressed_file .. " ."
		print( "\27[1;35m--  \27[0m" .. cmd )
		return self.os.execute( cmd )
	end,
	-- Make a backup of 1st CrossDebootstrap Stage to be used by 2nd stage..later..
	restore_Stage	= function( self, compressed_file, restore_dir )
		cmd = "tar  -C " .. restore_dir .. " -xvf " .. compressed_file
		print( "\27[1;35m--  \27[0m" .. cmd )
		return self.os.execute( cmd )
	end
}
return bootstrap