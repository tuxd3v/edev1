-- OS base functionalities..
local base	= {
	os		= require "os",
	io		= require "io",
	-- check if file or directory exists
	file_exists		= function( self, file  )
		return self.os.rename( file, file )
	end,
	-- Install Emulator
	install_emulator	= function( self, emulator, directory )
		return self.os.execute( "install -m 755 -o root -g root " .. emulator .. " " .. directory .. "/" .. emulator )
	end,
	-- Install Package Manager Keyring..
	install_keyring		= function(self, keyring_file, install_dir )
		return self.os.execute( "mkdir -p " .. install_dir .. "/usr/share/keyrings && install --owner=root --mode=644 /usr/share/keyrings/devuan-archive-keyring.gpg " .. install_dir .. "/usr/share/keyrings/devuan-archive-keyring.gpg" )
	end,
	-- Execute a 'cmd' command in chroot 'dest' environment
	chRoot		= function(self, dest, cmd )
		print( "\27[1;35m--- chRoot: Executing \'" .. cmd .. "\' \27[0m" )
		--local EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "/bin/dash -c \'mkdir -pv --mode=555 " .. dest .. "/proc; mkdir -pv --mode=555 " .. dest .. "/sys; mkdir -pv --mode=755 " .. dest .. "/dev; mkdir -pv --mode=755 " .. dest .. "/dev/pts\'" )
		local EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "/bin/bash -c \'mkdir -pv --mode=555 " .. dest .. "/{proc,sys};mkdir -pv --mode=755 " .. dest .. "/dev{,/pts}\'" )
		if ( EXIT_CODE == true )
		then
			EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = self.os.execute(
								--"mount -t proc proc " .. dest .. "/proc && " ..
								"/bin/dash -c \'mount -o bind /proc " .. dest .. "/proc && " ..
								"mount -o bind /sys " .. dest .. "/sys && " ..
								"mount -o bind /dev " .. dest .. "/dev && " ..
								"mount -o bind /dev/pts " .. dest .. "/dev/pts && " ..
								"chroot " .. dest .. " " .. cmd .. " && exit 0||exit 1\'" )
			if ( EXIT_CODE == true )
			then
				print( "\27[1;35m--  \27[0mchRoot: executed \27[1;32mSUCCESSFULLY\27[0m!!" )
			else
				print( "\27[1;31m--  \27[0mchRoot: execution \27[1;31mFAILLED\27[0m!!" )
			end
			self.os.execute( "/bin/dash -c \'sleep 2 && umount " .. dest .. "/proc;umount " .. dest .. "/sys;umount " .. dest .. "/dev/pts;sleep 2 && umount " .. dest .. "/dev;exit 0\'" )
		else
			print( "\27[1;31m--  \27[0mchRoot: problems with chroot mount points.. \27[1;31mFAILLED\27[0m!!" )
		end
		return EXIT_SIGNAL
	end,
	-- Remove directory
	rmdir		= function (self, dir )
		return self.os.execute( "rm -rf " .. dir )
	end,
	-- Create directory
	mkdir		= function (self, dir )
		return self.os.execute( "mkdir -pv " .. dir )
	end,
	--- Create tmpdir
	mktmpdir	= function( self )
			local tmpdir = nil
			local handle = self.io.popen( "mktemp -d | tr -d '\\n'" )
			if ( handle ~= nil )
			then
				tmpdir = handle:read( "*a" )
				handle:close()
			end
			return tmpdir
	end,
	-- Get Processor cores number
	getNproc	= function ( self )
			local nproc = nil
			local EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = self.os.execute( "nproc 1> /dev/null 2>&1 | tr -d '\\n';" )
			if ( EXIT_CODE == true )
			then
				local handle = self.io.popen("nproc | tr -d '\\n';" )
				nproc = tonumber( handle:read("*a") )
				handle:close()
			end
			return nproc
	end,
	-- Get PWD
	getPWD		= function( self )
			local pwd = nil
			local handle = self.io.popen("echo $PWD | tr -d '\\n';" )
			if ( handle ~= nil )
			then
				pwd = handle:read("*a")
				handle:close()
				return pwd
			else
				return handle
			end
			-- with sudo runs badly, don't try what is bellow..
			--return os.getenv("PWD")
	end,
	-- remove a file or empty directory
	rmfile		= function ( self, file )
		return self.os.remove( file )
	end,
	-- execute a shell command
	shell		= function ( self, cmd )
		return self.os.execute( cmd )
	end,
	-- execute a shell command and get result back as astring
	mshell		= function ( self, cmd )
			local option
			local handle = self.io.popen( cmd )
			if( handle ~= nil )
			then
				option = handle:read("*a")
				option = tonumber( option )
				handle:close()
				return option
			end
			return handle
	end
}
return base
