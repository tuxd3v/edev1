-- Devuan OS Related Module..
local devuan	= {
		--io		= require "os.base.io",
		osbase		= require "reference.os.base",
		-- Require CrossDebBootStrap Module
		crossdebootstrap = require "reference.os.crossdebootstrap",
		rootpath	= "Devuan",
		name		= "devuan",
		release		= "beowulf",
		osrole		= "base",
		hostname	= "devuarm",
		-- Primary Aplication user ..
		devuser		= "devuan",

		--- DebootStraping info
		--
		apt_server	= "http://deb.devuan.org/merged",
		variant		= "minbase",
		exclude		= "systemd,systemd-sysv,dialog,libsystemd0,openssh-server",
		-- 'debian-archive-keyring' I am including mawk intead of gawk..
		include		= "apt-transport-https,apt-utils,apt-file,ca-certificates,devuan-keyring,sysvinit,sysvinit-core,sysvinit-utils,lsb-release,parted,dbus,alsa-utils,rng-tools,eudev,locales,util-linux-locales,vim-tiny,sudo,openssh-client,mawk,whiptail,device-tree-compiler,u-boot-tools,console-setup",
		arch		= "UNKNOWN",
		arch_emulator	= {
				-- Calling 'ARM' to aarch32 is wrong!!
				-- Previous archs( 'ARM32' ), is <= armv6..
				-- aarch32 is armv7( big + litle endian, or bi-endian )
				-- So I decided to call aarch32( armv7l ) simply 'ARMHF'..
				-- MIPS32 here we are refering to MIPS32r5( SMA - 128 bits SIMD ), needs toolchain >=7.3, better >= 9.x
				armel	= "/usr/bin/qemu-arm-static",
				armhf	= "/usr/bin/qemu-arm-static",
				arm64	= "/usr/bin/qemu-aarch64-static",
				mips32	= "/usr/bin/qemu-mipsel-static"
		},
		debootstrapfile	= {
				armel	= "base-armel.tar.gz",
				armhf	= "base-armhf.tar.gz",
				arm64	= "base-arm64.tar.gz",
				mips32	= "base-mipsel.tar.gz"
		},
		--- tmpdir for 1st Crossdebootstrap Stage
		--
		tmpdir		= "UNKNOWN",
		-- Cross DebootStrap
		BootStrap = function( self, pwd, board )
			return self.crossdebootstrap:BootStrap( pwd, self.name, self.release, self.apt_server, self.exclude, self.include, self.arch, self.arch_emulator[ self.arch ], self.debootstrapfile[ self.arch ], self.tmpdir, board )
		end,
		-- Build Final Devuan rootFs
		buildRootFS = function( self, pwd, quemu_arch, RootPath )
			-- To verify binfmt-support, if its configured for the target guest arch bellow..
			-- We use qemu syscalls emulation, which is faster than qemu virtualization..
			local arch = nil
			arch = quemu_arch

			-- Chose arch, for emulation, and bootstrap, also for chroot..
			-- TODO: implement several more, and try it more dynamic..
			-- TODO: implement them also in devuan.lua

			-- Check Requirements for DevBootStraping..
			EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = self.osbase:shell( "sudo apt-get --yes --quiet install binfmt-support qemu-user-static" )
			if( EXIT_CODE == true )
			then
				-- Check if binfmt has aarch64 enabled..
				EXIT_SIGNAL = self.osbase:mshell( "sudo update-binfmts --display|grep -EA 7 \"" .. arch .. ".*enabled\" --color 1> /dev/null 2>&1;echo $? | tr -d '\\n';" )
				if( EXIT_SIGNAL == 0 )
				then
					-- Create tmpdir, to hold 1st-Stage and Second stage, until backup..
					self.tmpdir = self.osbase:mktmpdir()
					-- Create rootfs folder inside tmpdir...easier to compress to and from..
					self.osbase:mkdir( self.tmpdir .. "/rootfs" )
					self.tmpdir = self.tmpdir .. "/rootfs"
					
					-- Attempt bootStrap( 1stStage + 2ndStage )
					if ( self:BootStrap( pwd, RootPath ) ~= 0 )
					then
						-- Problems bootStraping
						-- Remove tmp dir
						-- Exit 1
						self.osbase:rmdir( string.gsub( self.tmpdir, "/rootfs", "" ) )
						self.osbase.os.exit( 1 )
					else
						-- After bootstrap, Continue not in tmpdir, but in rootfs dir..
						-- This call again to BootStrap will force it to extract second stage, to a folder.. what will be known as rootfs..
						self:BootStrap( pwd, RootPath )
					end
				end
			end
		end,
		config = {
			basics = {
				osbase		= require "reference.os.base",
				--- Configurations for each type of OS( Basics( commun to server and Desktop.. ), Server, Desktop )
				--- Basic Type Configs
				-- prevent services from starting during rootfs creation..
				preventStart_Services	= function ( self, rootfs )
						local policy_rc = {
							"#! /bin/sh\n",
							"exit 101\n"
						}
						local handler = io.open( rootfs .. "/usr/sbin/policy-rc.d", "w+" )
						for k,v in ipairs( policy_rc )
						do
							handler:write( v )
						end
						handler:flush()
						handler:close()
						self.osbase:shell( "chmod a+x " .. rootfs .. "/usr/sbin/policy-rc.d" )
						print( rootfs .. "/usr/sbin/policy-rc.d" )
				end,
				-- Enable services to start after rootfs creation..
				enableStart_Services	= function ( self, rootfs )
						self.osbase:shell( "rm -f " .. rootfs .. "/usr/sbin/policy-rc.d" )
						print( rootfs .. "/usr/sbin/policy-rc.d" )
				end,
				-- Load Zram module on boot
				configure_Modules	= function ( self, rootfs, modules )
					if( modules ~= nil )
					then
						local handler = io.open( rootfs .. "/etc/modules", "w+" )
						for k,v in ipairs( modules )
						do
							handler:write( v )
						end
						handler:flush()
						handler:close()
						print( rootfs .. "/etc/modules" )
					end
				end,
				-- Add Zram as Swap and /tmp
				configure_Zram	= function ( self, rootfs, zram_rules )
					if( zram_rules ~= nil )
					then
						local handler = io.open( rootfs .. "/etc/udev/rules.d/10-zram.rules", "w+" )
						for k,v in ipairs( zram_rules )
						do
							handler:write( v )
						end
						handler:flush()
						handler:close()
						print( rootfs .. "/etc/udev/rules.d/10-zram.rules" )
					end
				end,
				-- Add Persistent NET Rules
				configure_ethx	= function ( self, rootfs, net_rules )
					if( net_rules ~= nil )
					then
						local handler = io.open( rootfs .. "/etc/udev/rules.d/70-persistent-net.rules", "w+" )
						for k,v in ipairs( net_rules )
						do
							handler:write( v )
						end
						handler:flush()
						handler:close()
						print( rootfs .. "/etc/udev/rules.d/70-persistent-net.rules.rules" )
					end
				end,
				-- Add apt-Sources..
				addApt_Sources	= function( self, rootfs, release )
						local apt_sources = {
							"\n## package repositories\n",
							"deb http://deb.devuan.org/merged " .. release .. " main contrib non-free\n",
							"deb http://deb.devuan.org/merged " .. release .. "-updates main contrib non-free\n",
							"deb http://deb.devuan.org/merged " .. release .. "-security main contrib non-free\n",
							"#deb http://deb.devuan.org/merged " .. release .. "-backports main contrib non-free\n",
							"\n",
							"## source repositories\n",
							"#deb-src http://deb.devuan.org/merged " .. release .. " main contrib non-free\n",
							"#deb-src http://deb.devuan.org/merged " .. release .. "-updates main contrib non-free\n",
							"#deb-src http://deb.devuan.org/merged " .. release .. "-security main contrib non-free\n",
							"#deb-src http://deb.devuan.org/merged " .. release .. "-backports main contrib non-free\n"
						}
						local handler = io.open( rootfs .. "/etc/apt/sources.list", "w+")
						for k,v in ipairs( apt_sources )
						do
							handler:write( v )
						end
						handler:flush()
						handler:close()
						self.osbase:shell( "chmod 644 " .. rootfs .. "/etc/apt/sources.list" )
						print( rootfs .. "/etc/apt/sources.list" )
				end,
				-- Exclude recomended packages..
				excluderec_pkgs	= function( self, rootfs )
						exclude_pkgs = {
							"APT::Install-Recommends \"no\";\n",
							"APT::AutoRemove::RecommendsImportant \"false\";\n"
						}
						local handler = io.open( rootfs .. "/etc/apt/apt.conf.d/01lean", "w+" )
						handler:write( exclude_pkgs[ 1 ] )
						handler:write( exclude_pkgs[ 2 ] )
						handler:flush()
						handler:close()
						self.osbase:shell( "chmod 644 " .. rootfs .. "/etc/apt/apt.conf.d/01lean" )
						print( rootfs .. "/etc/apt/apt.conf.d/01lean" )
				end,
				-- Remove proposed, and/or default sources
				rmdefault_prop_srcs	= function( self, rootfs )
						local default_prop_srcs = {
							rootfs .. "/etc/apt/sources.list.d/devuan.list",
							rootfs .. "/etc/apt/sources.list.d/proposed.list"
						}
						local EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL
						for k,default_prop_src in ipairs( default_prop_srcs )
						do
							EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = self.osbase:file_exists( default_prop_src )
							if ( EXIT_CODE == true )
							then
								self.osbase:rmfile( default_prop_srcs[ 1 ] )
								print( rootfs .. "/etc/apt/sources.list.d/devuan.list" )
							end
						end
				end,
				configure_basics	= function ( self, rootfs, user )
						extradeps	= "network-manager"
						addppacmd	= ""
						disptoolcmd	= ""
						basics = {
							"#! /usr/bin/env dash\n",
							"export DEBIAN_FRONTEND=noninteractive\n",
							"export DEVUAN_FRONTEND=noninteractive\n",
							"locale-gen en_GB.UTF-8\n",
							"locale-gen pt_PT.UTF-8\n",
							"\n",
							"# Re-Install the Devuan keyring( above was copied by hand.. ) so packages can be authenticated from this point onwards.\n",
							"apt-get -y install --reinstall devuan-keyring --allow-unauthenticated\n",
							"\n",
							"# Depending on your setup you may not care for internationalisation in debconf so this can be removed in that case.\n",
							"dpkg --purge debconf-i18n\n",
							"\n",
							"# As you only need one gcc-base package you can remove gcc-4.8-base in favour of gcc-4.9-base\n",
							"dpkg --purge gcc-4.8-base\n",
							"apt-get -y update\n",
							"\n",
							"# Install and configure the debconf interface\n",
							"# A good choice for minimalism purposes is the default whiptail interface on Devuan..\n",
							"apt-get -y remove dialog\n",
							"apt-get -y install whiptail\n",
							"dpkg-reconfigure debconf\n",
							"\n",
							"# Adding the network components\n",
							"#Install the minimum required packages to get networking.\n",
							"apt-get -y install netbase net-tools ifupdown\n",
							"\n",
							"#Some optional network tools may be wanted..\n",
							"apt-get -y install inetutils-ping dnsutils\n",
							"\n",
							"# Optional extra Packages\n",
							"apt-get -y install psmisc pciutils rsyslog less curl wget bc\n",
							"\n",
							"# This is a suggestion for packages you may want that will help you post-install if needed.\n",
							"apt-get -y install man manpages lynx irssi\n",
							"\n",
							"# If shell access over the network is needed .. you need to enable server type role..\n",
							"#  openssh-blacklist not found !!! Why?? .. TODO: needds investigation..\n",
							"apt-get -y install openssh-client\n",
							"\n",
							"# " .. extradeps .. "\n",
							"#apt-get -y install dosfstools iw rfkill wpasupplicant usbutils alsa-utils\n",
							"#apt-get -y install libsysfs-dev\n",
							"\n",
							"#" .. addppacmd .. "\n",
							"apt-get -y update\n",
							"#" .. disptoolcmd .. "\n",
							"\n",
							"\n",
							"# Configure Users/Groups/folders..\n",
							"adduser --gecos " .. user .. " --disabled-login " .. user .. " --uid 1000\n",
							"adduser --gecos root --disabled-login root --uid 0\n",
							"chown -R 1000:1000 /home/" .. user .. "\n",
							"echo \"" .. user .. ":" .. user .. "\" | chpasswd\n",
							-- fsmithred proposed root passwd to be 'toor', to be aligned with live-isos..
							"echo \"root:toor\" | chpasswd\n",
							"chown root:root /usr/bin/sudo\n",
							"chmod 4755 /usr/bin/sudo\n",
							"chown " .. user .. ":" .. user .. " /home/" .. user .. "/.*\n",
							"chown root:root /home\n",
							"usermod -a -G sudo,adm,input,video,plugdev " .. user .. "\n",
							"\n",
							"# Clean cache and remove unneeded packages..\n",
							"apt-get -y autoremove\n",
							"apt-get clean\n"
						}
						-- write content to file..
						local handler = io.open( rootfs .. "/second-phase.basics", "w+")
						-- better to sustitute this, for a iterator over the table, in this way its dynamic, werever the size of the table..
						--for i = 1, 64, 1
						for key,value in ipairs( basics )
						do
							--handler:write( basics[ i ] )
							handler:write( value )
						end
						handler:flush()
						handler:close()
						self.osbase:shell( "chmod +x " .. rootfs .. "/second-phase.basics" )
						print( rootfs .. "/second-phase.basics" )
				end,
				-- Configure Network
				configure_network	= function( self, rootfs, interfaces )
					if( interfaces ~= nil )
					then
						local handler = io.open( rootfs .. "/etc/network/interfaces", "w")
						for k,v in ipairs( interfaces )
						do
							handler:write( v )
						end
						handler:flush()
						handler:close()
						self.osbase:shell( "chmod 644 " .. rootfs .. "/etc/network/interfaces" )
						print( rootfs .. "/etc/network/interfaces" )
					end
				end,
				-- Configure Machine Hostname
				configure_hostname	= function( self, rootfs, hostname )
					local handler = io.open( rootfs .. "/etc/hostname", "w")
					handler:write( hostname .. "\n" )
					handler:flush()
					handler:close()
					self.osbase:shell( "chmod 644 " .. rootfs .. "/etc/hostname" )
					print( rootfs .. "/etc/hostname" )
				end,
				-- Configure LocalHosts..
				configure_localhosts	= function( self, rootfs, hostname )
					local hosts ={
						"127.0.0.1 localhost " .. hostname .. "\n",
						"127.0.1.1 " .. hostname .. "\n",
						"\n",
						"# The following lines are desirable for IPv6 capable hosts\n",
						"::1     localhost ip6-localhost ip6-loopback\n",
						"fe00::0 ip6-localnet\n",
						"ff00::0 ip6-mcastprefix\n",
						"ff02::1 ip6-allnodes\n",
						"ff02::2 ip6-allrouters\n"
					}
					local handler = io.open( rootfs .. "/etc/hosts", "w")
					for k,v in ipairs( hosts )
					do
						handler:write( v )
					end
					handler:flush()
					handler:close()
					self.osbase:shell( "chmod 644 " .. rootfs .. "/etc/hosts" )
					print( rootfs .. "/etc/hosts" )
				end,
				-- Point resolver to /run/resolvconf/resolv.conf, so that network can work, on target..
				configure_resolver	= function( self, osrole, rootfs )
					-- if you need a dhcp-client,
					-- then you need to point resolv.conf to '/run/resolvconf/resolv.conf'
					if( osrole ~= "basics" )
					then
						self.osbase:shell( "rm -f " .. rootfs .. "/etc/resolv.conf" )
						self.osbase:chRoot( rootfs, "ln -s /run/resolvconf/resolv.conf /etc/resolv.conf" )
					end
					print( rootfs .. "/etc/resolv.conf" )
				end,
				-- configure target fstab
				configure_fstab		= function( self, rootfs, fstab )
					if( fstab ~= nil )
					then
						local handler = io.open( rootfs .. "/etc/fstab", "w")
						for k,v in ipairs( fstab )
						do
							handler:write( v )
						end
						handler:flush()
						handler:close()
						self.osbase:shell( "chmod 644 " .. rootfs .. "/etc/fstab" )
						print( rootfs .. "/etc/fstab" )
					end
				end,
				-- configure target inittab
				configure_inittab	= function( self, rootfs, inittab )
					if( inittab ~= nil )
					then
						local handler = io.open( rootfs .. "/etc/inittab", "a+")
						for k,v in ipairs( inittab )
						do
							handler:write( v )
						end
						handler:flush()
						handler:close()
						self.osbase:shell( "chmod 644 " .. rootfs .. "/etc/inittab" )
						print( rootfs .. "/etc/inittab" )
					end
				end,
				--[[configure_motd		= function ( self, rootfs )
					local motd = {
						'echo \"\" >> /etc/motd',
						'echo -en \"\033[1;35m ____  ____\033[0m\033[1;31m __     ___   _   _    \033[0m\033[1;35m__   _  \033[0m\n\" >> /etc/motd',
						'echo -en \"\033[1;35m|  _ \| ____\033[0m\033[1;31m\ \   / / | | | / \  \033[0m\033[1;35m|  \ | |\033[0m\033[0;32m __ _ _ __ _ __ ___  \033[0m\n\" >> /etc/motd',
						'echo -en \"\033[1;35m| | | |  _|\033[0m\033[1;31m  \ \ / /| | | |/ _ \ \033[0m\033[1;35m|   \| |\033[0m\033[0;32m/ _\` | '__| '_ \` _ \ \033[0m\n\" >> /etc/motd',
						'echo -en \"\033[1;35m| |_| | |___\033[0m\033[1;31m  \ V / | |_| / ___ \ \b\033[0m\033[1;35m| |\   \033[0m\033[0;32m| (_| | |  | | | | | |\033[0m\n\" >> /etc/motd',
						'echo -en \"\033[1;35m|____/|_____|\033[0m\033[1;31m  \_/\033[0m   \033[1;31m\___/_/\033[0m   \033[1;31m\_\ \b\033[0m\033[1;35m_| \__|\033[0m\033[0;32m\__,_|_|  |_| |_| |_|\033[0m\n\" >> /etc/motd',
						'echo "" >> /etc/motd'
					}
					for k,line in ipairs( motd )
					do
						self.osbase:shell( line )
					end
					print( rootfs .. "/etc/motd" )
				end,]]
				-- Clean environment from basics execution stage
				clean_basics	= function( self, rootfs )
					-- remove now basics phase config file..
					self.osbase:shell( "rm -f " .. rootfs .. "/second-phase.basics" )
				end,
				-- execute all functions for basic second-phase target..
				execute_basics	= function( self, rootfs, hostname, release, user, fstab, inittab, modules, zram_rules, net_rules, interfaces )
					self.preventStart_Services( self, rootfs )
					self.configure_Modules( self, rootfs, modules )
					self.configure_Zram( self, rootfs, zram_rules )
					self.configure_ethx( self, rootfs, net_rules )
					self.addApt_Sources( self, rootfs, release )
					self.excluderec_pkgs( self, rootfs )
					self.rmdefault_prop_srcs ( self, rootfs )
					self.configure_basics( self, rootfs, user )
					-- execute in Chroot basics configs
					self.osbase:chRoot( rootfs, "/second-phase.basics" )
					-- remove now basics phase config file..
					--self.osbase:shell( "rm -f" .. rootfs .. "/second-phase.basics" )
					self.configure_network( self, rootfs, interfaces )
					self.configure_hostname( self, rootfs, hostname )
					self.configure_localhosts( self, rootfs, hostname )
					---- remove first target '/etc/resolv.conf', to fix target resolver with a symbolic link, below
					--self.osbase:shell( "rm -f " .. rootfs .. "/etc/resolv.conf" )
					--self.configure_resolver( self, rootfs )
					self.configure_fstab( self, rootfs, fstab )
					self.configure_inittab( self, rootfs, inittab )
					-- Configure MOTD devual logo
					--self:configure_motd( rootfs )
					-- clean now environment of BASICS execution
					self:clean_basics( rootfs )
					-- will continue, in OS type 'server' or 'Desktop'

				end,
			},
			server = {
				osbase		= require "reference.os.base",
				--- Server Type
				-- Configurations for Server type OS Role
				configure_server = function( self, rootfs, hostname )
					local server = {
						"#! /usr/bin/env dash\n",
						"# Save HOST name\n",
						"ORIGHOSTNAME=$(cat /proc/sys/kernel/hostname )\n",
						"apt-get -y install git rsync xz-utils gcc autoconf libtool g++ libmpfr4 libgomp1 pkg-config xutils-dev\n",
						"\n",
						"#Some optional network tools may be wanted..\n",
						"apt-get -y install isc-dhcp-client inetutils-ping ntp dnsutils\n",
						"\n",
						"# Post-Install Cleanup Procedures..\n",
						"apt-get -y purge systemd systemd-shim libsystemd0\n",
						"# Force set GUEST Hostname( for SSH key Generation bellow )..\n",
						"echo \'" .. hostname .. "\' > /proc/sys/kernel/hostname\n",
						"export HOSTNAME=" .. hostname .. "\n",
						"apt-get -y install openssh-server openssh-sftp-server\n",
						"# Force set HOST Hostname BACK to Original Name..\n",
						"echo \"$ORIGHOSTNAME\" > /proc/sys/kernel/hostname\n",
						"export HOSTNAME=$ORIGHOSTNAME\n",
						"apt-get -y autoremove --purge\n",
						"apt-get -y autoclean\n",
						"apt-get clean\n"
					}
					-- write content to file..
					local handler = io.open( rootfs .. "/type-phase.server", "w+")
					for k,v in ipairs( server )
					do
						handler:write( v )
					end
					handler:flush()
					handler:close()
					self.osbase:shell( "chmod +x " .. rootfs .. "/type-phase.server" )
					print( rootfs .. "/type-phase.server" )
				end,
				-- Clean environment from Server execution stage
				clean_server	= function( self, rootfs )
					-- remove target '/type-phase.server"', to remove config server type file..
					self.osbase:shell( "rm -f " .. rootfs .. "/type-phase.server" )
					-- remove target '/usr/sbin/policy-rc.d', from Basics, preventing services from starting
					self.osbase:shell( "rm -f " .. rootfs .. "/usr/sbin/policy-rc.d" )
					-- remove target '/usr/bin/qemu-aarch64-static' from CrossdebootStrap to emulate programs..
					--self.osbase:shell( "rm -f " .. rootfs .. "/usr/bin/qemu-aarch64-static" )
				end,
				-- execute all functions forServer type-phase target..
				execute_server	= function( self, rootfs, hostname )
					self:configure_server( rootfs, hostname )
					-- execute in Chroot type Server configs
					self.osbase:chRoot( rootfs, "/type-phase.server" )
					-- Clean now from Server type execution
					self:clean_server( rootfs )
					-- End configs..
				end,
			},
			desktop = {
				osbase		= require "reference.os.base",
				--- Desktop Type
				-- Configurations for Desktop type OS Role
				configure_desktop = function( self, rootfs, hostname )
					local desktop = {
						"#! /usr/bin/env dash\n",
						"# Save HOST name\n",
						"ORIGHOSTNAME=$(cat /proc/sys/kernel/hostname )\n",
						"\n",
						"# Network programs, and development\n",
						"apt-get -y install tree hexedit xorriso curl git rsync xz-utils gcc g++ make automake pkg-config autoconf libtool g++ libmpfr6 libgmp10 libmpfr-dev libgmp-dev xutils-dev lua5.3 liblua5.3-0 liblua5.3-dev python3 libpython3-stdlib libfltk1.3 ??libfltk-gl1.3?? libfltk-images1.3 libfltk1.3-dev ??libfltk-cairo1.3 libfltk-forms1.3 fltk1.3-doc?? ??libfltk1.3-compat-headers?? librsvg2-bin ??librsvg2-dev?? jam librsvg2-bin libinotifytools0-dev doxygen  ??libdbus-c++-dev??\n",
						"\n",
						"#echo \"Install Some alternative Linls to default tools..\"\n",
						"update-alternatives --install /usr/bin/lua lua /usr/bin/lua5.3 1\n",
						"update-alternatives --install /usr/bin/luac luac /usr/bin/luac5.3 1\n",
						"update-alternatives --install /usr/bin/python python /usr/bin/python3 1\n",
						"update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-8 1\n",
						"update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-8 1\n",
						"\n",
						"#Install Xorg\n",
						"apt-get install xserver-xorg-core xserver-xorg-input-evdev xserver-xorg-input-libinput xserver-xorg-input-kbd xserver-xorg-input-mouse xfonts-base xfonts-utils xauth\n",
						"# Setup a core of aplications, low memory usage, that will be enough to allow you to start using your new desktop environment.\n",
						"apt-get -y install fluxbox bbmail idesk xterm nedit rox-filer mirage upower xscreensaver\n",
						"#Install theme manager for Fluxbox\n",
						"#cd /root && git clone https://github.com/michaelrice/fluxStyle && cd fluxStyle && ./setup.py install && cd .. && rm -rf fluxstyle\n",
						"\n",
						"# Install Simple LogIn Manager\n",
						"apt-get -y install slim\n",
						"# Set the session manager to 'startluxbox' so that the new desktop can be used later.\n",
						"#echo \"Set the Session manager to 'startluxbox' so that the new desktop can be used later.\"\n",
						"update-alternatives --config x-session-manager\n",
						"\n",
						"# Miscelanious \"extra\" apps for X,and themes\n",
						"apt-get -y install gtk-update-icon-cache\n",
						"\n",
						"# Add Support for auto-mount\n",
						"#apt-get -y install pmount policykit-1\n",
						"apt-get -y install pmount\n",
						"\n",
						"#Some optional network tools needed.\n",
						"# Force set GUEST Hostname( for SSH key Generation bellow )..\n",
						"echo \'" .. hostname .. "\' > /proc/sys/kernel/hostname\n",
						"export HOSTNAME=" .. hostname .. "\n",
						"apt-get -y install openssh-server openssh-sftp-server\n",
						"# Force set HOST Hostname BACK to Original Name..\n",
						"echo \"$ORIGHOSTNAME\" > /proc/sys/kernel/hostname\n",
						"export HOSTNAME=$ORIGHOSTNAME\n",
						"\n",
						"apt-get -y install isc-dhcp-client inetutils-ping ntpdate dnsutils\n",
						"\n",
						"# stop network-manabger processes, so that Ceni could configure your network..\n",
						"/etc/init.d/network-manager stop\n",
						"\n",
						"# Make sure network-manager is not installed\n",
						"update-rc.d -f network-manager remove\n",
						"\n",
						"# Install a Network Manager Application for your network\n",
						"apt-get -y install Ceni\n",
						"echo \"Install Some alternative Links to default tools..\"\n",
						"update-alternatives --install /usr/sbin/ceni ceni /usr/sbin/Ceni 1\n",
						"# Will be used Ceni intead of connman..\n",
						"\n",
						"# Post-Install Cleanup Procedures..\n",
						"#apt-get -y purge systemd systemd-shim libsystemd0\n",
						"#apt-get -y autoremove --purge\n",
						"#apt-get -y autoclean\n",
						"#apt-get clean\n"
					}
					-- write content to file..
					local handler = io.open( rootfs .. "/type-phase.desktop", "w+")
					for k,v in ipairs( desktop )
					do
						handler:write( v )
					end
					handler:flush()
					handler:close()
					self.osbase:shell( "chmod +x " .. rootfs .. "/type-phase.desktop" )
					print( rootfs .. "/type-phase.desktop" )
				end,
				-- Configure Display Manager
				configure_displaymanager	= function( self, rootfs, user )
					-- We will use slim display manager..
					local slim = {
						"# Path, X server and arguments (if needed)\n",
						"# Note: -xauth $authfile is automatically appended\n",
						"#\n",
						"default_path        /usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games\n",
						"default_xserver     /usr/bin/X11/X\n",
						"xserver_arguments   -nolisten tcp -dpi 96\n",
						"\n",
						"# Commands for halt, login, etc.\n",
						"halt_cmd            /sbin/shutdown -h now\n",
						"reboot_cmd          /sbin/shutdown -r now\n",
						"console_cmd         /usr/bin/xterm -C -fg white -bg black +sb -T \"Console login\" -e /bin/sh -c \"/bin/cat /etc/issue.net; exec /bin/login\"\n",
						"#suspend_cmd        /usr/sbin/suspend\n",
						"\n",
						"# Full path to the xauth binary\n",
						"xauth_path         /usr/bin/X11/xauth\n",
						"\n",
						"# Xauth file for server\n",
						"authfile           /var/run/slim.auth\n",
						"\n",
						"\n",
						"# Activate numlock when slim starts. Valid values: on|off\n",
						"numlock             on\n",
						"\n",
						"# Hide the mouse cursor (note: does not work with some WMs).\n",
						"# Valid values: true|false\n",
						"# hidecursor          false\n",
						"\n",
						"# This command is executed after a succesful login.\n",
						"# you can place the %session and %theme variables\n",
						"# to handle launching of specific commands in .xinitrc\n",
						"# depending of chosen session and slim theme\n",
						"#\n",
						"# NOTE: if your system does not have bash you need\n",
						"# to adjust the command according to your preferred shell,\n",
						"# i.e. for freebsd use:\n",
						"# login_cmd           exec /bin/sh - ~/.xinitrc %session\n",
						"login_cmd           exec /bin/bash -login /etc/X11/Xsession %session\n",
						"\n",
						"# Commands executed when starting and exiting a session.\n",
						"# They can be used for registering a X11 session with\n",
						"# sessreg. You can use the %user variable\n",
						"#\n",
						"#sessionstart_cmd	exec startfluxbox\n",
						"# sessionstop_cmd	some command\n",
						"\n",
						"# Start in daemon mode. Valid values: yes | no\n",
						"# Note that this can be overriden by the command line\n",
						"# options \"-d\" and \"-nodaemon\"\n",
						"# daemon	yes\n",
						"\n",
						"# Set directory that contains the xsessions.\n",
						"# slim reads xsesion from this directory, and be able to select.\n",
						"sessiondir            /usr/share/xsessions/\n",
						"\n",
						"# Executed when pressing F11 (requires scrot)\n",
						"screenshot_cmd      scrot /root/slim.png\n",
						"\n",
						"# welcome message. Available variables: %host, %domain\n",
						"welcome_msg         Welcome to %host\n",
						"\n",
						"# Session message. Prepended to the session name when pressing F1\n",
						"# session_msg         Session: \n",
						"\n",
						"# shutdown / reboot messages\n",
						"shutdown_msg       The system is halting...\n",
						"reboot_msg         The system is rebooting...\n",
						"\n",
						"# default user, leave blank or remove this line\n",
						"# for avoid pre-loading the username.\n",
						"default_user        " .. user .. "\n",
						"\n",
						"# Focus the password field on start when default_user is set\n",
						"# Set to \"yes\" to enable this feature\n",
						"focus_password      yes \n",
						"\n",
						"# Automatically login the default user (without entering\n",
						"# the password. Set to \"yes\" to enable this feature\n",
						"#auto_login          no\n",
						"\n",
						"\n",
						"# current theme, use comma separated list to specify a set to\n",
						"# randomly choose from\n",
						"current_theme       desktop-slim-theme\n",
						"\n",
						"# Lock file\n",
						"lockfile            /var/run/slim.lock\n",
						"\n",
						"# Log file\n",
						"logfile             /var/log/slim.log\n",
						"\n",
					}
					-- write content to file..
					local handler = io.open( rootfs .. "/etc/slim.conf", "w+")
					for k,v in ipairs( slim )
					do
						handler:write( v )
					end
					handler:flush()
					handler:close()
					self.osbase:shell( "chmod 644 " .. rootfs .. "/etc/slim.conf" )
					local slimlock = {
						"\n",
						"# DPM settings on Lock\n",
						"# number of seconds of inactivity before the screen blanks. Default: 60\n",
						"dpms_standby_timeout 6\n",
						"\n",
						"# number of seconds of inactivity before the screen is turned off. Default: 600\n",
						"dpms_off_timeout 600\n",
						"\n",
						"# delay in seconds after an incorrect password is entered. Default: 2\n",
						"#wrong_passwd_timeout\n",
						"\n",
						"# message to display after a failed authentication attempt. Default: Authentication failed\n",
						"#passwd_feedback_msg\n",
						"\n",
						"# message to display after a failed authentication attempt if the CapsLock is on. Default: Authentication failed (CapsLock is on)\n",
						"passwd_feedback_capslock\n",
						"\n",
						"# 1 to show username on themes with single input field; 0 to disable. Default: 1\n",
						"#show_username\n",
						"\n",
						"#1 to show SLiM's welcome message; 0 to disable. Default: 0\n",
						"#show_welcome_msg\n",
						"\n",
						"#1 to disallow virtual terminals switching; 0 to allow. Default: 1\n",
						"#tty_lock\n",
						"\n",
						"# 1 to enable the bell on authentication failure; 0 to disable. Default: 1\n",
						"bell"
					}
					-- write content to file..
					local handler = io.open( rootfs .. "/etc/slimlock.conf", "w+")
					for k,v in ipairs( slimlock )
					do
						handler:write( v )
					end
					handler:flush()
					handler:close()
					self.osbase:shell( "chmod 644 " .. rootfs .. "/etc/slimlock.conf" )
				end,
				-- Configure Automount Media..
				automount_fs		= function( self, rootfs )
					local automount = {
						"cdrom -fstype=iso9660,ro,nodev,nosuid :/dev/cdrom",
						"windows -fstype=auto,async,nodev,nosuid,umask=000 :/dev/sda1",
						"ubuntu -fstype=auto,async,nodev,nosuid,umask=000 :/dev/sda5"
					}
					-- write content to file..
					local handler = io.open( rootfs .. "/etc/autofs/auto.media", "w+")
					for k,v in ipairs( automount )
					do
						handler:write( v )
					end
					handler:flush()
					handler:close()
					self.osbase:shell( "chmod 644 " .. rootfs .. "/etc/autofs/auto.media" )
				end,
				-- Sound Configuration..
				addasound_state		= function( self, rootfs )
					-- Create Directory..
					self.osbase:mkdir( rootfs .. "/var/lib/alsa" )
					-- TODO: Alsa config needed
				end,
				-- add ilynx Text Editor theme
				addeditor_theme	= function( self, rootfs )
					self.osbase:shell( "sudo mkdir -pv " .. rootfs .. "/usr/share/gtksourceview-2.0/styles && sudo cp --preserve /usr/share/gtksourceview-2.0/styles/ilynx.xml " .. rootfs .. "/usr/share/gtksourceview-2.0/styles" )
				end,
				-- add Plataro Desktop theme
				adddesktop_theme	= function( self, rootfs )
					self.osbase:shell( "sudo cp -r --preserve /usr/share/icons/Plataro " .. rootfs .. "/usr/share/icons/" )
					-- execute in Chroot Icons cache creation..
					self.osbase:chRoot( rootfs, "sudo gtk-update-icon-cache -f -t /usr/share/icons/Plataro" )
				end,
				-- Clean environment from Desktop execution stage
				clean_desktop	= function( self, rootfs )
					-- remove target '/type-phase.desktop', to remove config Desktop type file..
					self.osbase:shell( "rm -f " .. rootfs .. "/type-phase.desktop" )
					-- remove target '/usr/sbin/policy-rc.d', from Basics, preventing services from starting
					self.osbase:shell( "rm -f " .. rootfs .. "/usr/sbin/policy-rc.d" )
					-- remove taret '/usr/bin/qemu-aarch64-static' from CrossdebootStrap to emulate programs..
					self.osbase:shell( "rm -f " .. rootfs .. "/usr/bin/qemu-aarch64-static" )
				end,
				-- execute all functions for Desktop second-phase target..
				execute_desktop	= function( self, rootfs, user, hostname )
					self:configure_desktop( rootfs, hostname )
					-- execute in Chroot type Server configs
					self.osbase:chRoot( rootfs, "/type-phase.desktop" )
					--self:configure_displaymanager( rootfs, user )
					self:addasound_state( rootfs )
					self:addeditor_theme( rootfs )
					self:adddesktop_theme( rootfs )
					-- Execute Desktop type Environment cleaning
					self:clean_desktop( rootfs )
					-- End of Configurations
				end
			}
		}
}
return devuan
