
--- crossdebootstrap Module
--  Provides Support for DeBootStraping
--
local crossdebootstrap = {
	-- Require OS related bootstrap functions..
	bootstrap		= require "reference.os.bootstrap",
	-- Require OS base type functions..
	osbase			= require "reference.os.base",
	apt_server		= "UNKNOWN",
	keyring			= "/usr/share/keyrings/devuan-archive-keyring.gpg",
	variant			= "minbase",
	exclude			= "systemd,systemd-sysv,dialog",
	include			= "whiptail",
	arch			= "UNKNOWN",
	arch_emulator		= "UNKNOWN",
	release			= "UNKNOWN",
	debootstrapfiles	= {
		-- First Stage CrossDeBootStrap file( aka unpack )
		firstStage	= "UNKNOWN",
		-- Second Stage CrossDeBootStrap file
		secondStage	= "UNKNOWN",
		-- tmpdir root directory
		tmpdir0		= "UNKNOWN",
		-- tmpdir directory
		tmpdir		= "UNKNOWN",
		-- rootfs root directory
		rootfs0		= "UNKNOWN",
		-- rootfs directory
		rootfs		= "UNKNOWN",
	},
	-- CrossDebootStrap 1st-Stage ( unpack only) + 2nd-Stage
	BootStrap		= function( self, pwd, os, release, apt_server, exclude, include, arch, arch_emulator, debootstrapfile, tmpdir, board )
		self.release				= release
		self.apt_server				= apt_server
		self.exclude				= exclude
		self.include				= include
		self.arch				= arch
		self.arch_emulator			= arch_emulator
		-- BootStrap
		self.debootstrapfiles.firstStage	= pwd .. "/Builds/1st-" .. os .. "-" .. release .. "-" .. debootstrapfile
		self.debootstrapfiles.secondStage	= pwd .. "/Builds/" .. os .. "-" .. release .. "-" .. debootstrapfile
		self.debootstrapfiles.tmpdir0		= string.gsub( tmpdir, "/rootfs", "" )
		self.debootstrapfiles.tmpdir		= tmpdir
		-- RootFS
		self.debootstrapfiles.rootfs0		= string.gsub( self.debootstrapfiles[ "secondStage" ], ".tar.gz", "" )
		self.debootstrapfiles.rootfs0		= string.gsub( self.debootstrapfiles.rootfs0, "Builds/", "Builds/" .. board .. "_" )
		self.debootstrapfiles.rootfs		= self.debootstrapfiles.rootfs0 .. "/rootfs"
		-- If CrossDebootstrap Second Stage Exist compressed, use it..instead of 1st stage( unpack only stage.. )
		local EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = self.osbase:file_exists( self.debootstrapfiles[ "secondStage" ] )
		if ( EXIT_CODE ~= true )
		then
			-- check if 1st stage exist, compressed..
			EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = self.osbase:file_exists( self.debootstrapfiles[ "firstStage" ] )
			if ( EXIT_CODE ~= true )
			then
				EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = self.bootstrap:bootStrap_firstStage( self.keyring, self.variant, self.exclude, self.include, self.arch, self.release, self.debootstrapfiles.tmpdir, self.apt_server )
				if ( EXIT_CODE == true )
				then
					--- Prepare rootfs for Second stage
					-- Copy qemu arch emulator binary, to rootfs
					EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = self.osbase:install_emulator( arch_emulator, self.debootstrapfiles.tmpdir )
					if ( EXIT_CODE == true )
					then
						-- Copy debian-archive-keyring.pgp
						-- Copy [ debian | devuan ]-archive-keyring to 'rootfs/usr/share/keyrings/devuan-archive-keyring.gpg
						EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = self.osbase:file_exists( "/usr/share/keyrings/devuan-archive-keyring.gpg" )
						if ( EXIT_CODE == true )
						then
							EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL =  self.osbase:install_keyring( "/usr/share/keyrings/devuan-archive-keyring.gpg", self.debootstrapfiles.tmpdir )
							if ( EXIT_CODE == true )
							then
								-- TODO: Think about a way to detect if $PWD/Builds already exist,
								--       If it doesn't, create it, but with default user onwer
								--       See Issue nr. 1: https://gitea.devuan.org/tuxd3v/edev1/issues/1
								---- Make a backup of 1st CrossDebootstrap Stage to be used by 2nd stage..later..
								EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = self.bootstrap:backup_Stage( self.debootstrapfiles.tmpdir, self.debootstrapfiles[ "firstStage" ] )
								if ( EXIT_CODE == true )
								then
									print( "\27[1;35m--  \27[0mDebootStrap: 1st_debootstrap_Stage Saved \27[1;32mSUCCESSFULLY\27[0m!!" )
								else
									print( "\27[1;31m--  \27[0mDebootStrap: 1st_debootstrap_Stage Saving \27[1;31mFAILLED\27[0m!!" )
									return EXIT_SIGNAL
								end
								print( "\27[1;35m--  \27[0mDebootStrap: 1st_debootstrap_Stage Ended \27[1;32mSUCCESSFULLY\27[0m!!" )
								-- 1st-Stage Done, will proceed with 2nd-Stage
							else
								print( "\27[1;35m--  \27[0mDebootStrap: 1st_debootstrap_Stage: copying \'devuan-archive-keyring.gpg\' \27[1;31mFAILLED\27[0m!!" )
								return EXIT_SIGNAL
							end
						else
							print( "\27[1;31m--  \27[0mDebootStrap: \'/usr/share/keyrings/devuan-archive-keyring.gpg\' \27[1;31mNOT FOUND\27[0m!!" )
							return EXIT_SIGNAL
						end
					else
						print( "\27[1;31m--  \27[0mDebootStrap: \'" .. arch_emulator .. "\' \27[1;31mNOT FOUND\27[0m!!" )
						return EXIT_SIGNAL
					end
				else
					print( "\27[1;31m--  \27[0mDebootStrap: 1st_debootstrap_Stage \27[1;31mFAILLED\27[0m!!" )
					return EXIT_SIGNAL
				end
			else
				--- Restore the Compressed, 1st-Stage..
				--
				print( "\27[1;35m--- DebootStrap: 1st_debootstrap_Stage Backup detected ..\27[0m" )
				EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = self.osbase:file_exists( self.debootstrapfiles.rootfs )
				if ( EXIT_CODE ~= true )
				then
					print( "\27[1;35m--  \27[0mDebootStrap: Restoring 1st_debootstrap_Stage Backup ..\27[0m" )
					EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = self.osbase:file_exists( self.debootstrapfiles.tmpdir )
					if ( EXIT_CODE == true )
					then
						-- Restore 2st-Stage to tmpdir..
						EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = self.bootstrap:restore_Stage( self.debootstrapfiles[ "firstStage" ], self.debootstrapfiles.tmpdir )
						if ( EXIT_CODE == true )
						then
							print( "\27[1;35m--  \27[0mDebootStrap: 1st_debootstrap_Stage Restored \27[1;32mSUCCESSFULLY\27[0m!!" )
						else
							print( "\27[1;31m--  \27[0mDebootStrap: 1st_debootstrap_Stage Restore \27[1;32mFAILLED\27[0m!!" )
							return EXIT_SIGNAL
						end
					else
						print( "\27[1;35m--  \27[0mDebootStrap: 1st_debootstrap_Stage Directory creation \'" .. self.debootstrapfiles.tmpdir .. "\' \27[1;32mFAILLED\27[0m!!" )
						return EXIT_SIGNAL
					end
				else
					print( "\27[1;35m--  \27[0mDebootStrap: RootFS detected \27[1;32mNothing to be Done\27[0m!!" )
					return EXIT_SIGNAL
				end
			end
			--- Execute 2nd Stage debootstrap..
			EXIT_SIGNAL = self:secondStage()
		else
			--- If is there a 2nd-Stage rootfs, restore the Compressed one, Otherwise do nothing..
			--
			EXIT_SIGNAL = self:secondStageRestore()
		end
		return EXIT_SIGNAL
	end,
	--- Execute 2nd Stage debootstrap..
	secondStage	= function( self )
		-- Prepare Chroot Environment..
		print( "\27[1;35m--- DebootStrap: 2nd_debootstrap_Stage Start ..\27[0m" )
		local EXIT_CODE, EXIT_STATUS
		local EXIT_SIGNAL = self.osbase:chRoot( self.debootstrapfiles.tmpdir, "/debootstrap/debootstrap --second-stage" )
		if ( EXIT_SIGNAL == 0 )
		then
			print( "\27[1;35m--  \27[0mDebootStrap: 2nd_debootstrap_Stage Executed \27[1;32mSUCCESSFULLY\27[0m!!" )
		else
			print( "\27[1;31m--  \27[0mDebootStrap: 2nd_debootstrap_Stage Execution \27[1;31mFAILLED\27[0m!!" )
			return EXIT_SIGNAL
		end
		--- Compress now 2nd-Stage, with quemu emulator inside( we will need it later.. )
		EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = self.bootstrap:backup_Stage( self.debootstrapfiles.tmpdir, self.debootstrapfiles[ "secondStage" ] )
		if ( EXIT_CODE == true )
		then
			print( "\27[1;35m--  \27[0mDebootStrap: 2nd_debootstrap_Stage Saved \27[1;32mSUCCESSFULLY\27[0m!!" )
			-- Remove now tmpdir, will not be used anymore, from now on..
			EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = self.osbase:file_exists( self.debootstrapfiles.tmpdir )
			if ( EXIT_CODE == true )
			then
				print( "\27[1;35m--  \27[0mDebootStrap: Removing " .. self.debootstrapfiles.tmpdir0 .. " ..!!" )
				EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = self.osbase:rmdir( self.debootstrapfiles.tmpdir0 )
			end
		end
		if( EXIT_CODE == true )
		then
			print( "\27[1;35m--  \27[0mDebootStrap: 2nd_debootstrap_Stage Completed \27[1;32mSUCCESSFULLY\27[0m!!" )
		else
			print( "\27[1;35m--  \27[0mDebootStrap: Something Went wront in 2nd-Stage \27[1;31mFAILLED\27[0m!!" )
		end
		return EXIT_SIGNAL
	end,
	-- Start by Restoring Second Stage
	secondStageRestore	= function( self )
		print( "\27[1;35m--- DebootStrap: 2nd_debootstrap_Stage Backup detected ..\27[0m" )
		local EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = self.osbase:file_exists( self.debootstrapfiles.rootfs )
		if ( EXIT_CODE ~= true )
		then
			print( "\27[1;35m--  \27[0mDebootStrap: Restoring 2nd_debootstrap_Stage Backup ..\27[0m" )
			EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = self.osbase:mkdir( self.debootstrapfiles.rootfs )
			if ( EXIT_CODE == true )
			then
				-- Restore Second Stage..but to rootfs, *not* tmpdir..
				EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = self.bootstrap:restore_Stage( self.debootstrapfiles[ "secondStage" ], self.debootstrapfiles.rootfs )
				if ( EXIT_CODE == true )
				then
					print( "\27[1;35m--  \27[0mDebootStrap: 2nd_debootstrap_Stage Restored \27[1;32mSUCCESSFULLY\27[0m!!" )
					-- Remove now tmpdir, will not be used anymore, from now on..
					EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = self.osbase:file_exists( self.debootstrapfiles.tmpdir0 )
					if ( EXIT_CODE == true )
					then
						print( "\27[1;35m--  \27[0mDebootStrap: Removing " .. self.debootstrapfiles.tmpdir0 .. " ..!!" )
						EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = self.osbase:rmdir( self.debootstrapfiles.tmpdir0 )
					end
				else
					print( "\27[1;31m--  \27[0mDebootStrap: 2nd_debootstrap_Stage Restore \27[1;31mFAILLED\27[0m!!" )
				end
			end
		else
			print( "\27[1;35m--  \27[0mDebootStrap: RootFS detected \27[1;32mNothing to be Done\27[0m!!" )
		end
		return EXIT_SIGNAL
	end
}
return crossdebootstrap
