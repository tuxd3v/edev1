-- Description for Pine64 RockPro64 Module
local RPro64 = {
		-- Require OS..
		os		= require "os",
	  	--toolchain	= nil,
		--bootloader	= nil,
		--kernel	= nil,
		rootpath	= "RPro64",

		-- CPU Properties
		cpu             = require "reference.hw.cpus".RockChip.RK3399,

		--- Bootloader Properties
		--
		-- u-boot support
		boot            = nil,
		boot_compiler   = nil,
		-- ARM Trusted Execution Environment
		tee		= {
			-- ARM Trusted Firmware
			name		= "armtrustedfirmware",
			platform	= "rk3399",
			stage		= "bl31",
			image		= "bl31.elf"
		},
		bootImage       = {
			"idbloader.img",
			"u-boot.itb"
		},
		bootdefconfig	= "rockpro64-rk3399_defconfig",
		env		= {},
		cmd		= {
				"setenv macaddr da 19 c8 7a 6d f4\n",
				"#setenv devtype mmc\n",
				"#setenv devnum 1\n",
				"#part uuid ${devtype} ${devnum}:1 idbloaderuuid\n",
				"#part uuid ${devtype} ${devnum}:2 ubootuuid\n",
				"#part uuid ${devtype} ${devnum}:3 noneuuid\n",
				"#part uuid ${devtype} ${devnum}:4 bootuuid\n",
				"#part uuid ${devtype} ${devnum}:5 rootfsuuid\n",
				"setenv bootargs \"earlyprintk debug=on earlycon=uart8250,mmio32,0xff1a0000  console=tty1 console=ttyS2,1500000n8 root=/dev/mmcblk1p5 rw rootfstype=ext4 fsck.repair=yes net.ifnames=0 video=eDP-1:1680x1050@60 video=HDMI-1:1680x1050@60 bootsplash.bootfile=dev1-arm16.png\"\n",
				"# ubootpart=${bootuuid}\n",
				"setenv fdtfile rockchip/rk3399-rockpro64.dtb\n",
				"if load ${devtype} ${devnum}:4 ${kernel_addr_r} vmlinuz; then\n",
				"  if load ${devtype} ${devnum}:5 ${fdt_addr_r} usr/lib/linux-image-5.8.11/${fdtfile}; then\n",
				"    fdt addr ${fdt_addr_r}\n",
				"    fdt resize 65536\n",
				"    fdt set /ethernet@fe300000 local-mac-address \"[${macaddr}]\"\n",
				"    booti ${kernel_addr_r} - ${fdt_addr_r};\n",
				"  fi;\n",
				"fi\n"
		},
		--- Kernel Properties
		--
		kernel		= nil,
		kernel_compiler = nil,
		kernelvers	= "current",
		arch		= "arm64",
		dtb             = "rk3399-rockpro64.dtb",
		kerneldefconfig	= "rockpro64-rk3399_defconfig",
		patchs		= {
				kernel = {
						current = {}
				},
				bootloader = {
					--atf	= {},
					--uboot	= {}
				}
		},
		---- User Space Properties
		---
		--- Modules config
		--
		modules = {
			"# /etc/modules: kernel modules to load at boot time.\n",
			"#\n",
			"# This file contains the names of kernel modules that should be loaded\n",
			"# at boot time, one per line. Lines beginning with \"#\" are ignored.\n",
			"zram num_devices=6"
		},
		--- ZRAM Config
		--
		zram_rules = {
			"KERNEL==\"zram0\", SUBSYSTEM==\"block\", DRIVER==\"\", ACTION==\"add\", ATTR{initstate}==\"0\", ATTR{comp_algorithm}=\"lz4\", ATTR{disksize}=\"96M\", RUN+=\"/sbin/mkswap $env{DEVNAME}\"\n",
			"KERNEL==\"zram1\", SUBSYSTEM==\"block\", DRIVER==\"\", ACTION==\"add\", ATTR{initstate}==\"0\", ATTR{comp_algorithm}=\"lz4\", ATTR{disksize}=\"96M\", RUN+=\"/sbin/mkswap $env{DEVNAME}\"\n",
			"KERNEL==\"zram2\", SUBSYSTEM==\"block\", DRIVER==\"\", ACTION==\"add\", ATTR{initstate}==\"0\", ATTR{comp_algorithm}=\"lz4\", ATTR{disksize}=\"96M\", RUN+=\"/sbin/mkswap $env{DEVNAME}\"\n",
			"KERNEL==\"zram3\", SUBSYSTEM==\"block\", DRIVER==\"\", ACTION==\"add\", ATTR{initstate}==\"0\", ATTR{comp_algorithm}=\"lz4\", ATTR{disksize}=\"96M\", RUN+=\"/sbin/mkswap $env{DEVNAME}\"\n",
			"KERNEL==\"zram4\", SUBSYSTEM==\"block\", DRIVER==\"\", ACTION==\"add\", ATTR{initstate}==\"0\", ATTR{comp_algorithm}=\"lz4\", ATTR{disksize}=\"96M\", RUN+=\"/sbin/mkswap $env{DEVNAME}\"\n",
			"KERNEL==\"zram5\", SUBSYSTEM==\"block\", DRIVER==\"\", ACTION==\"add\", ATTR{initstate}==\"0\", ATTR{comp_algorithm}=\"lz4\", ATTR{disksize}=\"96M\", RUN+=\"/sbin/mkswap $env{DEVNAME}\""
		},
		--- FSTab Config
		--
		fstab = {
			"\n# <file system>	<dir>	<type>	<options>		<dump>		<pass>\n",
			"/dev/mmcblk1p4		/boot	ext2	defaults		0		2\n",
			"/dev/mmcblk1p5		/	ext4	defaults,noatime	0		1\n",
			"/dev/zram0 		swap    swap    pri=1			0		0\n",
			"/dev/zram1 		swap    swap    pri=2			0		0\n",
			"/dev/zram2 		swap    swap    pri=3			0		0\n",
			"/dev/zram3 		swap    swap    pri=5			0		0\n",
			"/dev/zram4 		swap    swap    pri=6			0		0\n",
			"/dev/zram5 		swap    swap    pri=4			0		0"
		},
		--- InitTab Config
		--
		inittab = {
			"\n# Login tty Via Serial Uart..\n",
			"T1:12345:respawn:/sbin/agetty -L ttyS2 1500000 vt100\n"
		},
		--- Interfaces Config
		--
		-- Add Persistent NET Rules
		net_rules  ={
			"SUBSYSTEM==\"net\", ACTION==\"add\", DRIVERS==\"?*\", ATTR{address}==\"da:19:c8:7a:6d:f3\", ATTR{dev_id}==\"0x0\", ATTR{type}==\"1\", KERNEL==\"eth*\", NAME=\"eth0\"\n"
		},
		-- Configure eth0 for dhcp
		interfaces = {
			"\n# This file describes the network interfaces available on your system\n",
			"# and how to activate them. For more information, see interfaces(5).\n",
			"\n",
			"source /etc/network/interfaces.d/*\n",
			"\n",
			"# The loopback network interface\n",
			"auto lo\n",
			"iface lo inet loopback\n",
			"\n",
			"# The primary network interface\n",
			"allow-hotplug eth0\n",
			"iface eth0 inet dhcp\n",
			"\n"
		},
		-- In RockChip, bootloader ...
		disk = {
			-- BootLoader Section
			bootloader = {
				-- Boot1 idbloader
				{
					-- Sector size in bytes [ Integer ]
					ssize	= 512,
					-- Seek number of Sectors[ Integer ]
					sseek	= 64,
					-- Size of the bootloader Area in Sectors[ Integer ]
					scount	= 8000,
					-- dd convert field[ String1,String2 .. ]
					conv	= "notrunc,fdatasync"
				},
				-- Boot2 uboot
				{
					-- Sector size in bytes [ Integer ]
					ssize	= 512,
					-- Seek number of Sectors[ Integer ]
					sseek	= 16384,
					-- Size of the bootloader Area in Sectors[ Integer ]
					scount	= 8192,
					-- dd convert field[ String1,String2 .. ]
					conv	= "notrunc,fdatasync"
				},
				-- Boot3 none
				{
					-- Sector size in bytes [ Integer ]
					ssize	= 512,
					-- Seek number of Sectors[ Integer ]
					sseek	= 24576,
					-- Size of the bootloader Area in Sectors[ Integer ]
					scount	= 8192,
					-- dd convert field[ String1,String2 .. ]
					conv	= "notrunc,fdatasync"
				}
			},
			-- parted script includes Partition table
			partitions = " --script mklabel gpt unit s mkpart idbloader 64 8063 mkpart uboot 16384 24575 mkpart none 24576 32767 mkpart boot 32768 262143 mkpart rootfs 262144 3743744 set 4 boot on set 4 esp on print",
			-- Partitions Fyle System Formats
			formatfs = {
				ext2 = 4,
				ext4 = 5
			} 
		},
		--- Assist Functions
		--
		-- Get RootPath for this Board
	  	init	= function( self )
	  		-- require Dependencies
	  		-- Uboot
			self.boot		= require "reference.tools.bootloaders".arm
			self.boot_compiler	= {}
			-- 1 - To Compile uboot
			-- 2 - To compile Arm Trusted Firmware- cortex M0 pmu driver
			self.boot_compiler[ 1 ]	= require "reference.tools.toolchains".arm.aarch64.baremetal
			self.boot_compiler[ 2 ]	= require "reference.tools.toolchains".arm.aarch32.baremetal
			-- Kernel
			self.kernel		= require ('reference.tools.kernels').mainline
			self.kernel_compiler 	= require "reference.tools.toolchains".arm.aarch64.linuxgnu
		end,
		-- Get RootPath for this Board
	  	getRootPath = function( self )
			return self.rootpath
		end
}
return RPro64
