-- Description for Khadas VIM3 Module
local ViM3 = {
		-- Require OS..
		os		= require "os",
		rootpath	= "ViM3",

		-- CPU Properties
		cpu             = require "reference.hw.cpus".AmLogic.A311D,

		--- Bootloader Properties
		--
		-- u-boot support
		boot            = nil,
		boot_compiler   = nil,
		-- ARM Trusted Execution Environment
		tee		= {
			-- ARM Trusted Firmware
			name		= "armtrustedfirmware",
			-- It should be "g12b", but 'g12b' doesn't exist in ARM Trusted Firmware :(
			-- And so we use the g12a, which is the one configured for uboot __mainline__ ( 'CONFIG_MESON_G12A=y' )
			platform	= "g12a",
			stage		= "bl31",
			image		= "bl31.bin"
		},
		bootImage       = {
			"u-boot.bin"
		},
		bootdefconfig	= "khadas-vim3_defconfig",
		env		= {
		},
		cmd		= {
		},
		--- Kernel Properties
		--
		kernel		= nil,
		kernel_compiler = nil,
		kernelvers	= "current",
		arch		= "arm64",
		dtb             = "meson-g12b-a311d-khadas-vim3.dtb",
		kerneldefconfig	= "meson-g12b-a311d-khadas-vim3_defconfig",
		patchs		= {
				kernel = {
						current = {}
				},
				bootloader = {
					--atf	= {},
					--uboot	= {}
				}
		},
		---- User Space Properties
		---
		--- Modules config
		--
		modules = {
			"# /etc/modules: kernel modules to load at boot time.\n",
			"#\n",
			"# This file contains the names of kernel modules that should be loaded\n",
			"# at boot time, one per line. Lines beginning with \"#\" are ignored.\n",
			"zram num_devices=6"
		},
		--- ZRAM Config
		--
		zram_rules = {
			"KERNEL==\"zram0\", SUBSYSTEM==\"block\", DRIVER==\"\", ACTION==\"add\", ATTR{initstate}==\"0\", ATTR{comp_algorithm}=\"lz4\", ATTR{disksize}=\"192M\", RUN+=\"/sbin/mkswap $env{DEVNAME}\"\n",
			"KERNEL==\"zram1\", SUBSYSTEM==\"block\", DRIVER==\"\", ACTION==\"add\", ATTR{initstate}==\"0\", ATTR{comp_algorithm}=\"lz4\", ATTR{disksize}=\"192M\", RUN+=\"/sbin/mkswap $env{DEVNAME}\"\n",
			"KERNEL==\"zram2\", SUBSYSTEM==\"block\", DRIVER==\"\", ACTION==\"add\", ATTR{initstate}==\"0\", ATTR{comp_algorithm}=\"lz4\", ATTR{disksize}=\"192M\", RUN+=\"/sbin/mkswap $env{DEVNAME}\"\n",
			"KERNEL==\"zram3\", SUBSYSTEM==\"block\", DRIVER==\"\", ACTION==\"add\", ATTR{initstate}==\"0\", ATTR{comp_algorithm}=\"lz4\", ATTR{disksize}=\"192M\", RUN+=\"/sbin/mkswap $env{DEVNAME}\"\n",
			"KERNEL==\"zram4\", SUBSYSTEM==\"block\", DRIVER==\"\", ACTION==\"add\", ATTR{initstate}==\"0\", ATTR{comp_algorithm}=\"lz4\", ATTR{disksize}=\"192M\", RUN+=\"/sbin/mkswap $env{DEVNAME}\"\n",
			"KERNEL==\"zram5\", SUBSYSTEM==\"block\", DRIVER==\"\", ACTION==\"add\", ATTR{initstate}==\"0\", ATTR{comp_algorithm}=\"lz4\", ATTR{disksize}=\"192M\", RUN+=\"/sbin/mkswap $env{DEVNAME}\""
		},
		--- FSTab Config
		--
		fstab = {
			"\n# <file system>	<dir>	<type>	<options>		<dump>		<pass>\n",
			"/dev/mmcblk1p4		/boot	ext2	defaults		0		2\n",
			"/dev/mmcblk1p5		/	ext4	defaults,noatime	0		1\n",
			"/dev/zram0 		swap    swap    pri=1			0		0\n",
			"/dev/zram1 		swap    swap    pri=2			0		0\n",
			"/dev/zram2 		swap    swap    pri=3			0		0\n",
			"/dev/zram3 		swap    swap    pri=5			0		0\n",
			"/dev/zram4 		swap    swap    pri=6			0		0\n",
			"/dev/zram5 		swap    swap    pri=4			0		0"
		},
		--- InitTab Config
		--
		inittab = {
			"\n# Login tty Via Serial Uart..\n",
			"T1:12345:respawn:/sbin/agetty -L ttyS2 1500000 vt100\n"
		},
		--- Interfaces Config
		--
		-- Add Persistent NET Rules
		--net_rules  ={
		--	"SUBSYSTEM==\"net\", ACTION==\"add\", DRIVERS==\"?*\", ATTR{address}==\"da:19:c8:7a:6d:f3\", ATTR{dev_id}==\"0x0\", ATTR{type}==\"1\", KERNEL==\"eth*\", NAME=\"eth0\"\n"
		--},
		-- Configure eth0 for dhcp
		interfaces = {
			"\n# This file describes the network interfaces available on your system\n",
			"# and how to activate them. For more information, see interfaces(5).\n",
			"\n",
			"source /etc/network/interfaces.d/*\n",
			"\n",
			"# The loopback network interface\n",
			"auto lo\n",
			"iface lo inet loopback\n",
			"\n",
			"# The primary network interface\n",
			"allow-hotplug eth0\n",
			"iface eth0 inet dhcp\n",
			"\n"
		},
		--- Assist Functions
		--
		-- In AmLogic, bootloader starts at 513 bytes and end at 1048576 bytes ( 1MB )
		disk = {
			-- BootLoader Section
			bootloader = {
				-- Boot1 u-boot.bin
				{
					-- Sector size in bytes [ Integer ]
					ssize	= 512,
					-- Seek number of Sectors[ Integer ]
					sseek	= 1,
					-- Size of the bootloader Area... including Uboot Environment[ 1920 - 2047 ] in Sectors[ Integer ]
					scount	= 2047,
					-- dd convert field[ String1,String2 .. ]
					conv	= "notrunc,fdatasync"
				}
			},
			-- parted script includes Partition table
			partitions = " --script mklabel msdos mkpart primary ext2 1MB 101MB mkpart primary ext4 101MB 1700MB set 1 lba off set 2 lba off print",
			-- Partitions Fyle System Formats
			formatfs = {
				ext2 = 1,
				ext4 = 2
			} 
		},
		-- Get RootPath for this Board
	  	init	= function( self )
	  		-- require Dependencies
	  		-- Uboot
			self.boot		= require "reference.tools.bootloaders".arm
			self.boot_compiler	= {}
			-- 1 - To Compile uboot
			-- 2 - To compile Arm Trusted Firmware- cortex M0 pmu driver
			self.boot_compiler[ 1 ]	= require "reference.tools.toolchains".arm.aarch64.baremetal
			--self.boot_compiler[ 2 ]	= require "reference.tools.toolchains".arm.aarch32.baremetal
			-- Kernel
			self.kernel		= require ('reference.tools.kernels').mainline
			self.kernel_compiler 	= require "reference.tools.toolchains".arm.aarch64.linuxgnu
		end,
		-- Get RootPath for this Board
	  	getRootPath = function( self )
			return self.rootpath
		end
}
return ViM3
