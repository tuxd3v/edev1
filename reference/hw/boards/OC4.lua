-- Description for Odroid C4 Module
local OC4 = {
		-- Require OS..
		os		= require "os",
	  	--toolchain	= nil,
		--bootloader	= nil,
		--kernel	= nil,
		rootpath	= "OC4",

		-- CPU Properties
		cpu		= require "reference.hw.cpus".AmLogic.S905X3,

		--- Bootloader Properties
		--
		-- u-boot support is in __mainline__
		boot		= nil,
		boot_compiler	= nil,
		-- ARM Trusted Execution Environment
		tee		= {
			-- ARM Trusted Firmware
			name		= "armtrustedfirmware",
			-- It should be "sm1", but 'sm1' doesn't exist in ARM Trusted Firmware :(
			-- And so we use the g12a, which is the one configured for uboot __mainline__ ( 'CONFIG_MESON_G12A=y' )
			platform	= "g12a",
			stage		= "bl31",
			image		= "bl31.bin"
		},
		bootImage	= {
			"u-boot.bin"
		},
		bootdefconfig	= "odroid-c4_defconfig",
		env		= {},
		cmd		= nil,
		--- Kernel Properties
		--
		kernel			= nil,
		kernel_compiler 	= nil,
		kernelvers		= "current",
		arch			= "arm64",
		dtb			= "meson-sm1-odroid-c4.dtb",
		kerneldefconfig		= "meson-sm1-odroid-c4_defconfig",
		-- applly on stable kernels..
		patchs		= {
				kernel = {
						current	= {}
				},
				bootloader = {
					--atf	= {},
					--uboot	= {}
				}
		},
		---- User Space Properties
		---
		--- Modules config
		--
		modules = {
			"# /etc/modules: kernel modules to load at boot time.\n",
			"#\n",
			"# This file contains the names of kernel modules that should be loaded\n",
			"# at boot time, one per line. Lines beginning with \"#\" are ignored.\n",
			"zram num_devices=4"
		},
		--- ZRAM Config
		--
		zram_rules = {
			"KERNEL==\"zram0\", SUBSYSTEM==\"block\", DRIVER==\"\", ACTION==\"add\", ATTR{initstate}==\"0\", ATTR{comp_algorithm}=\"lz4\", ATTR{disksize}=\"224M\", RUN+=\"/sbin/mkswap $env{DEVNAME}\"\n",
			"KERNEL==\"zram1\", SUBSYSTEM==\"block\", DRIVER==\"\", ACTION==\"add\", ATTR{initstate}==\"0\", ATTR{comp_algorithm}=\"lz4\", ATTR{disksize}=\"224M\", RUN+=\"/sbin/mkswap $env{DEVNAME}\"\n",
			"KERNEL==\"zram2\", SUBSYSTEM==\"block\", DRIVER==\"\", ACTION==\"add\", ATTR{initstate}==\"0\", ATTR{comp_algorithm}=\"lz4\", ATTR{disksize}=\"224M\", RUN+=\"/sbin/mkswap $env{DEVNAME}\"\n",
			"KERNEL==\"zram3\", SUBSYSTEM==\"block\", DRIVER==\"\", ACTION==\"add\", ATTR{initstate}==\"0\", ATTR{comp_algorithm}=\"lz4\", ATTR{disksize}=\"224M\", RUN+=\"/sbin/mkswap $env{DEVNAME}\"\n"
		},
		--- FSTab Config
		--
		fstab = {
			"\n# <file system>	<dir>	<type>	<options>		<dump>		<pass>\n",
			"/dev/mmcblk0p1		/boot	ext2	defaults		0		2\n",
			"/dev/mmcblk0p2		/	ext4	defaults,noatime	0		1\n",
			"/dev/zram0 		none    swap    pri=1			0		0\n",
			"/dev/zram1 		none    swap    pri=2			0		0\n",
			"/dev/zram2 		none    swap    pri=4			0		0\n",
			"/dev/zram3 		none    swap    pri=3			0		0\n"
		},
		--- InitTab Config
		--
		inittab = {
			"\n# Login tty Via Serial Uart..\n",
			"T1:12345:respawn:/sbin/agetty -L ttyS0 115200 vt100\n"
		},
		--- Interfaces Config
		--
		interfaces = {
			"\n# This file describes the network interfaces available on your system\n",
			"# and how to activate them. For more information, see interfaces(5).\n",
			"\n",
			"source /etc/network/interfaces.d/*\n",
			"\n",
			"# The loopback network interface\n",
			"auto lo\n",
			"iface lo inet loopback\n",
			"\n",
			"# The primary network interface\n",
			"allow-hotplug eth0\n",
			"iface eth0 inet dhcp\n",
			"\n"
		},
		--- Assist Functions
		--
		-- In AmLogic, bootloader is at [ 512 - 1048576 ] bytes
		disk = {
			-- BootLoader Section
			bootloader = {
				-- Boot1 u-boot.bin
				{
					-- Sector size in bytes [ Integer ]
					ssize	= 512,
					-- Seek number of Sectors[ Integer ]
					sseek	= 1,
					-- Size of the bootloader Area... including Uboot Environment[ 1920 - 2047 ] in Sectors[ Integer ]
					scount	= 2047,
					-- dd convert field[ String1,String2 .. ]
					conv	= "notrunc,fdatasync"
				}
			},
			-- parted script includes Partition table
			partitions = " --script mklabel msdos mkpart primary ext2 1MB 101MB mkpart primary ext4 101MB 1700MB set 1 lba off set 2 lba off print",
			-- Partitions Fyle System Formats
			formatfs = {
				ext2 = 1,
				ext4 = 2
			} 
		},
		-- Init Board function
	  	init	= function( self )
	  		-- require Dependencies
	  		-- Uboot
			self.boot		= require "reference.tools.bootloaders".arm
			self.boot_compiler	= {}
			-- 1 - To Compile uboot
			-- -- 2 - To compile Arm Trusted Firmware- cortex M0 pmu driver
			self.boot_compiler[ 1 ]	= require "reference.tools.toolchains".arm.aarch64.baremetal
			-- For the Arm Cortex-M3 PMU included( For now, there are no mainline support for it in uboot/ATF )
			--self.boot_compiler[ 2 ]	= require "reference.tools.toolchains".arm.aarch32.baremetal
			-- Kernel
			self.kernel		= require ('reference.tools.kernels').mainline
			self.kernel_compiler 	= require "reference.tools.toolchains".arm.aarch64.linuxgnu
		end,
		-- Get RootPath for this Board
	  	getRootPath = function( self )
			return self.rootpath
		end
}
return OC4
