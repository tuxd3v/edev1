-- Description for OrangePi One Plus Module
local RPi4 = {
		-- Require OS..
		os		= require "os",
	  	--toolchain	= nil,
		--bootloader	= nil,
		--kernel		= nil,
		rootpath	= "RPi4",

		-- CPU Properties
		cpu		= require "reference.hw.cpus".Broadcom.BCM2711,

		--- Bootloader Properties
		--
		-- u-boot support since v2018.09 release
		boot		= nil,
		boot_compiler	= nil,
		-- Trusted Execution Environment
		tee		= {
			-- ARM Trusted Firmware
			platform	= "rpi4",
			stage		= "bl31",
			image		= "bl31.bin"
		},
		bootImage	= {
			"u-boot.bin",
			"u-boot-nodtb.bin"
		},
		bootdefconfig	= "rpi_4_defconfig",
		env		= {
				"console=ttyS0,115200n8 no_console_suspend\n",
				"kernel_filename=orangepi/uImage\n",
				"initrd_filename=initrd.img\n",
				"root=/dev/mmcblk1p2\n",
				"bootdelay=6"
		},
		cmd		= {
				"# setup MAC address\n",
				"setenv ethaddr 7E:8C:70:59:B5:69\n",
				"# set console to tty0\n",
				"setenv console tty0\n",
				"# Send debug info to uart, and also display\n",
				"setenv stdout=serial,vga\n",
				"setenv stderr=serial,vga\n",
				"setenv bootargs debug=on console=tty0 console=ttyS0,115200 earlyprintk root=/dev/mmcblk0p2 rw rootfstype=ext4 rootwait fsck.repair=yes panic=15\n",
				"setenv fdtfile allwinner/sun50i-h6-orangepi-one-plus.dtb\n",
				"load mmc 0:2 0x43000000 usr/lib/linux-image-5.3.10/${fdtfile} || load mmc 0:1 0x43000000 ${fdtfile} || load mmc 0:1 0x43000000 boot/${fdtfile}\n",
				"load mmc 0:1 0x42000000 vmlinuz-5.3.10 || load mmc 0:1 0x42000000 Image || load mmc 0:1 0x42000000 boot/vmlinuz-5.3.10 || load mmc 0:1 0x42000000 boot/Image\n",
				"booti 0x42000000 - 0x43000000"
		},
		--- Kernel Properties
		--
		kernel			= nil,
		kernel_compiler 	= nil,
		kernelvers		= "current",
		arch			= "arm64",
		dtb			= "bcm2711-rpi-4-b.dtb",
		kerneldefconfig		= "bcm2711_defconfig",
		-- applly on stable kernels..
		patchs		= {
				kernel = {
						current = {}
				},
				bootloader = {
					--atf	= {},
					--uboot	= {}
				}
		},
		---- User Space Properties
		---
		--- Modules config
		--
		modules = {
			"# /etc/modules: kernel modules to load at boot time.\n",
			"#\n",
			"# This file contains the names of kernel modules that should be loaded\n",
			"# at boot time, one per line. Lines beginning with \"#\" are ignored.\n",
			"zram num_devices=4"
		},
		--- ZRAM Config
		--
		zram_rules = {
			"KERNEL==\"zram0\", SUBSYSTEM==\"block\", DRIVER==\"\", ACTION==\"add\", ATTR{initstate}==\"0\", ATTR{comp_algorithm}=\"lz4\", ATTR{disksize}=\"192M\", RUN+=\"/sbin/mkswap $env{DEVNAME}\"\n",
			"KERNEL==\"zram1\", SUBSYSTEM==\"block\", DRIVER==\"\", ACTION==\"add\", ATTR{initstate}==\"0\", ATTR{comp_algorithm}=\"lz4\", ATTR{disksize}=\"192M\", RUN+=\"/sbin/mkswap $env{DEVNAME}\"\n",
			"KERNEL==\"zram2\", SUBSYSTEM==\"block\", DRIVER==\"\", ACTION==\"add\", ATTR{initstate}==\"0\", ATTR{comp_algorithm}=\"lz4\", ATTR{disksize}=\"192M\", RUN+=\"/sbin/mkswap $env{DEVNAME}\"\n",
			"KERNEL==\"zram3\", SUBSYSTEM==\"block\", DRIVER==\"\", ACTION==\"add\", ATTR{initstate}==\"0\", ATTR{comp_algorithm}=\"lz4\", ATTR{disksize}=\"192M\", RUN+=\"/sbin/mkswap $env{DEVNAME}\"\n"
		},
		--- FSTab Config
		--
		fstab = {
			"\n# <file system>	<dir>	<type>	<options>		<dump>		<pass>\n",
			"/dev/mmcblk0p1		/boot	vfat	defaults		0		2\n",
			"/dev/mmcblk0p2		/	ext4	defaults,noatime	0		1\n",
			"/dev/zram0 		none    swap    pri=1			0		0\n",
			"/dev/zram1 		none    swap    pri=2			0		0\n",
			"/dev/zram2 		none    swap    pri=4			0		0\n",
			"/dev/zram3 		none    swap    pri=3			0		0\n"
		},
		--- InitTab Config
		--
		inittab = {},
		--- Interfaces Config
		--
		interfaces = {
			"\n# This file describes the network interfaces available on your system\n",
			"# and how to activate them. For more information, see interfaces(5).\n",
			"\n",
			"source /etc/network/interfaces.d/*\n",
			"\n",
			"# The loopback network interface\n",
			"auto lo\n",
			"iface lo inet loopback\n",
			"\n",
			"# The primary network interface\n",
			"allow-hotplug eth0\n",
			"iface eth0 inet dhcp\n",
			"\n"
		},
		--- Assist Functions
		--
		-- Get RootPath for this Board
	  	init	= function( self )
	  		-- require Dependencies
	  		-- Uboot
			self.boot		= require "reference.tools.bootloaders".arm
			self.boot_compiler	= {}
			self.boot_compiler[ 1 ]	= require "reference.tools.toolchains".arm.aarch64.baremetal
			-- Kernel
			self.kernel		= require ('reference.tools.kernels').mainline
			self.kernel_compiler 	= require "reference.tools.toolchains".arm.aarch64.linuxgnu
		end,
		mkimagePrepare = function( self, pwd, mediadisk )
			-- remove partitions
			self.os.execute( "parted " .. mediadisk .. " --script rm 1 rm 2 print" )
			-- Clean first part of the disk
			self.os.execute( "dd if=/dev/zero of=" .. mediadisk .. " bs=1M count=8 conv=sync && sleep 1" )
			-- Create new partition table, and new partitions
			self.os.execute( "parted " .. mediadisk .. " --script mklabel msdos mkpart primary vfat 1MB 101MB mkpart primary ext4 101MB 2000MB set 1 lba off set 2 lba off print" )
			--Write bootloader to sector 16, or 8K offset
			self.os.execute( "dd if=" .. pwd .. "/Builds/" .. self.rootpath .. "/" .. self.rootpath .. "_" .. self.bootImage[ 1 ] .. " of=" .. mediadisk .. " bs=512 seek=16 conv=sync" )
			-- Format partitions to new format types
			self.os.execute( "yes | mkfs.vfat " .. mediadisk .. "1 && yes | mkfs.ext4 " .. mediadisk .. "2" )
		end,

		--	-- Copy rootfs to SDCard
		--	"cd $PWD/Builds/" .. self.rootpath .. "_" .. os_base .. "/rootfs && mount /dev/sdb2 /mnt && cp -pr ./* /mnt && sync &&  umount /mnt;",
		--	-- Copy Kernel Modules,headers
		--	"cd $PWD/Builds/" .. self.rootpath .. "_" .. os_base .. "/rootfs && sudo chown root: -R " .. self.rootpath .. "{_dtbs/boot,_headers/usr,_KImage/boot,_modules/lib}",
		--},
		-- Get RootPath for this Board
	  	getRootPath = function( self )
			return self.rootpath
		end
}
return RPi4
