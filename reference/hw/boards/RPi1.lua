-- Description for RaspBerryPi1 B Module
local RPi1 = {
		-- Require OS..
		os		= require "os",
	  	--toolchain	= nil,
		--bootloader	= nil,
		--kernel	= nil,
		rootpath	= "RPi1",

		-- CPU Properties
		cpu		= require "reference.hw.cpus".Broadcom.BCM2835,

		--- Bootloader Properties
		--
		-- u-boot support
		boot		= nil,
		boot_compiler	= nil,
		-- Trusted Execution Environment
		tee		= nil,
		bootImage	= {
			"u-boot.bin",
			"u-boot-nodtb.bin"
		},
		bootdefconfig	= "rpi_defconfig",
		env		= {},
		cmd		= {
				"setenv bootdelay 3\n",
				"#setenv baudrate 115200\n",
				"# Send debug info to uart, and also display\n",
				"setenv stdout serial,vga\n",
				"setenv stderr serial,vga\n",
				"mmc dev 0\n",
				"# Set fdtfile envvar to corresponding dtb file\n",
				"setenv fdt_addr 0x02400000\n",
				"setenv fdtfile bcm2835-rpi-b.dtb\n",
				"# 64bits Kernels loads at kernel_addr_r 0x80000\n",
				"setenv kernel_addr 0x08000\n",
				"setenv bootargs earlyprintk=serial,ttyAMA0,115200n8 console=ttyAMA0,115200n8 console=tty0 root=/dev/mmcblk0p2 rw rootfstype=ext4 elevator=noop fsck.repair=yes rootwait smsc95xx.turbo_mode=N dwc_otg.lpm_enable=0 kgdboc=ttyAMA0,115200 selinux=0 noinitrd\n",
				"load mmc 0:2 ${fdt_addr} usr/lib/linux-image-5.4.67/${fdtfile}\n",
				"load mmc 0:1 ${kernel_addr} vmlinuz-5.4.67\n",
				"bootz ${kernel_addr} - ${fdt_addr}\n"
		},
		--- Kernel Properties
		--
		kernel			= nil,
		kernel_compiler 	= nil,
		kernelvers		= "mainline",
		arch			= "arm",
		dtb			= "bcm2835-rpi-b.dtb",
		kerneldefconfig		= "bcm2835_defconfig",
		-- applly on stable kernels..
		patchs		= {
				kernel = {
						mainline = {}
				},
				bootloader = {
					--uboot	= {}
				}
		},
		---- User Space Properties
		---
		--- Modules config
		--
		modules = {
			"# /etc/modules: kernel modules to load at boot time.\n",
			"#\n",
			"# This file contains the names of kernel modules that should be loaded\n",
			"# at boot time, one per line. Lines beginning with \"#\" are ignored.\n",
			"zram num_devices=1\n"
		},
		--- ZRAM Config
		--
		zram_rules = {
			"KERNEL==\"zram0\", SUBSYSTEM==\"block\", DRIVER==\"\", ACTION==\"add\", ATTR{initstate}==\"0\", ATTR{comp_algorithm}=\"lz4\", ATTR{disksize}=\"192M\", RUN+=\"/sbin/mkswap $env{DEVNAME}\"\n"
		
		},
		--- ETH0 persistent
		--
		net_rules = {
			"SUBSYSTEM==\"net\", DRIVERS==\"smsc95xx\", ACTION==\"add\", ATTR{iflink}==\"2\", ATTR{address}==\"b8:27:eb:a8:d5:96\", NAME=\"eth0\""
		
		},
		--- FSTab Config
		--
		fstab = {
			"\n# <file system>	<dir>	<type>	<options>		<dump>		<pass>\n",
			"/dev/mmcblk0p1		/boot	vfat	defaults		0		2\n",
			"/dev/mmcblk0p2		/	ext4	defaults,noatime	0		1\n",
			"/dev/zram0 		swap    swap    pri=1			0		0\n"
		},
		--- InitTab Config
		--
		inittab = {
			"\n# Login tty Via Serial Uart..\n",
			"T0:23:respawn:/sbin/agetty -L ttyAMA0 115200 vt100\n"
		},
		--- Interfaces Config
		--
		interfaces = {
			"\n# This file describes the network interfaces available on your system\n",
			"# and how to activate them. For more information, see interfaces(5).\n",
			"\n",
			"source /etc/network/interfaces.d/*\n",
			"\n",
			"# The loopback network interface\n",
			"auto lo\n",
			"iface lo inet loopback\n",
			"\n",
			"# The primary network interface\n",
			"allow-hotplug enxb827eba8d596\n",
			"iface enxb827eba8d596 inet dhcp\n",
			"\n"
		},
		-- In RaspBerryPi1, bootloader ...
		disk = {
			-- BootLoader Section
			bootloader = {
			},
			-- parted script includes Partition table
			partitions = " --script mklabel msdos mkpart primary ext2 1MB 101MB mkpart primary ext4 101MB 1700MB set 1 lba off set 2 lba off print",
			-- Partitions Fyle System Formats
			formatfs = {
				ext2 = 1,
				ext4 = 2
			} 
		},
		--- Assist Functions
		--
		-- Get RootPath for this Board
	  	init	= function( self )
	  		-- require Dependencies
	  		-- Uboot
			self.boot		= require "reference.tools.bootloaders".arm
			self.boot_compiler	= {}
			self.boot_compiler[ 1 ]	= require "reference.tools.toolchains".arm.arm32.baremetal
			-- Kernel
			self.kernel		= require ('reference.tools.kernels').mainline
			self.kernel_compiler 	= require "reference.tools.toolchains".arm.arm32.linuxgnu
		end,
		-- Get RootPath for this Board
	  	getRootPath = function( self )
			return self.rootpath
		end
}
return RPi1
