-- Description for nanoPi R2S Module
local nPiR2S = {
		-- Require OS..
		os		= require "os",
	  	--toolchain	= nil,
		--bootloader	= nil,
		--kernel	= nil,
		rootpath	= "nPiR2S",

		-- CPU Properties
		cpu		= require "reference.hw.cpus".RockChip.RK3328,

		--- Bootloader Properties
		--
		-- u-boot support
		boot		= nil,
		boot_compiler	= nil,
		-- ARM Trusted Execution Environment
		tee		= {
			-- ARM Trusted Firmware
			name		= "armtrustedfirmware",
			platform	= "rk3328",
			stage		= "bl31",
			image		= "bl31.elf"
		},
		bootImage	= {
				"idbloader.img",
				"u-boot.itb"
		},
		bootdefconfig	= "nanopi-r2s-rk3328_defconfig",
		env		= {},
		cmd		= {
				"setenv eth1addr \"B2 54 C3 7B 35 A8\"\n",
				"setenv macaddr1 \"B2 54 C3 7B 35 A8\"\n",
				"#setenv console tty1\n",
				"setenv devtype mmc\n",
				"setenv devnum 1\n",
				"part uuid ${devtype} ${devnum}:2 rootfsuuid\n",
				"setenv bootargs \"earlyprintk earlycon=uart8250,mmio32,0xff130000 swiotlb=1 coherent_pool=4m consoleblank=0 net.ifnames=0 biosdevname=0 console=ttyS2,1500000n8 cma=64M usbcore.quirks=0bda:8153:k usbcore.autosuspend=-1 usb-storage.quirks=0x2537:0x1066:u,0x2537:0x1068:u root=PARTUUID=${rootfsuuid} rw fsck.repair=yes rootwait elevator=noop\"\n",
				"if ext2load ${devtype} ${devnum}:1 ${kernel_addr_r} vmlinux; then\n",
				"  if ext4load ${devtype} ${devnum}:2 ${fdt_addr_r} usr/lib/linux-image-5.10.1/rockchip/rk3328-nanopi-r2s.dtb; then\n",
				"    fdt addr ${fdt_addr_r}\n",
				"    fdt resize 65536\n",
				"    fdt set /ethernet@ff550000 local-mac-address \"[${macaddr1}]\"\n",
				"    booti ${kernel_addr_r} - ${fdt_addr_r}\n",
				"  fi;\n",
				"fi;\n"
		},
		--- Kernel Properties
		--
		kernel			= nil,
		kernel_compiler 	= nil,
		kernelvers		= "lts",
		arch			= "arm64",
		--dtb			= "rk3328-nanopi-r2-rev00.dtb",
		dtb			= "rk3328-nanopi-r2s.dtb",
		kerneldefconfig		= "rk3328-nanopi-r2s_defconfig",
		-- applly on stable kernels..
		patchs		= {
				kernel = {
					lts= {
						-- add support for kernel mainline 5.10.x
						"add-board-nanopi-r2s.patch",
						--"add-board-nanopi-r2s-dwc3.patch",
						-- add support for kernel mainline 5.10.x
						-- ass usb3,0 support for usb-to-ethernet adapter
						"add-rk3328-usb3-phy-driver.patch",
						--"add-rk3328-usb3-phy-driver-dwc3.patch",
						-- add support for kernel mainline 5.10.x
						-- Bump driver version to v2.14.0( 2020.09 )
						-- Costumize leds for usb-to-ethernet r8153b controller, using the r8152 driver
						-- link & act
						-- Force, if reset is triggered, to re-aquire same maccaddress( b2:54:c3:7b:35:a8 )
						"r8152-customise-driver.patch"
						--"board-nanopi-r2s-r8152-customise-leds.patch"
					}
				},
				bootloader = {
					--atf	= {},
					uboot	= {
						--"add uboot support for nanopi-r2s"
						"rockchip-rk3328-Add-support-for-FriendlyARM-NanoPi-R2S.patch"
					}
				}
		},
		---- User Space Properties
		---
		--- Modules config
		--
		modules = {
			"# /etc/modules: kernel modules to load at boot time.\n",
			"#\n",
			"# This file contains the names of kernel modules that should be loaded\n",
			"# at boot time, one per line. Lines beginning with \"#\" are ignored.\n",
			"zram num_devices=4"
		},
		--- ZRAM Config
		--
		zram_rules = {
			"KERNEL==\"zram0\", SUBSYSTEM==\"block\", DRIVER==\"\", ACTION==\"add\", ATTR{initstate}==\"0\", ATTR{comp_algorithm}=\"lz4\", ATTR{disksize}=\"192M\", RUN+=\"/sbin/mkswap $env{DEVNAME}\"\n",
			"KERNEL==\"zram1\", SUBSYSTEM==\"block\", DRIVER==\"\", ACTION==\"add\", ATTR{initstate}==\"0\", ATTR{comp_algorithm}=\"lz4\", ATTR{disksize}=\"192M\", RUN+=\"/sbin/mkswap $env{DEVNAME}\"\n",
			"KERNEL==\"zram2\", SUBSYSTEM==\"block\", DRIVER==\"\", ACTION==\"add\", ATTR{initstate}==\"0\", ATTR{comp_algorithm}=\"lz4\", ATTR{disksize}=\"192M\", RUN+=\"/sbin/mkswap $env{DEVNAME}\"\n",
			"KERNEL==\"zram3\", SUBSYSTEM==\"block\", DRIVER==\"\", ACTION==\"add\", ATTR{initstate}==\"0\", ATTR{comp_algorithm}=\"lz4\", ATTR{disksize}=\"192M\", RUN+=\"/sbin/mkswap $env{DEVNAME}\"\n"
		},
		--- ETH0/ETH1 persistent
		--
		net_rules = {
			"SUBSYSTEM==\"net\", KERNEL==\"eth*\", ATTR{address}==\"b2:54:c3:7b:35:a8\", NAME=\"lan\"\n",
			"SUBSYSTEM==\"net\", KERNEL==\"eth*\", ATTR{ifindex}==\"3\", NAME=\"wan\"\n"
		},
		--- FSTab Config
		--
		fstab = {
			"\n# <file system>	<dir>	<type>	<options>		<dump>		<pass>\n",
			"/dev/mmcblk0p1		/boot	ext2	defaults		0		2\n",
			"/dev/mmcblk0p2		/	ext4	defaults,noatime	0		1\n",
			"/dev/zram0 		none    swap    pri=1			0		0\n",
			"/dev/zram1 		none    swap    pri=2			0		0\n",
			"/dev/zram2 		none    swap    pri=4			0		0\n",
			"/dev/zram3 		none    swap    pri=3			0		0\n"
		},
		--- InitTab Config
		--
		inittab = {
			"\n# Login tty Via Serial Uart..\n",
			"T1:12345:respawn:/sbin/agetty -L ttyS2 1500000 vt100\n"
		},
		--- Interfaces Config
		--
		interfaces = {
			"\n# This file describes the network interfaces available on your system\n",
			"# and how to activate them. For more information, see interfaces(5).\n",
			"\n",
			"source /etc/network/interfaces.d/*\n",
			"\n",
			"# The loopback network interface\n",
			"auto lo\n",
			"iface lo inet loopback\n",
			"\n",
			"# The primary network interface - wan\n",
			"#auto wan\n",
			"allow-hotplug wan\n",
			"iface wan inet dhcp\n",
			"	post-up  echo 1 > /sys/class/leds/nanopi-r2s\\:green\\:wan/brightness\n",
			"	post-up  ethtool -G wan rx 1024 tx 1024\n",
			"	pre-down echo 0 > /sys/class/leds/nanopi-r2s\\:green\\:wan/brightness\n",
			"\n",
			"# The secondary network interface - lan\n",
			"#auto lan\n",
			"allow-hotplug lan\n",
			"iface lan inet dhcp\n",
			"hwaddress ether b2:54:c3:7b:35:a8\n",
			"	post-up  echo 1 > /sys/class/leds/nanopi-r2s\\:green\\:lan/brightness\n",
			"	post-up  ethtool -G lan rx 4096\n",
			"	pre-down echo 0 > /sys/class/leds/nanopi-r2s\\:green\\:lan/brightness\n",
			"\n"
		},
		--- Assist Functions
		--
		-- In RockChip, bootloader: 3 raw partitions for bootloader + Environment
		disk = {
			-- BootLoader Section
			bootloader = {
				-- Boot1 idbloader
				{
					-- Sector size in bytes [ Integer ]
					ssize	= 512,
					-- Seek number of Sectors[ Integer ]
					sseek	= 64,
					-- Size of the bootloader Area in Sectors[ Integer ]
					scount	= 8000,
					-- dd convert field[ String1,String2 .. ]
					conv	= "notrunc,fdatasync"
				},
				-- Boot2 uboot.itb
				{
					-- Sector size in bytes [ Integer ]
					ssize	= 512,
					-- Seek number of Sectors[ Integer ]
					sseek	= 16384,
					-- Size of the bootloader Area in Sectors[ Integer ]
					scount	= 8192,
					-- dd convert field[ String1,String2 .. ]
					conv	= "notrunc,fdatasync"
				},
				-- Boot3 none
				{
					-- Sector size in bytes [ Integer ]
					ssize	= 512,
					-- Seek number of Sectors[ Integer ]
					sseek	= 24576,
					-- Size of the bootloader Area in Sectors[ Integer ]
					scount	= 8192,
					-- dd convert field[ String1,String2 .. ]
					conv	= "notrunc,fdatasync"
				}
			},
			-- parted script includes Partition table
			--partitions = " --script mklabel gpt unit s mkpart idbloader 64 8063 mkpart uboot 16384 24575 mkpart none 24576 32767 mkpart boot 32768 262143 mkpart rootfs 262144 3743744 set 4 boot on set 4 esp on print",
			partitions = " --script mklabel msdos unit s mkpart primary ext2 32768 262143 mkpart primary ext4 262144 3743744 set 1 boot on set 2 esp on print",
			-- Partitions Fyle System Formats
			formatfs = {
				ext2 = 1,
				ext4 = 2
			} 
		},
		-- Init Board function
	  	init	= function( self )
	  		-- require Dependencies
	  		-- Uboot
			self.boot		= require "reference.tools.bootloaders".arm
			self.boot_compiler	= {}
			self.boot_compiler[ 1 ]	= require "reference.tools.toolchains".arm.aarch64.baremetal
			-- Kernel
			self.kernel		= require ('reference.tools.kernels').mainline
			self.kernel_compiler 	= require "reference.tools.toolchains".arm.aarch64.linuxgnu
		end,
		-- Get RootPath for this Board
	  	getRootPath = function( self )
			return self.rootpath
		end
}
return nPiR2S
