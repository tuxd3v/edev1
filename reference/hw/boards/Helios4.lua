-- Description for Cobol Helios4 Module
local Helios4 = {
		-- Require OS..
		os		= require "os",
	  	--toolchain	= nil,
		--bootloader	= nil,
		--kernel	= nil,
		rootpath	= "Helios4",

		-- CPU Properties
		cpu		= require "reference.hw.cpus".Marvel.ARMADA388,

		--- Bootloader Properties
		--
		-- u-boot support since v2018.09 release
		boot		= nil,
		boot_compiler	= nil,
		-- Trusted Execution Environment
		tee		= nil,
		bootImage	= {
			"u-boot-sunxi-with-spl.bin"
		},
		bootdefconfig	= "A20-OLinuXino-Lime2_defconfig",
		env		= "console=ttyS0,115200n8 no_console_suspend\n\
kernel_filename=orangepi/uImage\n\
initrd_filename=initrd.img\n\
root=/dev/mmcblk1p2\n\
bootdelay=6",
		cmd		= {
			"# setup MAC address",
			"# setenv ethaddr 7E:8C:70:59:B5:71",
			"# Send debug info to uart, and also display",
			"setenv stdout serial,vga",
			"setenv stderr serial,vga",
			"setenv bootargs debug=on console=ttyS0,115200 earlyprintk root=/dev/mmcblk0p2 rw rootfstype=ext4 rootwait fsck.repair=yes panic=15",
			"setenv fdtfile sun7i-a20-olinuxino-lime2.dtb",
			"load mmc 0:2 0x43000000 usr/lib/linux-image-5.3.11/${fdtfile} || load mmc 0:1 0x43000000 ${fdtfile} || load mmc 0:1 0x43000000 boot/${fdtfile}",
			"load mmc 0:1 0x42000000 vmlinuz-5.3.11 || load mmc 0:1 0x42000000 zImage || load mmc 0:1 0x42000000 boot/vmlinuz-5.3.11 || load mmc 0:1 0x42000000 boot/zImage",
			"bootz 0x42000000 - 0x43000000"
		},
		--- Kernel Properties
		--
		kernel			= nil,
		kernel_compiler 	= nil,
		kernelvers		= "lts",
		arch			= "arm",
		dtb			= "sun7i-a20-olinuxino-lime2.dtb",
		kerneldefconfig		= "A20olinuxino_lime2_defconfig",
		-- applly on stable kernels..
		patchs		= {
				kernel = {
						lts = {}
				},
				bootloader = {
					--uboot	= {}
				}
		},
		---- User Space Properties
		---
		--- Modules config
		--
		modules = {
			"# /etc/modules: kernel modules to load at boot time.\n",
			"#\n",
			"# This file contains the names of kernel modules that should be loaded\n",
			"# at boot time, one per line. Lines beginning with \"#\" are ignored.\n",
			"zram num_devices=2"
		},
		--- ZRAM Config
		--
		zram_rules = {
			"KERNEL==\"zram0\", SUBSYSTEM==\"block\", DRIVER==\"\", ACTION==\"add\", ATTR{initstate}==\"0\", ATTR{comp_algorithm}=\"lz4\", ATTR{disksize}=\"333M\", RUN+=\"/sbin/mkswap $env{DEVNAME}\"\n",
			"KERNEL==\"zram1\", SUBSYSTEM==\"block\", DRIVER==\"\", ACTION==\"add\", ATTR{initstate}==\"0\", ATTR{comp_algorithm}=\"lz4\", ATTR{disksize}=\"333M\", RUN+=\"/sbin/mkswap $env{DEVNAME}\""
		},
		--- FSTab Config
		--
		fstab = {
			"\n# <file system>	<dir>	<type>	<options>		<dump>		<pass>\n",
			"/dev/mmcblk0p1		/boot	ext2	defaults		0		2\n",
			"/dev/mmcblk0p2		/	ext4	defaults,noatime	0		1\n",
			"/dev/zram0 		none    swap    sw,pri=1		0		0\n",
			"/dev/zram1 		none    swap    sw,pri=2		0		0\n"
		},
		--- InitTab Config
		--
		inittab = {
			"\n# Login tty Via Serial Uart..\n",
			"T1:12345:respawn:/sbin/agetty -L ttyS0 115200 vt100\n"
		},
		--- Interfaces Config
		--
		interfaces = {
			"\n# This file describes the network interfaces available on your system\n",
			"# and how to activate them. For more information, see interfaces(5).\n",
			"\n",
			"source /etc/network/interfaces.d/*\n",
			"\n",
			"# The loopback network interface\n",
			"auto lo\n",
			"iface lo inet loopback\n",
			"\n",
			"# The primary network interface\n",
			"allow-hotplug eth0\n",
			"iface eth0 inet dhcp\n",
			"\n"
		},
		--- Assist Functions
		--
		-- Get RootPath for this Board
	  	init	= function( self )
	  		-- require Dependencies
	  		-- Uboot
			self.boot		= require "reference.tools.bootloaders".arm
			self.boot_compiler	= {}
			self.boot_compiler[ 1 ]	= require "reference.tools.toolchains".arm.aarch32.baremetal
			-- Kernel
			self.kernel		= require ('reference.tools.kernels').mainline
			self.kernel_compiler 	= require "reference.tools.toolchains".arm.aarch32.linuxgnu
		end,
		mkimagePrepare = function( self, pwd, mediadisk )
			-- remove partitions
			self.os.execute( "parted " .. mediadisk .. " --script rm 1 rm 2 print" )
			-- Clean first part of the disk
			self.os.execute( "dd if=/dev/zero of=" .. mediadisk .. " bs=1M count=8 conv=sync && sleep 1" )
			-- Create new partition table, and new partitions
			self.os.execute( "parted " .. mediadisk .. " --script mklabel msdos mkpart primary ext2 1MB 101MB mkpart primary ext4 101MB 2000MB set 1 lba off set 2 lba off print" )
			--Write bootloader to sector 16, or 8K offset
			self.os.execute( "dd if=" .. pwd .. "/Builds/" .. self.rootpath .. "/" .. self.rootpath .. "_" .. self.bootImage[ 1 ] .. " of=" .. mediadisk .. " bs=512 seek=16 conv=sync" )
			-- Format partitions to new format types
			self.os.execute( "yes | mkfs.ext2 " .. mediadisk .. "1 && yes | mkfs.ext4 " .. mediadisk .. "2" )
		end,
		-- Get RootPath for this Board
	  	getRootPath = function( self )
			return self.rootpath
		end,
}
return Helios4
