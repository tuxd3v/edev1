-- Description for nanoPi Fire3 LTS Module
local Fire3 = {
		-- Require OS..
		os		= require "os",
	  	--toolchain	= nil,
		--bootloader	= nil,
		--kernel	= nil,
		rootpath	= "Fire3",

		-- CPU Properties
		cpu		= require "reference.hw.cpus".Nexell.S5P6818,

		--- Bootloader Properties
		--
		-- u-boot support
		boot		= nil,
		boot_compiler	= nil,
		-- ARM Trusted Execution Environment
		tee		= {
			-- ARM Trusted Firmware
			name		= "armtrustedfirmware",
			platform	= "sun50i_h6",
			stage		= "bl31",
			image		= "bl31.bin"
		},
		bootImage	= {
			""
		},
		bootdefconfig	= "nanopi_fire3_defconfig",
		env		= {},
		cmd		= {
			"console=ttySAC0,115200n8"
		},
		--- Kernel Properties
		--
		kernel			= nil,
		kernel_compiler 	= nil,
		kernelvers		= "current",
		arch			= "arm64",
		dtb			= "s5p6818-nanopi-fire3.dts",
		kerneldefconfig		= "s5p6818-nanopi-fire3_defconfig",
		-- applly on stable kernels..
		patchs		= {
				kernel = {
					current	= {
						"nexcell-support-v3.patch",
						"update-nanopi-fire3-support.patch"
					}
				},
				bootloader = {
					atf	= {},
					uboot	= {
						-- original patch
						--"update-nanopi-fire3-support.patch"
						"s5p6818-nanopi-fire3-support.patch"
					}
				}
		},
		---- User Space Properties
		---
		--- Modules config
		--
		modules = {
			"# /etc/modules: kernel modules to load at boot time.\n",
			"#\n",
			"# This file contains the names of kernel modules that should be loaded\n",
			"# at boot time, one per line. Lines beginning with \"#\" are ignored.\n",
			"zram num_devices=8"
		},
		--- ZRAM Config
		--
		zram_rules = {
			"KERNEL==\"zram0\", SUBSYSTEM==\"block\", DRIVER==\"\", ACTION==\"add\", ATTR{initstate}==\"0\", ATTR{comp_algorithm}=\"lz4\", ATTR{disksize}=\"96M\", RUN+=\"/sbin/mkswap $env{DEVNAME}\"\n",
			"KERNEL==\"zram1\", SUBSYSTEM==\"block\", DRIVER==\"\", ACTION==\"add\", ATTR{initstate}==\"0\", ATTR{comp_algorithm}=\"lz4\", ATTR{disksize}=\"96M\", RUN+=\"/sbin/mkswap $env{DEVNAME}\"\n",
			"KERNEL==\"zram2\", SUBSYSTEM==\"block\", DRIVER==\"\", ACTION==\"add\", ATTR{initstate}==\"0\", ATTR{comp_algorithm}=\"lz4\", ATTR{disksize}=\"96M\", RUN+=\"/sbin/mkswap $env{DEVNAME}\"\n",
			"KERNEL==\"zram3\", SUBSYSTEM==\"block\", DRIVER==\"\", ACTION==\"add\", ATTR{initstate}==\"0\", ATTR{comp_algorithm}=\"lz4\", ATTR{disksize}=\"96M\", RUN+=\"/sbin/mkswap $env{DEVNAME}\"\n",
			"KERNEL==\"zram4\", SUBSYSTEM==\"block\", DRIVER==\"\", ACTION==\"add\", ATTR{initstate}==\"0\", ATTR{comp_algorithm}=\"lz4\", ATTR{disksize}=\"96M\", RUN+=\"/sbin/mkswap $env{DEVNAME}\"\n",
			"KERNEL==\"zram5\", SUBSYSTEM==\"block\", DRIVER==\"\", ACTION==\"add\", ATTR{initstate}==\"0\", ATTR{comp_algorithm}=\"lz4\", ATTR{disksize}=\"96M\", RUN+=\"/sbin/mkswap $env{DEVNAME}\"\n",
			"KERNEL==\"zram6\", SUBSYSTEM==\"block\", DRIVER==\"\", ACTION==\"add\", ATTR{initstate}==\"0\", ATTR{comp_algorithm}=\"lz4\", ATTR{disksize}=\"96M\", RUN+=\"/sbin/mkswap $env{DEVNAME}\"\n",
			"KERNEL==\"zram7\", SUBSYSTEM==\"block\", DRIVER==\"\", ACTION==\"add\", ATTR{initstate}==\"0\", ATTR{comp_algorithm}=\"lz4\", ATTR{disksize}=\"96M\", RUN+=\"/sbin/mkswap $env{DEVNAME}\"\n"
		},
		--- FSTab Config
		--
		fstab = {
			"\n# <file system>	<dir>	<type>	<options>		<dump>		<pass>\n",
			"/dev/mmcblk0p1		/boot	ext2	defaults		0		2\n",
			"/dev/mmcblk0p2		/	ext4	defaults,noatime	0		1\n",
			"/dev/zram0 		none    swap    pri=1			0		0\n",
			"/dev/zram1 		none    swap    pri=2			0		0\n",
			"/dev/zram2 		none    swap    pri=3			0		0\n",
			"/dev/zram3 		none    swap    pri=4			0		0\n",
			"/dev/zram4 		none    swap    pri=5			0		0\n",
			"/dev/zram5 		none    swap    pri=6			0		0\n",
			"/dev/zram6 		none    swap    pri=8			0		0\n",
			"/dev/zram7 		none    swap    pri=7			0		0\n"
		},
		--- InitTab Config
		--
		inittab = {
			"\n# Login tty Via Serial Uart..\n",
			"T1:12345:respawn:/sbin/agetty -L ttySAC0 115200 vt100\n"
		},
		--- Interfaces Config
		--
		interfaces = {
			"\n# This file describes the network interfaces available on your system\n",
			"# and how to activate them. For more information, see interfaces(5).\n",
			"\n",
			"source /etc/network/interfaces.d/*\n",
			"\n",
			"# The loopback network interface\n",
			"auto lo\n",
			"iface lo inet loopback\n",
			"\n",
			"# The primary network interface\n",
			"allow-hotplug eth0\n",
			"iface eth0 inet dhcp\n",
			"\n"
		},
		--- Assist Functions
		--
		-- In Nexel cpus,
		disk = {
			-- BootLoader Section
			bootloader = {
				-- Boot1 u-boot-sunxi-with-spl.bin
				{
					-- Sector size in bytes [ Integer ]
					ssize	= 512,
					-- Seek number of Sectors[ Integer ]
					sseek	= 16,
					-- Size of the bootloader Area in Sectors[ Integer ]
					scount	= 2033,
					-- dd convert field[ String1,String2 .. ]
					conv	= "notrunc,fdatasync"
				}
			},
			-- parted script includes Partition table
			partitions = " --script mklabel msdos mkpart primary ext2 1MB 101MB mkpart primary ext4 101MB 1700MB set 1 lba off set 2 lba off print",
			-- Partitions Fyle System Formats
			formatfs = {
				ext2 = 1,
				ext4 = 2
			} 
		},
		-- Init Board function
	  	init	= function( self )
	  		-- require Dependencies
	  		-- Uboot
			self.boot		= require "reference.tools.bootloaders".arm
			self.boot_compiler	= {}
			self.boot_compiler[ 1 ]	= require "reference.tools.toolchains".arm.aarch64.baremetal
			-- Kernel
			self.kernel		= require ('reference.tools.kernels').mainline
			self.kernel_compiler 	= require "reference.tools.toolchains".arm.aarch64.linuxgnu
		end,
		-- Get RootPath for this Board
	  	getRootPath = function( self )
			return self.rootpath
		end
}
return Fire3
