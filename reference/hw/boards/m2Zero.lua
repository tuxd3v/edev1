-- Description for BananaPi m2 zero Module
local m2Zero = {
	-- Require OS..
	os		= require "os",
	rootpath	= "m2Zero",

	-- CPU Properties
	cpu		= require "reference.hw.cpus".AllWinner.H2,

	--- Bootloader Properties
	--
	-- u-boot support
	boot		= nil,
	boot_compiler	= nil,
	-- Trusted Execution Environment
	tee		= nil,
	bootImage	= {
		"u-boot-sunxi-with-spl.bin"
	},
	bootdefconfig	= "bananapi_m2_zero_defconfig",
	env		= {},
	cmd		= {
		"# Send debug info to uart, and also display\n",
		"setenv stdout serial,vga\n",
		"setenv stderr serial,vga\n",
		"setenv ethaddr \"B2 54 C3 7B 35 A8\"\n",
		"setenv devtype mmc\n",
		"setenv devnum 0\n",
		"\n",
		"# Print boot source\n",
		"itest.b *0x28 == 0x00 && echo \"U-boot loaded from SD\"\n",
		"itest.b *0x28 == 0x02 && echo \"U-boot loaded from eMMC or secondary SD\"\n",
		"itest.b *0x28 == 0x03 && echo \"U-boot loaded from SPI\"\n",
		"\n",
		"part uuid ${devtype} ${devnum}:2 rootfsuuid\n",
		"setenv bootargs \"earlyprintk net.ifnames=0 debug=on earlycon=uart8250,mmio32,0x1c28000 console=ttyS0,115200 cma=128M hdmi.audio=EDID:0 disp.screen0_output_mode=1680x1050p60 consoleblank=0 root=PARTUUID=${rootfsuuid} rw fsck.repair=yes rootwait usb-storage.quirks=0x2537:0x1066:u,0x2537:0x1068:u sunxi_ve_mem_reserve=0 sunxi_g2d_mem_reserve=0 sunxi_fb_mem_reserve=16 cgroup_enable=memory elevator=noop\"\n",
		"if ext2load ${devtype} ${devnum}:1 ${kernel_addr_r} vmlinuz; then\n",
		"  if ext4load ${devtype} ${devnum}:2 ${fdt_addr_r} usr/lib/linux-image-5.10.30/sun8i-h2-plus-bananapi-m2-zero.dtb; then\n",
		"    fdt addr ${fdt_addr_r}\n",
		"    fdt resize 65536\n",
		"    bootz ${kernel_addr_r} - ${fdt_addr_r}\n",
		"  fi;\n",
		"fi;\n"
	},
	--- Kernel Properties
	--
	kernel		= nil,
	kernel_compiler = nil,
	kernelvers	= "lts",
	arch		= "arm",
	dtb		= "sun8i-h2-plus-bananapi-m2-zero.dtb",
	kerneldefconfig	= "sun8i-h2-plus-bananapi-m2-zero_defconfig",
	-- Applly on stable kernels..
	patchs		= {
			kernel = {
				lts = {
					-- Add support for rfkill for bluetooth,,improve WIFI, hdmi, i2s, and hdmi sound..
					"sun8i-h2-plus-bananapi-m2-zero.patch"
				}
			},
			bootloader = {
				--uboot	= {}
			}
	},
	---- User Space Properties
	---
	--- Modules config
	--
	modules = {
		"# /etc/modules: kernel modules to load at boot time.\n",
		"#\n",
		"# This file contains the names of kernel modules that should be loaded\n",
		"# at boot time, one per line. Lines beginning with \"#\" are ignored.\n",
		"zram num_devices=4\n"
	},
	--- ZRAM Config
	--
	zram_rules = {
		"KERNEL==\"zram0\", SUBSYSTEM==\"block\", DRIVER==\"\", ACTION==\"add\", ATTR{initstate}==\"0\", ATTR{comp_algorithm}=\"lz4\", ATTR{disksize}=\"96M\", RUN+=\"/sbin/mkswap $env{DEVNAME}\"\n",
		"KERNEL==\"zram1\", SUBSYSTEM==\"block\", DRIVER==\"\", ACTION==\"add\", ATTR{initstate}==\"0\", ATTR{comp_algorithm}=\"lz4\", ATTR{disksize}=\"96M\", RUN+=\"/sbin/mkswap $env{DEVNAME}\"\n",
		"KERNEL==\"zram2\", SUBSYSTEM==\"block\", DRIVER==\"\", ACTION==\"add\", ATTR{initstate}==\"0\", ATTR{comp_algorithm}=\"lz4\", ATTR{disksize}=\"96M\", RUN+=\"/sbin/mkswap $env{DEVNAME}\"\n",
		"KERNEL==\"zram3\", SUBSYSTEM==\"block\", DRIVER==\"\", ACTION==\"add\", ATTR{initstate}==\"0\", ATTR{comp_algorithm}=\"lz4\", ATTR{disksize}=\"96M\", RUN+=\"/sbin/mkswap $env{DEVNAME}\"\n"
	},
	--- FSTab Config
	--
	fstab = {
		"\n# <file system>	<dir>	<type>	<options>		<dump>		<pass>\n",
		"/dev/mmcblk0p1		/boot	ext2	defaults		0		2\n",
		"/dev/mmcblk0p2		/	ext4	defaults,noatime	0		1\n",
		"/dev/zram0 		none    swap    pri=1			0		0\n",
		"/dev/zram1 		none    swap    pri=2			0		0\n",
		"/dev/zram2 		none    swap    pri=4			0		0\n",
		"/dev/zram3 		none    swap    pri=3			0		0\n"
	},
	--- InitTab Config
	--
	inittab = {
		"\n# Login tty Via Serial Uart..\n",
		"T1:12345:respawn:/sbin/agetty -L ttyS0 115200 vt100\n"
	},
	--- Interfaces Config
	--
	interfaces = {
		"\n# This file describes the network interfaces available on your system\n",
		"# and how to activate them. For more information, see interfaces(5).\n",
		"\n",
		"source /etc/network/interfaces.d/*\n",
		"\n",
		"# The loopback network interface\n",
		"auto lo\n",
		"iface lo inet loopback\n",
		"\n",
		"# The primary WIFI network interface\n",
		"allow-hotplug wlan0\n",
		"iface wlan0 inet dhcp\n",
		"   pre-up wpa_supplicant -B -D wext -i wlan0 -c /etc/wpa_supplicant/wpa_supplicant-wlan0.conf -f /var/log/wpa_supplicant.log\n",
		"   post-down killall -q wpa_supplicant\n",
		"\n",
		"# The primary network interface\n",
		"#allow-hotplug eth0\n",
		"#iface eth0 inet dhcp\n",
		"\n"
	},
	--- WPA_SUPPLICANT WIFI Support
	--
	--[[wpa_supplicant = {
    "# Input your ISO 3166-1 alpha-2 Country Code bellow\n",
    "country=pt\n",
    "update_config=1\n",
    "ctrl_interface=/var/run/wpa_supplicant\n",
    "\n",
    "network={\n",
    "       scan_ssid=1\n",
    "       ssid="MYSSID"\n",
    "       proto=RSN\n",
    "       key_mgmt=WPA-PSK\n",
    "       pairwise=CCMP\n",
    "       group=CCMP\n",
    "       # result bellow produced by 'wpa_passphrase MYSSID passphrase'\n",
    "       psk=paste_result_of_above_command_here\n",
    "}\n"
    },
	]]
	--- Assist Functions
	--
	-- In AllWiner, bootloader starts at 8192 bytes( 8KB) and end at 1048576 bytes ( 1MB )
	disk = {
		-- BootLoader Section
		bootloader = {
			-- Boot1 u-boot-sunxi-with-spl.bin
			{
				-- Sector size in bytes [ Integer ]
				ssize	= 512,
				-- Seek number of Sectors[ Integer ]
				sseek	= 16,
				-- Size of the bootloader Area in Sectors[ Integer ]
				scount	= 2033,
				-- dd convert field[ String1,String2 .. ]
				conv	= "notrunc,fdatasync"
			}
		},
		-- parted script includes Partition table
		partitions = " --script mklabel msdos mkpart primary ext2 1MB 101MB mkpart primary ext4 101MB 1700MB set 1 lba off set 2 lba off print",
		-- Partitions Fyle System Formats
		formatfs = {
			ext2 = 1,
			ext4 = 2
		}
	},
	-- Init Board function
  	init	= function( self )
  		-- require Dependencies
  		-- Uboot
		self.boot		        = require "reference.tools.bootloaders".arm
		self.boot_compiler	    = {}
		self.boot_compiler[ 1 ]	= require "reference.tools.toolchains".arm.aarch32.baremetal
		-- Kernel
		self.kernel		        = require ('reference.tools.kernels').mainline
		self.kernel_compiler 	= require "reference.tools.toolchains".arm.aarch32.linuxgnu
	end,
	-- Get RootPath for this Board
  	getRootPath = function( self )
		return self.rootpath
	end
}
return m2Zero
