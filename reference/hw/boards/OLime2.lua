-- Description for OrangePi One Plus Module
local OLime2 = {
		-- Require OS..
		os		= require "os",
	  	--toolchain	= nil,
		--bootloader	= nil,
		--kernel	= nil,
		rootpath	= "OLime2",

		-- CPU Properties
		cpu		= require "reference.hw.cpus".AllWinner.A20,

		--- Bootloader Properties
		--
		-- u-boot support
		boot		= nil,
		boot_compiler	= nil,
		-- Trusted Execution Environment
		tee		= nil,
		bootImage	= {
			"u-boot-sunxi-with-spl.bin"
		},
		bootdefconfig	= "A20-OLinuXino-Lime2_defconfig",
		env		= {},
		cmd		= {
				"# Send debug info to uart, and also display\n",
				"setenv stdout serial,vga\n",
				"setenv stderr serial,vga\n",
				"setenv bootargs debug=on console=ttyS0,115200 earlyprintk root=/dev/mmcblk0p2 rw rootfstype=ext4 rootwait fsck.repair=yes panic=15\n",
				"setenv fdtfile sun7i-a20-olinuxino-lime2.dtb\n",
				"load mmc 0:2 0x43000000 usr/lib/linux-image-5.8.11/${fdtfile}\n",
				"load mmc 0:1 0x42000000 vmlinuz\n",
				"bootz 0x42000000 - 0x43000000\n"
		},
		--- Kernel Properties
		--
		kernel			= nil,
		kernel_compiler 	= nil,
		kernelvers		= "current",
		arch			= "arm",
		dtb			= "sun7i-a20-olinuxino-lime2.dtb",
		kerneldefconfig		= "A20olinuxino_lime2_defconfig",
		-- applly on stable kernels..
		patchs		= {
				kernel = {
						current	= {
							"v3_Add_support_for_sun4i_HDMI_encoder_audio.patch"
						}
				},
				bootloader = {
					--uboot	= {}
				}
		},
		---- User Space Properties
		---
		--- Modules config
		--
		modules = {
			"# /etc/modules: kernel modules to load at boot time.\n",
			"#\n",
			"# This file contains the names of kernel modules that should be loaded\n",
			"# at boot time, one per line. Lines beginning with \"#\" are ignored.\n",
			"zram num_devices=2"
		},
		--- ZRAM Config
		--
		zram_rules = {
			"KERNEL==\"zram0\", SUBSYSTEM==\"block\", DRIVER==\"\", ACTION==\"add\", ATTR{initstate}==\"0\", ATTR{comp_algorithm}=\"lz4\", ATTR{disksize}=\"333M\", RUN+=\"/sbin/mkswap $env{DEVNAME}\"\n",
			"KERNEL==\"zram1\", SUBSYSTEM==\"block\", DRIVER==\"\", ACTION==\"add\", ATTR{initstate}==\"0\", ATTR{comp_algorithm}=\"lz4\", ATTR{disksize}=\"333M\", RUN+=\"/sbin/mkswap $env{DEVNAME}\""
		},
		--- FSTab Config
		--
		fstab = {
			"\n# <file system>	<dir>	<type>	<options>		<dump>		<pass>\n",
			"/dev/mmcblk0p1		/boot	ext2	defaults		0		2\n",
			"/dev/mmcblk0p2		/	ext4	defaults,noatime	0		1\n",
			"/dev/zram0 		none    swap    sw,pri=1		0		0\n",
			"/dev/zram1 		none    swap    sw,pri=2		0		0\n"
		},
		--- InitTab Config
		--
		inittab = {
			"\n# Login tty Via Serial Uart..\n",
			"T1:12345:respawn:/sbin/agetty -L ttyS0 115200 vt100\n"
		},
		--- Interfaces Config
		--
		interfaces = {
			"\n# This file describes the network interfaces available on your system\n",
			"# and how to activate them. For more information, see interfaces(5).\n",
			"\n",
			"source /etc/network/interfaces.d/*\n",
			"\n",
			"# The loopback network interface\n",
			"auto lo\n",
			"iface lo inet loopback\n",
			"\n",
			"# The primary network interface\n",
			"allow-hotplug eth0\n",
			"iface eth0 inet dhcp\n",
			"\n"
		},
		-- In AllWiner, bootloader starts at 8192 bytes( 8KB) and end at 1048576 bytes ( 1MB )
		disk = {
			-- BootLoader Section
			bootloader = {
				-- Boot1 u-boot-sunxi-with-spl.bin
				{
					-- Sector size in bytes [ Integer ]
					ssize	= 512,
					-- Seek number of Sectors[ Integer ]
					sseek	= 16,
					-- Size of the bootloader Area in Sectors[ Integer ]
					scount	= 2033,
					-- dd convert field[ String1,String2 .. ]
					conv	= "notrunc,fdatasync"
				}
			},
			-- parted script includes Partition table
			partitions = " --script mklabel msdos mkpart primary ext2 1MB 101MB mkpart primary ext4 101MB 1700MB set 1 lba off set 2 lba off print",
			-- Partitions Fyle System Formats
			formatfs = {
				ext2 = 1,
				ext4 = 2
			} 
		},
		--- Assist Functions
		--
		-- Get RootPath for this Board
	  	init	= function( self )
	  		-- require Dependencies
	  		-- Uboot
			self.boot		= require "reference.tools.bootloaders".arm
			self.boot_compiler	= {}
			self.boot_compiler[ 1 ]	= require "reference.tools.toolchains".arm.aarch32.baremetal
			-- Kernel
			self.kernel		= require ('reference.tools.kernels').mainline
			self.kernel_compiler 	= require "reference.tools.toolchains".arm.aarch32.linuxgnu
		end,
		-- Get RootPath for this Board
	  	getRootPath = function( self )
			return self.rootpath
		end,
}
return OLime2
