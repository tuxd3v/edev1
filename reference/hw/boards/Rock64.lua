-- Description for Pine64 Rock64 Module
local Rock64 = {
		-- Require OS..
		os		= require "os",
	  	--toolchain	= nil,
		--bootloader	= nil,
		--kernel		= nil,
		rootpath	= "Rock64",

		-- CPU Properties
		cpu		= require "reference.hw.cpus".RockChip.RK3328,

		--- Bootloader Properties
		--
		-- u-boot support since v2018.09 release
		boot		= nil,
		boot_compiler	= nil,
		-- Trusted Execution Environment
		tee		= {
			-- ARM Trusted Firmware
			platform	= "rk3328",
			stage		= "bl31",
			image		= "bl31.elf"
		},
		bootImage	= {
			"idbloader.img",
			"u-boot.itb"
		},
		bootdefconfig	= "rock64-rk3328_defconfig",
		env		= {
				"console=ttyS0,115200n8 no_console_suspend\n",
				"kernel_filename=orangepi/uImage\n",
				"initrd_filename=initrd.img\n",
				"root=/dev/mmcblk1p2\n",
				"bootdelay=6"
		},
		cmd		= {
				"# setup MAC address\n",
				"setenv ethaddr 7E:8C:70:59:B5:70\n",
		},
		--- Kernel Properties
		--
		kernel			= nil,
		kernel_compiler 	= nil,
		kernelvers		= "current",
		arch			= "arm64",
		dtb			= "rk3328-rock64.dtb",
		kerneldefconfig		= "rk3328-rock64_defconfig",
		-- applly on stable kernels..
		patchs		= {
				kernel = {
						current	= {}
				},
				bootloader = {
					--atf	= {},
					--uboot	= {}
				}
		},
		---- User Space Properties
		---
		--- Modules config
		--
		modules = {
			"# /etc/modules: kernel modules to load at boot time.\n",
			"#\n",
			"# This file contains the names of kernel modules that should be loaded\n",
			"# at boot time, one per line. Lines beginning with \"#\" are ignored.\n",
			"zram num_devices=4"
		},
		--- ZRAM Config
		--
		zram_rules = {
			"KERNEL==\"zram0\", SUBSYSTEM==\"block\", DRIVER==\"\", ACTION==\"add\", ATTR{initstate}==\"0\", ATTR{comp_algorithm}=\"lz4\", ATTR{disksize}=\"192M\", RUN+=\"/sbin/mkswap $env{DEVNAME}\"\n",
			"KERNEL==\"zram1\", SUBSYSTEM==\"block\", DRIVER==\"\", ACTION==\"add\", ATTR{initstate}==\"0\", ATTR{comp_algorithm}=\"lz4\", ATTR{disksize}=\"192M\", RUN+=\"/sbin/mkswap $env{DEVNAME}\"\n",
			"KERNEL==\"zram2\", SUBSYSTEM==\"block\", DRIVER==\"\", ACTION==\"add\", ATTR{initstate}==\"0\", ATTR{comp_algorithm}=\"lz4\", ATTR{disksize}=\"192M\", RUN+=\"/sbin/mkswap $env{DEVNAME}\"\n",
			"KERNEL==\"zram3\", SUBSYSTEM==\"block\", DRIVER==\"\", ACTION==\"add\", ATTR{initstate}==\"0\", ATTR{comp_algorithm}=\"lz4\", ATTR{disksize}=\"192M\", RUN+=\"/sbin/mkswap $env{DEVNAME}\"\n"
		},
		--- FSTab Config
		--
		fstab = {
			"\n# <file system>	<dir>	<type>	<options>		<dump>		<pass>\n",
			"/dev/mmcblk1p4		/boot	ext2	defaults		0		2\n",
			"/dev/mmcblk1p5		/	ext4	defaults,noatime	0		1\n",
			"/dev/zram0 		none    swap    pri=1			0		0\n",
			"/dev/zram1 		none    swap    pri=2			0		0\n",
			"/dev/zram2 		none    swap    pri=4			0		0\n",
			"/dev/zram3 		none    swap    pri=3			0		0\n"
		},
		--- InitTab Config
		--
		inittab = {
			"\n# Login tty Via Serial Uart..\n",
			"T1:12345:respawn:/sbin/agetty -L ttyS2 1500000 vt100\n"
		},
		--- Interfaces Config
		--
		interfaces = {
			"\n# This file describes the network interfaces available on your system\n",
			"# and how to activate them. For more information, see interfaces(5).\n",
			"\n",
			"source /etc/network/interfaces.d/*\n",
			"\n",
			"# The loopback network interface\n",
			"auto lo\n",
			"iface lo inet loopback\n",
			"\n",
			"# The primary network interface\n",
			"allow-hotplug eth0\n",
			"iface eth0 inet dhcp\n",
			"\n"
		},
		--- Assist Functions
		--
		-- In RockChip, bootloader ...
		disk = {
			-- BootLoader Section
			bootloader = {
				-- Boot1 idbloader
				{
					-- Sector size in bytes [ Integer ]
					ssize	= 512,
					-- Seek number of Sectors[ Integer ]
					sseek	= 64,
					-- Size of the bootloader Area in Sectors[ Integer ]
					scount	= 8000,
					-- dd convert field[ String1,String2 .. ]
					conv	= "notrunc,fdatasync"
				},
				-- Boot2 uboot
				{
					-- Sector size in bytes [ Integer ]
					ssize	= 512,
					-- Seek number of Sectors[ Integer ]
					sseek	= 16384,
					-- Size of the bootloader Area in Sectors[ Integer ]
					scount	= 8192,
					-- dd convert field[ String1,String2 .. ]
					conv	= "notrunc,fdatasync"
				},
				-- Boot3 none
				{
					-- Sector size in bytes [ Integer ]
					ssize	= 512,
					-- Seek number of Sectors[ Integer ]
					sseek	= 24576,
					-- Size of the bootloader Area in Sectors[ Integer ]
					scount	= 8192,
					-- dd convert field[ String1,String2 .. ]
					conv	= "notrunc,fdatasync"
				}
			},
			-- parted script includes Partition table
			partitions = " --script mklabel gpt unit s mkpart idbloader 64 8063 mkpart uboot 16384 24575 mkpart none 24576 32767 mkpart boot 32768 262143 mkpart rootfs 262144 3743744 set 4 boot on set 4 esp on print",
			-- Partitions Fyle System Formats
			formatfs = {
				ext2 = 4,
				ext4 = 5
			} 
		},
		-- Get RootPath for this Board
	  	init	= function( self )
	  		-- require Dependencies
	  		-- Uboot
			self.boot		= require "reference.tools.bootloaders".arm
			self.boot_compiler	= {}
			-- 1 - To Compile uboot
			-- 2 - To compile Arm Trusted Firmware- cortex M0 pmu driver
			self.boot_compiler[ 1 ]	= require "reference.tools.toolchains".arm.aarch64.baremetal
			self.boot_compiler[ 2 ]	= require "reference.tools.toolchains".arm.aarch32.baremetal
			-- Kernel
			self.kernel		= require ('reference.tools.kernels').mainline
			self.kernel_compiler 	= require "reference.tools.toolchains".arm.aarch64.linuxgnu
		end,
		--	-- Copy rootfs to SDCard
		--	"cd $PWD/Builds/" .. self.rootpath .. "_" .. os_base .. "/rootfs && mount /dev/sdb2 /mnt && cp -pr ./* /mnt && sync &&  umount /mnt;",
		--	-- Copy Kernel Modules,headers
		--	"cd $PWD/Builds/" .. self.rootpath .. "_" .. os_base .. "/rootfs && sudo chown root: -R " .. self.rootpath .. "{_dtbs/boot,_headers/usr,_KImage/boot,_modules/lib}",
		--},
		-- Get RootPath for this Board
	  	getRootPath = function( self )
			return self.rootpath
		end
}
return Rock64
