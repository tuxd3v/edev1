-- Description for Graphic drivers Module
local gpus = {
		-- Require OS..
		os		= require "os",
	  	toolchain	= nil,
		bootloader	= nil,
		kernel		= nil,
		rootpath	= "Graphics",
		-- Get RootPath for this Board
	  	getRootPath = function( self )
			print( self.rootpath )
		end,

		arm = {
			sunxi-utgard = {
				rootpath	= "sunxi-utgard"
			},
			sunxi-midgard = {
				rootpath	= "sunxi-midgard"
				url		= "https://github.com/mripard/sunxi-mali.git",
				---- Get sunxi-Mali stack
				getCrossToolChain		= function ( self,  base_path )
					local path = base_path .. "/Graphics"
					print( "\27[1;35m---- DownLoad Gnu/Linux Cross Compiller Toolchain, Version< " .. self.version .. " >\27[0m!" )
					print( "\27[1;35m---  Files to DownLoad\27[0m:\n\27[1;35m--   \27[0m" .. self.content0 .. "\n\27[1;35m--   \27[0m" .. self.content1 .. "\27[0m" )
					local EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.rename( path, path )
					if(  EXIT_CODE == nil )
					then
						os.execute( "mkdir " .. path )
					end
					EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "cd " .. path .. " && curl -L -X GET -O " .. self.url .. "/" .. self.content0 .. " -O " .. self.url .. "/" ..  self.content1 )
					if ( EXIT_CODE == true )
					then
						print( "\27[1;35m--  \27[0mDownLoad of Gnu/Linux Cross Compiller Toolchain ended \27[1;32mSUCCESSFULLY\27[0m.. !!\n" )
					else
						print( "\27[1;31m--  \27[0mSomething went wrong.. in Gnu/Linux Cross Compiller Toolchain Installation, \27[1;31mFAILLED\27[0m !!\n" )
					end
					return EXIT_SIGNAL
				end
			},
		},
		-- Get RootPath for this Board
	  	init	= function( self )
	  		-- require Dependencies
	  		self.toolchain		= require "reference.toolchains"
			self.bootloader		= require "reference.bootloaders"
			self.kernel		= require ('reference.kernels')
	  		-- Uboot
			self.boot		= self.bootloader.arm
			self.boot_compiler	= {}
			self.boot_compiler[ 1 ]	= self.toolchain.arm.aarch32.baremetal
			-- Kernel
			self.kernel		= self.kernel.mainline
			self.kernel_compiler 	= self.toolchain.arm.aarch32.linuxgnu
		end
}
return gpus