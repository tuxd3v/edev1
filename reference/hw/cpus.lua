local cpus = {
	--- AllWinner Table
	--
	AllWinner = {
		--- A20 Boards Table
		--
		A20	= {
			arch = {
				os		= "armhf",
				quemu_arch	= "arm",
				toolchain	= "aarch32",
				march		= "armv7-a+simd+vfpv4",
				mtune		= "cortex-a7",
				CFLAGS		= "-Os -march=armv7-a+simd+vfpv4 -mtune=cortex-a7 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen",
				CXXFLAGS	= "-Os -march=armv7-a+simd+vfpv4 -mtune=cortex-a7 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen"
			}
		},
		--- H2 Boards Table
		--
		H2	= {
			arch = {
				os		= "armhf",
				quemu_arch	= "arm",
				toolchain	= "aarch32",
				march		= "armv7-a+simd+vfpv4",
				mtune		= "cortex-a7",
				CFLAGS		= "-O3 -march=armv7-a -mtune=cortex-a7 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen",
				CXXFLAGS	= "-O3 -march=armv7-a -mtune=cortex-a7 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen"
			}
		},
		--- H3 Boards Table
		--
		H3	= {
			arch = {
				os		= "armhf",
				quemu_arch	= "arm",
				toolchain	= "aarch32",
				march		= "armv7-a+simd+vfpv4",
				mtune		= "cortex-a7",
				CFLAGS		= "-O3 -march=armv7-a -mtune=cortex-a7 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen",
				CXXFLAGS	= "-O3 -march=armv7-a -mtune=cortex-a7 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen"
			}
		},
		--- A64 Boards Table
		--
		A64	= {
			arch = {
				os		= "arm64",
				quemu_arch	= "aarch64",
				toolchain	= "aarch64",
				march		= "armv8-a+simd+crypto+crc",
				mtune		= "cortex-a53",
				CFLAGS		= "-O3 -march=armv8-a+simd+crypto+crc -mtune=cortex-a53 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen",
				CXXFLAGS	= "-O3 -march=armv8-a+simd+crypto+crc -mtune=cortex-a53 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen"
			}
		},
		--- H5 Boards Table
		--
		H5	= {
			arch = {
				os		= "arm64",
				quemu_arch	= "aarch64",
				toolchain	= "aarch64",
				march		= "armv8-a+simd+crypto+crc",
				mtune		= "cortex-a53",
				CFLAGS		= "-O3 -march=armv8-a+simd+crypto+crc -mtune=cortex-a53 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen",
				CXXFLAGS	= "-O3 -march=armv8-a+simd+crypto+crc -mtune=cortex-a53 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen"
			}
		},
		--- H6 Boards Table
		--
		H6	= {
			arch = {
				os		= "arm64",
				quemu_arch	= "aarch64",
				toolchain	= "aarch64",
				march		= "armv8-a+simd+crypto+crc",
				mtune		= "cortex-a53",
				CFLAGS		= "-O3 -march=armv8-a+simd+crypto+crc -mtune=cortex-a53 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen",
				CXXFLAGS	= "-O3 -march=armv8-a+simd+crypto+crc -mtune=cortex-a53 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen"
			}
		}
	},
	--- Amd Table
	--
	Amd = {
		-- GX-412TC Boards Table
		GX412TC	= {
			-- Jaguar Familly 16h( btver2: MOVBE, F16C, BMI, AVX, PCLMUL, AES, SSE4.2, SSE4.1, CX16, ABM, SSE4A, SSSE3, SSE3, SSE2, SSE, MMX )
			arch	= {
				os		= "amd64",
				quemu_arch	= nil,
				toolchain	= "amd64",
				march		= "btver2+mmx+sse+sse2+sse3+ssse3+sse4a+cx16+abm+sse4.1+sse4.2+movbe+f16c+bmi+pcl_mul+avx+aes",
				mtune		= "btver2",
				CFLAGS		= "-O3 march=btver2+mmx+sse+sse2+sse3+ssse3+sse4a+cx16+abm+sse4.1+sse4.2+movbe+f16c+bmi+pcl_mul+avx+aes -mtune=btver2 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion -ftree-vectorize -foptimize-strlen",
				CXXFLAGS	= "-O3 march=btver2+mmx+sse+sse2+sse3+ssse3+sse4a+cx16+abm+sse4.1+sse4.2+movbe+f16c+bmi+pcl_mul+avx+aes -mtune=btver2 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion -ftree-vectorize -foptimize-strlen"
			}
		}
	},
	--- AmLogic Table
	--
	AmLogic = {
		-- S905X3 Boards Table
		S905X3	= {
			arch = {
				os		= "arm64",
				quemu_arch	= "aarch64",
				toolchain	= "aarch64",
				march		= "armv8-a+simd+crypto+crc",
				mtune		= "cortex-a55",
				CFLAGS		= "-O3 -march=armv8-a+simd+crypto+crc -mtune=cortex-a55 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen",
				CXXFLAGS	= "-O3 -march=armv8-a+simd+crypto+crc -mtune=cortex-a55 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen"
			}
		},
		-- A311D Boards Table
		A311D	= {
			arch	= {
				os		= "arm64",
				quemu_arch	= "aarch64",
				toolchain	= "aarch64",
				march		= "armv8-a+simd+crypto+crc",
				mtune		= "cortex-a73.cortex-a53",
				CFLAGS		= "-O3 -march=armv8-a+simd+crypto+crc -mtune=cortex-a73.cortex-a53 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen",
				CXXFLAGS	= "-O3 -march=armv8-a+simd+crypto+crc -mtune=cortex-a73.cortex-a53 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen"
			}
		}
	},
	--- Broadcom Table
	--
	Broadcom = {
		-- BCM2835 Boards Table
		BCM2835	= {
			arch = {
				os		= "armel",
				quemu_arch	= "arm",
				toolchain	= "arm32",
				march		= "armv6+fp",
				mtune		= "arm1176jzf-s",
				CFLAGS		= "-Os -march=armv6+fp -mtune=arm1176jzf-s -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen",
				CXXFLAGS	= "-Os -march=armv6+fp -mtune=arm1176jzf-s -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen"
			}
		},
		-- BCM2836 Boards Table
		BCM2836	= {
			arch = {
				os		= "armhf",
				quemu_arch	= "arm",
				toolchain	= "aarch32",
				march		= "armv7-a",
				mtune		= "cortex-a7",
				CFLAGS		= "-O3 -march=armv7-a -mtune=cortex-a7 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen",
				CXXFLAGS	= "-O3 -march=armv7-a -mtune=cortex-a7 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen"
			}
		},
		-- BCM2837 Boards Table
		BCM2837	= {
			arch = {
				os		= "arm64",
				quemu_arch	= "aarch64",
				toolchain	= "aarch64",
				march		= "armv8-a+simd+crypto+crc",
				mtune		= "cortex-a53",
				CFLAGS		= "-O3 -march=armv8-a+simd+crypto+crc -mtune=cortex-a53 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen",
				CXXFLAGS	= "-O3 -march=armv8-a+simd+crypto+crc -mtune=cortex-a53 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen"
			}
		},
		-- BCM2711 Boards Table
		BCM2711	= {
			arch = {
				os		= "arm64",
				quemu_arch	= "aarch64",
				toolchain	= "aarch64",
				march		= "armv8-a+simd+crypto+crc",
				mtune		= "cortex-a72",
				CFLAGS		= "-O3 -march=armv8-a+simd+crypto+crc -mtune=cortex-a72 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen",
				CXXFLAGS	= "-O3 -march=armv8-a+simd+crypto+crc -mtune=cortex-a72 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen"
			}
		}
	},
	--- Ingenic Mips Table
	--
	Ingenic = {
		-- JZ4780 Boards Table
		 JZ4780 = {
			arch	= {
				os		= "mipsel",
				quemu_arch	= "mipsel",
				toolchain	= "mips32",
				march		= "MIPS32r2",
				mtune		= "MIPS32r2",
				CFLAGS		= "-O3 -march=MIPS32r2 -mtune=MIPS32r2 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen",
				CXXFLAGS	= "-O3 -march=MIPS32r2 -mtune=MIPS32r2 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen"
			}
		}
	},	
	--- Marvel Table
	--
	Marvel = {
		-- ARMADA 8K Family
		ARMADA8040 = {
			arch = {
				os		= "arm64",
				quemu_arch	= "aarch64",
				toolchain	= "aarch64",
				march		= "armv8-a+simd+crypto+crc",
				mtune		= "cortex-A72",
				CFLAGS		= "-O3 -march=armv8-a+simd+crypto+crc -mtune=cortex-a72 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen",
				CXXFLAGS	= "-O3 -march=armv8-a+simd+crypto+crc -mtune=cortex-a72 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen"
			}
		},
		-- ARMADA388 Boards Table
		ARMADA388 = {
			arch = {
				os		= "armhf",
				quemu_arch	= "arm",
				toolchain	= "aarch32",
				march		= "armv7-a",
				mtune		= "cortex-a9",
				CFLAGS		= "-O3 -march=armv7-a -mtune=cortex-a9 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen",
				CXXFLAGS	= "-O3 -march=armv7-a -mtune=cortex-a9 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen"
			}
		}
	},
	--- MindSpeed Table
	--
	MindSpeed = {
		-- Concerto2K Boards Table
		ComcertoC2K = {
			arch = {
				os		= "armhf",
				quemu_arch	= "arm",
				toolchain	= "aarch32",
				march		= "armv7-a",
				mtune		= "cortex-a7",
				CFLAGS		= "-O3 -march=armv7-a -mtune=cortex-a7 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen",
				CXXFLAGS	= "-O3 -march=armv7-a -mtune=cortex-a7 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen"
			}
		}
	},
	--- NEXELL Table
	--
	Nexell = {
		-- S5P6818 Boards Table
		S5P6818	= {
			arch = {
				os		= "arm64",
				quemu_arch	= "aarch64",
				toolchain	= "aarch64",
				march		= "armv8-a+simd+crypto+crc",
				mtune		= "cortex-a53",
				CFLAGS		= "-O3 -march=armv8-a+simd+crypto+crc -mtune=cortex-a53 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen",
				CXXFLAGS	= "-O3 -march=armv8-a+simd+crypto+crc -mtune=cortex-a53 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen"
			}
		}
	},
	--- NVIDIA Table
	--
	Nvidia = {
		-- TM660M Boards Table
		TM660M	= {
			arch = {
				os		= "arm64",
				quemu_arch	= "aarch64",
				toolchain	= "aarch64",
				march		= "armv8-a+simd+crypto+crc",
				mtune		= "cortex-a57",
				CFLAGS		= "-O3 -march=armv8-a+simd+crypto+crc -mtune=cortex-a57 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen",
				CXXFLAGS	= "-O3 -march=armv8-a+simd+crypto+crc -mtune=cortex-a57 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen"
			}
		}
	},
	--- NXP Table
	--
	NXP = {
		-- LS1088A Boards Table
		LS1088A	= {
			arch = {
				os		= "arm64",
				quemu_arch	= "aarch64",
				toolchain	= "aarch64",
				march		= "armv8-a+simd+crypto+crc",
				mtune		= "Cortex-A53",
				CFLAGS		= "-O3 -march=armv8-a+simd+crypto+crc -mtune=Cortex-A53 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen",
				CXXFLAGS	= "-O3 -march=armv8-a+simd+crypto+crc -mtune=Cortex-A53 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen"
			}
		},
		-- LS1046A Boards Table
		LS1046A	= {
			arch = {
				os		= "arm64",
				quemu_arch	= "aarch64",
				toolchain	= "aarch64",
				march		= "armv8-a+simd+crypto+crc",
				mtune		= "Cortex-A72",
				CFLAGS		= "-O3 -march=armv8-a+simd+crypto+crc -mtune=Cortex-A72 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen",
				CXXFLAGS	= "-O3 -march=armv8-a+simd+crypto+crc -mtune=Cortex-A72 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen"
			}
		},
		-- LS2160A Boards Table
		LS2160A	= {
			arch = {
				os		= "arm64",
				quemu_arch	= "aarch64",
				toolchain	= "aarch64",
				march		= "armv8-a+simd+crypto+crc",
				mtune		= "Cortex-A72",
				CFLAGS		= "-O3 -march=armv8-a+simd+crypto+crc -mtune=Cortex-A72 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen",
				CXXFLAGS	= "-O3 -march=armv8-a+simd+crypto+crc -mtune=Cortex-A72 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen"
			}
		},
		-- T2080 Boards Table
		-- TODO: impelement arch, tune, toolchain, etc - update-binfmts --display|grep -EA 7 "ppc64.*enabled" --color
		T2080	= {
			arch = {
				os		= "ppc64",
				quemu_arch	= "ppc64",
				toolchain	= "ppc64",
				march		= "e6500+altivec",
				mtune		= "e6500",
				CFLAGS		= "-O3 -march=e6500+altivec -mtune=e6500 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen",
				CXXFLAGS	= "-O3 -march=e6500+altivec -mtune=e6500 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen"
			}
		}
	},
	--- RockChip Table
	--
	RockChip = {
		--- RK3328 Boards Table
		RK3328	= {
			arch = {
				os		= "arm64",
				quemu_arch	= "aarch64",
				toolchain	= "aarch64",
				march		= "armv8-a+simd+crypto+crc",
				mtune		= "cortex-a53",
				CFLAGS		= "-O3 -march=armv8-a+simd+crypto+crc -mtune=cortex-a53 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen",
				CXXFLAGS	= "-O3 -march=armv8-a+simd+crypto+crc -mtune=cortex-a53 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen"
			}
		},
		-- RK3399 Boards Table
		RK3399	= {
			arch	= {
				os		= "arm64",
				quemu_arch	= "aarch64",
				toolchain	= "aarch64",
				march		= "armv8-a+simd+crypto+crc",
				mtune		= "cortex-a72.cortex-a53",
				CFLAGS		= "-O3 -march=armv8-a+simd+crypto+crc -mtune=cortex-a72.cortex-a53 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen",
				CXXFLAGS	= "-O3 -march=armv8-a+simd+crypto+crc -mtune=cortex-a72.cortex-a53 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen"
			}
		}
	},
	--- BaikalElectronics Mips Table
	--
	BaikalElectronics = {
		-- BaikalT1 Boards Table
		BaikalT1 = {
			arch	= {
				os		= "mipsel",
				quemu_arch	= "mipsel",
				toolchain	= "mips32",
				march		= "mips32r5",
				mtune		= "mips32r5",
				CFLAGS		= "-O3 -march=mips32r5 -mtune=mips32r5 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen",
				CXXFLAGS	= "-O3 -march=mips32r5 -mtune=mips32r5 -finline-functions -foptimize-sibling-calls -funroll-loops -faggressive-loop-optimizations -fif-conversion  -ftree-vectorize -foptimize-strlen"
			}
		}
	}
}
return cpus
