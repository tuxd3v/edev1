--- Architectures Module
--
local boards = {
	-- dependencies
	mediadisk = require "reference.media.mediadisk",
	current	= nil,

	--- Init Board Function
	--
	InitBoard		= function( self, board )
		-- Load Correct Board Module
		self.current = require ( "reference.hw.boards." .. board )
		
		---Initialize bootloader,kernel,toolchains for bootloader and for Kernel
		self.current:init()
	end,
	--- Download and Install BootLoader Compiler
	--
	installBootloadertoolchain = function ( self, pwd )
		local EXIT_SIGNAL = 0
		-- for each cross toolchain in boot_compiler's table, get that toolchain
		for k,v in ipairs( self.current.boot_compiler )
		do
			if ( v:getCrossToolChain( pwd ) ~= 0 )
			then
				EXIT_SIGNAL	= 1
			end
		end
		return EXIT_SIGNAL
	end,
	--- Download and Install Kernel Compiler
	--
	installKerneltoolchain	= function ( self, pwd )
		--selection = self[ board ]
		return self.current.kernel_compiler:getCrossToolChain( pwd )
	end,
	--- BootLoader Functions
	--
	buildBootLoader		= function ( self, Kconfig, do_patchs, pwd, nproc )
		return self.current.boot:buildBootLoader( pwd, self.current.rootpath, self.current.tee, self.current.bootdefconfig, self.current.boot_compiler[ 1 ].prefix, Kconfig, self.current.bootImage, self.current.patchs.bootloader, do_patchs, nproc )
	end,
	--- Kernel Functions
	--
	buildKernel		= function ( self, Kconfig, do_patchs, pwd, nproc )
		return self.current.kernel:buildKernel( pwd, self.current.rootpath, self.current.kernelvers, self.current.arch, self.current.kerneldefconfig, self.current.dtb, self.current.kernel_compiler.prefix, Kconfig, self.current.patchs.kernel, do_patchs, nproc )
	end,
	--- Media Disk Functions
	--
	mediaCard		= function ( self, pwd, values )
	
		-- Verify if 'Select' button was really pressed, and if is to Generate boot.cmd )
		if( values[ 1 ] == "Select" and values[ 3 ] == true )
		then
			self.mediadisk:genBootcmd( pwd, self.current:getRootPath(), self.current.cmd )
		end
		-- Verify if 'Select' button was really pressed, and if disk name is at least 8 chars( /dev/sd? )
		if( values[ 1 ] == "Select" and values[ 2 ]:len() >= 8 )
		then
			--- Prepare SDCard with bootloader and partitions..
			-- Erase the bootloader zones in disk( clean )
			if( values[ 4 ] == true )
			then
				self.mediadisk:eraseBLoader( values[ 2 ], self.current.disk.bootloader )
			end
			-- Write the compiled bootloader to the correct place in disk..
			if( values[ 5 ] == true )
			then
				self.mediadisk:writeBLoader( pwd, self.current:getRootPath(), values[ 2 ], self.current.disk.bootloader, self.current.bootImage )
			end
			-- Create the partitions.. including partition table on disk
			if( values[ 6 ] == true )
			then
				self.mediadisk:partDisk( values[ 2 ], self.current.disk.partitions )
			end
			-- Format the above created partitions
			if( values[ 7 ] == true )
			then
				self.mediadisk:formatFS( values[ 2 ], self.current.disk.formatfs )
			end

			--- Create SDCard Operating System Image..
			-- Write missing parts( /boot, rootfs ) to the SDCARD
			if( values[ 8 ] == true )
			then
				-- TODO: Implement functionality to build a OS Image
			end
		end
	end
}
return boards
