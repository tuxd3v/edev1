
local toolchains = {
	rootpath	= "ToolChains",
	getRootPath	= function( self )
		print( self.rootpath )
	end,
	arm = {
		arm32 = {
			-- ToolChain arm32 for MCUs softfloat BareMetal Litle-Endian
			baremetal0	={
				version		= "10-2020q4-major",
				arch		= "arm32",
				url		= "https://developer.arm.com/-/media/Files/downloads/gnu-rm/10-2020q4",
				content0	= "gcc-arm-none-eabi-10-2020-q4-major-x86_64-linux.tar.bz2",
				rootpath	= "gcc-arm-none-eabi-10-2020-q4-major",
				prefix		= "arm-none-eabi-",
				--- Baremetal Functions
				--
				---- Get Baremetal Cross Compilling ToolChain
				getCrossToolChain		= function ( self,  base_path )
					local path = base_path .. "/ToolChains"
					print( "\27[1;35m---- DownLoad Baremetal0 Cross Compiller Toolchain, Version< " .. self.version .. " >\27[0m!" )
					print( "\27[1;35m---  Files to DownLoad\27[0m:\n\27[1;35m--   \27[0m" .. self.content0 )
					local EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.rename( path, path )
					if(  EXIT_CODE == nil )
					then
						os.execute( "mkdir " .. path )
					end
					-- Get toolchain only if it doesn't exist already..
					EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.rename( path .. "/" .. self.rootpath, path .. "/" .. self.rootpath )
					if(  EXIT_CODE == nil )
					then
						EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "cd " .. path .. " && curl -L -X GET -O " .. self.url .. "/" .. self.content0 )
						if ( EXIT_CODE == true )
						then
							print( "\27[1;35m--   \27[0mDownLoad of Baremetal0 Cross Compiller Toolchain ended \27[1;32mSUCCESSFULLY\27[0m.. !!\n" )
							-- Finally Prepare the toolchain ( uncompress it.. )
							EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "cd " .. path .. " && tar -xjf " .. self.content0 )
							if ( EXIT_CODE == true )
							then
								print( "\27[1;35m--   \27[0mBaremetal0 Cross Compiller Toolchain Instalation ended \27[1;32mSUCCESSFULLY\27[0m.. !!\n" )
								
							else
								print( "\27[1;31m--   \27[0mSomething went wrong.. in Baremetal0 Cross Compiller Toolchain Installation, \27[1;31mFAILLED\27[0m !!\n" )
							end
						else
							print( "\27[1;31m--   \27[0mSomething went wrong.. in Baremetal0 Cross Compiller Toolchain DownLoad, \27[1;31mFAILLED\27[0m !!\n" )
						end
					else
						-- No need to download the toolchain has it already exists..
						print( "\27[1;35m--   \27[0mBaremetal0 Cross Compiller Toolchain already in place.. \27[1;32mOK\27[0m.. !!\n" )
						EXIT_SIGNAL = 0
					end

					return EXIT_SIGNAL
				end
			},
			-- ToolChain arm32 softfloat BareMetal Litle-Endian
			baremetal	= {
				---- amd64
				version		= "8.3-2019.03",
				arch		= "aarch32",
				url		= "https://developer.arm.com/-/media/Files/downloads/gnu-a/8.3-2019.03/binrel",
				content0	= "gcc-arm-8.3-2019.03-x86_64-arm-eabi.tar.xz",
				content1	= "gcc-arm-8.3-2019.03-x86_64-arm-eabi.tar.xz.asc",
				rootpath	= "gcc-arm-8.3-2019.03-x86_64-arm-eabi",
				prefix		= "arm-eabi-",
				-- i686
				--version		= "7.5.0-2019.12",
				--arch		= "aarch32",
				--url		= "https://releases.linaro.org/components/toolchain/binaries/latest-7/arm-eabi",
				--content0	= "gcc-linaro-7.5.0-2019.12-i686_arm-eabi.tar.xz",
				--content1	= "gcc-linaro-7.5.0-2019.12-i686_arm-eabi.tar.xz.asc",
				--rootpath	= "gcc-linaro-7.5.0-2019.12-i686_arm-eabi",
				--prefix		= "arm-eabi-",
				--- Baremetal Functions
				--
				---- Get Baremetal Cross Compilling ToolChain
				getCrossToolChain		= function ( self,  base_path )
					local path = base_path .. "/ToolChains"
					print( "\27[1;35m---- DownLoad Baremetal Cross Compiller Toolchain, Version< " .. self.version .. " >\27[0m!" )
					print( "\27[1;35m---  Files to DownLoad\27[0m:\n\27[1;35m--   \27[0m" .. self.content0 .. "\n\27[1;35m--   \27[0m" .. self.content1 .. "\27[0m" )
					local EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.rename( path, path )
					if(  EXIT_CODE == nil )
					then
						os.execute( "mkdir " .. path )
					end
					-- Get toolchain only if it doesn't exist already..
					EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.rename( path .. "/" .. self.rootpath, path .. "/" .. self.rootpath )
					if(  EXIT_CODE == nil )
					then
						EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "cd " .. path .. " && curl -L -X GET -O " .. self.url .. "/" .. self.content0 .. " -O " .. self.url .. "/" ..  self.content1 )
						if ( EXIT_CODE == true )
						then
							print( "\27[1;35m--   \27[0mDownLoad of Baremetal Cross Compiller Toolchain ended \27[1;32mSUCCESSFULLY\27[0m.. !!\n" )
							-- Finally Prepare the toolchain ( uncompress it.. )
							EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "cd " .. path .. " && tar -xJf " .. self.content0 )
							if ( EXIT_CODE == true )
							then
								print( "\27[1;35m--   \27[0mBaremetal Cross Compiller Toolchain Instalation ended \27[1;32mSUCCESSFULLY\27[0m.. !!\n" )
								
							else
								print( "\27[1;31m--   \27[0mSomething went wrong.. in Baremetal Cross Compiller Toolchain Installation, \27[1;31mFAILLED\27[0m !!\n" )
							end
						else
							print( "\27[1;31m--   \27[0mSomething went wrong.. in Baremetal Cross Compiller Toolchain DownLoad, \27[1;31mFAILLED\27[0m !!\n" )
						end
					else
						-- No need to download the toolchain has it already exists..
						print( "\27[1;35m--   \27[0mBaremetal Cross Compiller Toolchain already in place.. \27[1;32mOK\27[0m.. !!\n" )
						EXIT_SIGNAL = 0
					end

					return EXIT_SIGNAL
				end
			},
			-- ToolChain arm32 hard-float Linaro Linux Litle-Endian
			linuxgnu	= {
				----amd64
				version		= "8.3-2019.03",
				arch		= "aarch32",
				url		= "https://developer.arm.com/-/media/Files/downloads/gnu-a/8.3-2019.03/binrel",
				content0	= "gcc-arm-8.3-2019.03-x86_64-arm-linux-gnueabihf.tar.xz",
				content1	= "gcc-arm-8.3-2019.03-x86_64-arm-linux-gnueabihf.tar.xz.asc",
				rootpath	= "gcc-arm-8.3-2019.03-x86_64-arm-linux-gnueabihf",
				prefix		= "arm-linux-gnueabihf-",
				-- i686
				--version		= "7.5.0-2019.12",
				--arch		= "aarch32",
				--url		= "https://releases.linaro.org/components/toolchain/binaries/latest-7/arm-linux-gnueabi",
				--content0	= "gcc-linaro-7.5.0-2019.12-i686_arm-linux-gnueabi.tar.xz",
				--content1	= "gcc-linaro-7.5.0-2019.12-i686_arm-linux-gnueabi.tar.xz.asc",
				--rootpath	= "gcc-linaro-7.5.0-2019.12-i686_arm-linux-gnueabi",
				--prefix		= "arm-linux-gnueabi-",
				--- Linux-Gnu Functions
				--
				---- Get Kernel Cross Compiling linux-Gnu ToolChain
				getCrossToolChain		= function ( self,  base_path )
					local path = base_path .. "/ToolChains"
					print( "\27[1;35m---- DownLoad Gnu/Linux Cross Compiller Toolchain, Version< " .. self.version .. " >\27[0m!" )
					print( "\27[1;35m---  Files to DownLoad\27[0m:\n\27[1;35m--   \27[0m" .. self.content0 .. "\n\27[1;35m--   \27[0m" .. self.content1 .. "\27[0m" )
					local EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.rename( path, path )
					if(  EXIT_CODE == nil )
					then
						os.execute( "mkdir " .. path )
					end
					-- Get toolchain only if it doesn't exist already..
					EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.rename( path .. "/" .. self.rootpath, path .. "/" .. self.rootpath )
					if(  EXIT_CODE == nil )
					then
						EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "cd " .. path .. " && curl -L -X GET -O " .. self.url .. "/" .. self.content0 .. " -O " .. self.url .. "/" ..  self.content1 )
						if ( EXIT_CODE == true )
						then
							print( "\27[1;35m--   \27[0mDownLoad of Gnu/Linux Cross Compiller Toolchain ended \27[1;32mSUCCESSFULLY\27[0m.. !!\n" )
							-- Finally Prepare the toolchain ( uncompress it.. )
							EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "cd " .. path .. " && tar -xJf " .. self.content0 )
							if ( EXIT_CODE == true )
							then
								print( "\27[1;35m--   \27[0mGnu/Linux Cross Compiller Toolchain Instalation ended \27[1;32mSUCCESSFULLY\27[0m.. !!\n" )
								
							else
								print( "\27[1;31m--   \27[0mSomething went wrong.. in Gnu/Linux Cross Compiller Toolchain Installation, \27[1;31mFAILLED\27[0m !!\n" )
							end
						else
							print( "\27[1;31m--   \27[0mSomething went wrong.. in Gnu/Linux Cross Compiller Toolchain DownLoad, \27[1;31mFAILLED\27[0m !!\n" )
						end
					else
						-- No need to download the toolchain has it already exists..
						print( "\27[1;35m--   \27[0mGnu/Linux Cross Compiller Toolchain already in place.. \27[1;32mOK\27[0m.. !!\n" )
						EXIT_SIGNAL = 0
					end
		
					return EXIT_SIGNAL
				end
			}
		},
		aarch32 = {
			-- ToolChain aarch32 Linaro BareMetal Litle-Endian
			baremetal	= {
				----amd64
				version		= "8.3-2019.03",
				arch		= "aarch32",
				url		= "https://developer.arm.com/-/media/Files/downloads/gnu-a/8.3-2019.03/binrel",
				content0	= "gcc-arm-8.3-2019.03-x86_64-arm-eabi.tar.xz",
				content1	= "gcc-arm-8.3-2019.03-x86_64-arm-eabi.tar.xz.asc",
				rootpath	= "gcc-arm-8.3-2019.03-x86_64-arm-eabi",
				prefix		= "arm-eabi-",
				-- i686
				--version	= "7.5.0-2019.12",
				--arch		= "aarch32",
				--url		= "https://releases.linaro.org/components/toolchain/binaries/latest-7/arm-eabi",
				--content0	= "gcc-linaro-7.5.0-2019.12-i686_arm-eabi.tar.xz",
				--content1	= "gcc-linaro-7.5.0-2019.12-i686_arm-eabi.tar.xz.asc",
				--rootpath	= "gcc-linaro-7.5.0-2019.12-i686_arm-eabi",
				--prefix	= "arm-eabi-",
				--- Baremetal Functions
				--
				---- Get Baremetal Cross Compilling ToolChain
				getCrossToolChain		= function ( self,  base_path )
					local path = base_path .. "/ToolChains"
					print( "\27[1;35m---- DownLoad Baremetal Cross Compiller Toolchain, Version< " .. self.version .. " >\27[0m!" )
					print( "\27[1;35m---  Files to DownLoad\27[0m:\n\27[1;35m--   \27[0m" .. self.content0 .. "\n\27[1;35m--   \27[0m" .. self.content1 .. "\27[0m" )
					local EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.rename( path, path )
					if(  EXIT_CODE == nil )
					then
						os.execute( "mkdir " .. path )
					end
					-- Get toolchain only if it doesn't exist already..
					EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.rename( path .. "/" .. self.rootpath, path .. "/" .. self.rootpath )
					if(  EXIT_CODE == nil )
					then
						EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "cd " .. path .. " && curl -L -X GET -O " .. self.url .. "/" .. self.content0 .. " -O " .. self.url .. "/" ..  self.content1 )
						if ( EXIT_CODE == true )
						then
							print( "\27[1;35m--   \27[0mDownLoad of Baremetal Cross Compiller Toolchain ended \27[1;32mSUCCESSFULLY\27[0m.. !!\n" )
							-- Finally Prepare the toolchain ( uncompress it.. )
							EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "cd " .. path .. " && tar -xJf " .. self.content0 )
							if ( EXIT_CODE == true )
							then
								print( "\27[1;35m--   \27[0mBaremetal Cross Compiller Toolchain Instalation ended \27[1;32mSUCCESSFULLY\27[0m.. !!\n" )
								
							else
								print( "\27[1;31m--   \27[0mSomething went wrong.. in Baremetal Cross Compiller Toolchain Installation, \27[1;31mFAILLED\27[0m !!\n" )
							end
						else
							print( "\27[1;31m--   \27[0mSomething went wrong.. in Baremetal Cross Compiller Toolchain DownLoad, \27[1;31mFAILLED\27[0m !!\n" )
						end
					else
						-- No need to download the toolchain has it already exists..
						print( "\27[1;35m--   \27[0mBaremetal Cross Compiller Toolchain already in place.. \27[1;32mOK\27[0m.. !!\n" )
						EXIT_SIGNAL = 0
					end

					return EXIT_SIGNAL
				end
			},
			-- ToolChain aarch32 Linaro Linux Litle-Endian
			linuxgnu	= {
	 			----amd64
				version		= "8.3-2019.03",
				arch		= "aarch32",
				url		= "https://developer.arm.com/-/media/Files/downloads/gnu-a/8.3-2019.03/binrel",
				content0	= "gcc-arm-8.3-2019.03-x86_64-arm-linux-gnueabihf.tar.xz",
				content1	= "gcc-arm-8.3-2019.03-x86_64-arm-linux-gnueabihf.tar.xz.asc",
				rootpath	= "gcc-arm-8.3-2019.03-x86_64-arm-linux-gnueabihf",
				prefix		= "arm-linux-gnueabihf-",
				--i686
				--version	= "7.5.0-2019.12",
				--arch		= "aarch32",
				--url		= "https://releases.linaro.org/components/toolchain/binaries/latest-7/arm-linux-gnueabihf",
				--content0	= "gcc-linaro-7.5.0-2019.12-i686_arm-linux-gnueabihf.tar.xz",
				--content1	= "gcc-linaro-7.5.0-2019.12-i686_arm-linux-gnueabihf.tar.xz.asc",
				--rootpath	= "gcc-linaro-7.5.0-2019.12-i686_arm-linux-gnueabihf",
				--prefix	= "arm-linux-gnueabihf-",
				--- Linux-Gnu Functions
				--
				---- Get Kernel Cross Compiling linux-Gnu ToolChain
				getCrossToolChain		= function ( self,  base_path )
					local path = base_path .. "/ToolChains"
					print( "\27[1;35m---- DownLoad Gnu/Linux Cross Compiller Toolchain, Version< " .. self.version .. " >\27[0m!" )
					print( "\27[1;35m---  Files to DownLoad\27[0m:\n\27[1;35m--   \27[0m" .. self.content0 .. "\n\27[1;35m--   \27[0m" .. self.content1 .. "\27[0m" )
					local EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.rename( path, path )
					if(  EXIT_CODE == nil )
					then
						os.execute( "mkdir " .. path )
					end
					-- Get toolchain only if it doesn't exist already..
					EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.rename( path .. "/" .. self.rootpath, path .. "/" .. self.rootpath )
					if(  EXIT_CODE == nil )
					then
						EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "cd " .. path .. " && curl -L -X GET -O " .. self.url .. "/" .. self.content0 .. " -O " .. self.url .. "/" ..  self.content1 )
						if ( EXIT_CODE == true )
						then
							print( "\27[1;35m--   \27[0mDownLoad of Gnu/Linux Cross Compiller Toolchain ended \27[1;32mSUCCESSFULLY\27[0m.. !!\n" )
							-- Finally Prepare the toolchain ( uncompress it.. )
							EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "cd " .. path .. " && tar -xJf " .. self.content0 )
							if ( EXIT_CODE == true )
							then
								print( "\27[1;35m--   \27[0mGnu/Linux Cross Compiller Toolchain Instalation ended \27[1;32mSUCCESSFULLY\27[0m.. !!\n" )
								
							else
								print( "\27[1;31m--   \27[0mSomething went wrong.. in Gnu/Linux Cross Compiller Toolchain Installation, \27[1;31mFAILLED\27[0m !!\n" )
							end
						else
							print( "\27[1;31m--   \27[0mSomething went wrong.. in Gnu/Linux Cross Compiller Toolchain DownLoad, \27[1;31mFAILLED\27[0m !!\n" )
						end
					else
						-- No need to download the toolchain has it already exists..
						print( "\27[1;35m--   \27[0mGnu/Linux Cross Compiller Toolchain already in place.. \27[1;32mOK\27[0m.. !!\n" )
						EXIT_SIGNAL = 0
					end

					return EXIT_SIGNAL
				end
			}
		},
		aarch64 = {
			-- ToolChain aarch64 Linaro BareMetal Litle-Endian
			baremetal	= {
				----amd64
				version		= "8.3-2019.03",
				arch		= "aarch64",
				url		= "https://developer.arm.com/-/media/Files/downloads/gnu-a/8.3-2019.03/binrel",
				content0	= "gcc-arm-8.3-2019.03-x86_64-aarch64-elf.tar.xz",
				content1	= "gcc-arm-8.3-2019.03-x86_64-aarch64-elf.tar.xz.asc",
				rootpath	= "gcc-arm-8.3-2019.03-x86_64-aarch64-elf",
				prefix		= "aarch64-elf-",
				--i686
				--version	= "7.5.0-2019.12",
				--arch		= "aarch64",
				--url		= "https://releases.linaro.org/components/toolchain/binaries/latest-7/aarch64-elf",
				--content0	= "gcc-linaro-7.5.0-2019.12-i686_aarch64-elf.tar.xz",
				--content1	= "gcc-linaro-7.5.0-2019.12-i686_aarch64-elf.tar.xz.asc",
				--rootpath	= "gcc-linaro-7.5.0-2019.12-i686_aarch64-elf",
				--prefix	= "aarch64-elf-",
				--- Baremetal Functions
				--
				-- Get Baremetal Cross Compilling ToolChain
				getCrossToolChain		= function ( self,  base_path )
					local path = base_path .. "/ToolChains"
					print( "\27[1;35m---- DownLoad Baremetal Cross Compiller Toolchain, Version< " .. self.version .. " >\27[0m!" )
					print( "\27[1;35m---  Files to DownLoad\27[0m:\n\27[1;35m--   \27[0m" .. self.content0 .. "\n\27[1;35m--   \27[0m" .. self.content1 .. "\27[0m" )
					local EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.rename( path, path )
					if( EXIT_CODE == nil )
					then
						os.execute( "mkdir " .. path )
					end
					-- Get toolchain only if it doesn't exist already..
					EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.rename( path .. "/" .. self.rootpath, path .. "/" .. self.rootpath )
					if(  EXIT_CODE == nil )
					then
						EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "cd " .. path .. " && curl -L -X GET -O " .. self.url .. "/" .. self.content0 .. " -O " .. self.url .. "/" ..  self.content1 )
						if ( EXIT_CODE == true )
						then
							print( "\27[1;35m--   \27[0mDownLoad of Baremetal Cross Compiller Toolchain ended \27[1;32mSUCCESSFULLY\27[0m.. !!\n" )
							-- Finally Prepare the toolchain ( uncompress it.. )
							EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "cd " .. path .. " && tar -xJf " .. self.content0 )
							if ( EXIT_CODE == true )
							then
								print( "\27[1;35m--   \27[0mBaremetal Cross Compiller Toolchain Instalation ended \27[1;32mSUCCESSFULLY\27[0m.. !!\n" )
								
							else
								print( "\27[1;31m--   \27[0mSomething went wrong.. in Baremetal Cross Compiller Toolchain Installation, \27[1;31mFAILLED\27[0m !!\n" )
							end
						else
							print( "\27[1;31m--   \27[0mSomething went wrong.. in Baremetal Cross Compiller Toolchain DownLoad, \27[1;31mFAILLED\27[0m !!\n" )
						end
					else
						-- No need to download the toolchain has it already exists..
						print( "\27[1;35m--   \27[0mBaremetal Cross Compiller Toolchain already in place.. \27[1;32mOK\27[0m.. !!\n" )
						EXIT_SIGNAL = 0
					end

					return EXIT_SIGNAL
				end
			},
			-- ToolChain aarch64 Linaro Linux Litle-Endian
			linuxgnu	= {
				----amd64
				version		= "8.3-2019.03",
				arch		= "aarch64",
				url		= "https://developer.arm.com/-/media/Files/downloads/gnu-a/8.3-2019.03/binrel",
				content0	= "gcc-arm-8.3-2019.03-x86_64-aarch64-linux-gnu.tar.xz",
				content1	= "gcc-arm-8.3-2019.03-x86_64-aarch64-linux-gnu.tar.xz.asc",
				rootpath	= "gcc-arm-8.3-2019.03-x86_64-aarch64-linux-gnu",
				prefix		= "aarch64-linux-gnu-",
				----i686
				--version	= "7.5.0-2019.12",
				--arch		= "aarch64",
				--url		= "https://releases.linaro.org/components/toolchain/binaries/latest-7/aarch64-linux-gnu",
				--content0	= "gcc-linaro-7.5.0-2019.12-i686_aarch64-linux-gnu.tar.xz",
				--content1	= "gcc-linaro-7.5.0-2019.12-i686_aarch64-linux-gnu.tar.xz.asc",
				--rootpath	= "gcc-linaro-7.5.0-2019.12-i686_aarch64-linux-gnu",
				--prefix	= "aarch64-linux-gnu-",
				--- Linux-Gnu Functions
				--
				---- Get Kernel Cross Compiling linux-Gnu ToolChain
				getCrossToolChain		= function ( self,  base_path )
					local path = base_path .. "/ToolChains"
					print( "\27[1;35m---- DownLoad Gnu/Linux Cross Compiller Toolchain, Version< " .. self.version .. " >\27[0m!" )
					print( "\27[1;35m---  Files to DownLoad\27[0m:\n\27[1;35m--   \27[0m" .. self.content0 .. "\n\27[1;35m--   \27[0m" .. self.content1 .. "\27[0m" )
					local EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.rename( path, path )
					if( EXIT_CODE == nil )
					then
						os.execute( "mkdir " .. path )
					end
					-- Get toolchain only if it doesn't exist already..
					EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.rename( path .. "/" .. self.rootpath, path .. "/" .. self.rootpath )
					if(  EXIT_CODE == nil )
					then
						EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "cd " .. path .. " && curl -L -X GET -O " .. self.url .. "/" .. self.content0 .. " -O " .. self.url .. "/" ..  self.content1 )
						if ( EXIT_CODE == true )
						then
							print( "\27[1;35m--   \27[0mDownLoad of Gnu/Linux Cross Compiller Toolchain ended \27[1;32mSUCCESSFULLY\27[0m.. !!\n" )
							-- Finally Prepare the toolchain ( uncompress it.. )
							EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "cd " .. path .. " && tar -xJf " .. self.content0 )
							if ( EXIT_CODE == true )
							then
								print( "\27[1;35m--   \27[0mGnu/Linux Cross Compiller Toolchain Instalation ended \27[1;32mSUCCESSFULLY\27[0m.. !!\n" )
								
							else
								print( "\27[1;31m--   \27[0mSomething went wrong.. in Gnu/Linux Cross Compiller Toolchain Installation, \27[1;31mFAILLED\27[0m !!\n" )
							end
						else
							print( "\27[1;31m--   \27[0mSomething went wrong.. in Gnu/Linux Cross Compiller Toolchain DownLoad, \27[1;31mFAILLED\27[0m !!\n" )
						end
					else
						-- No need to download the toolchain has it already exists..
						print( "\27[1;35m--   \27[0mGnu/Linux Cross Compiller Toolchain already in place.. \27[1;32mOK\27[0m.. !!\n" )
						EXIT_SIGNAL = 0
					end

					return EXIT_SIGNAL
				end
			}
		}
	},
	x86 = {
		amd64 = {
			-- ToolChain amd64 BareMetal Litle-Endian
			baremetal	= {
				----amd64
				--version	= "8.3-2019.03",
				--arch		= "amd64",
				--url		= "https://developer.arm.com/-/media/Files/downloads/gnu-a/8.3-2019.03/binrel",
				--content0	= "gcc-arm-8.3-2019.03-x86_64-aarch64-elf.tar.xz",
				--content1	= "gcc-arm-8.3-2019.03-x86_64-aarch64-elf.tar.xz.asc",
				--rootpath	= "gcc-arm-x86_64-aarch64-elf",
				--prefix	= "amd64-elf-",
				--i686
				version		= "7.5.0-2019.12",
				arch		= "amd64",
				url		    = "https://releases.linaro.org/components/toolchain/binaries/latest-7/aarch64-elf",
				content0	= "gcc-linaro-7.5.0-2019.12-i686_aarch64-elf.tar.xz",
				content1	= "gcc-linaro-7.5.0-2019.12-i686_aarch64-elf.tar.xz.asc",
				rootpath	= "gcc-linaro-7.5.0-2019.12-i686_aarch64-elf",
				prefix		= "amd64-elf-",
				--- Baremetal Functions
				--
				-- Get Baremetal Cross Compilling ToolChain
				getCrossToolChain		= function ( self,  base_path )
					local path = base_path .. "/ToolChains"
					print( "\27[1;35m---- DownLoad Baremetal Cross Compiller Toolchain, Version< " .. self.version .. " >\27[0m!" )
					print( "\27[1;35m---  Files to DownLoad\27[0m:\n\27[1;35m--   \27[0m" .. self.content0 .. "\n\27[1;35m--   \27[0m" .. self.content1 .. "\27[0m" )
					local EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.rename( path, path )
					if( EXIT_CODE == nil )
					then
						os.execute( "mkdir " .. path )
					end
					-- Get toolchain only if it doesn't exist already..
					EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.rename( path .. "/" .. self.rootpath, path .. "/" .. self.rootpath )
					if(  EXIT_CODE == nil )
					then
						EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "cd " .. path .. " && curl -L -X GET -O " .. self.url .. "/" .. self.content0 .. " -O " .. self.url .. "/" ..  self.content1 )
						if ( EXIT_CODE == true )
						then
							print( "\27[1;35m--   \27[0mDownLoad of Baremetal Cross Compiller Toolchain ended \27[1;32mSUCCESSFULLY\27[0m.. !!\n" )
							-- Finally Prepare the toolchain ( uncompress it.. )
							EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "cd " .. path .. " && tar -xJf " .. self.content0 )
							if ( EXIT_CODE == true )
							then
								print( "\27[1;35m--   \27[0mBaremetal Cross Compiller Toolchain Instalation ended \27[1;32mSUCCESSFULLY\27[0m.. !!\n" )
								
							else
								print( "\27[1;31m--   \27[0mSomething went wrong.. in Baremetal Cross Compiller Toolchain Installation, \27[1;31mFAILLED\27[0m !!\n" )
							end
						else
							print( "\27[1;31m--   \27[0mSomething went wrong.. in Baremetal Cross Compiller Toolchain DownLoad, \27[1;31mFAILLED\27[0m !!\n" )
						end
					else
						-- No need to download the toolchain has it already exists..
						print( "\27[1;35m--   \27[0mBaremetal Cross Compiller Toolchain already in place.. \27[1;32mOK\27[0m.. !!\n" )
						EXIT_SIGNAL = 0
					end

					return EXIT_SIGNAL
				end
			},
			-- ToolChain aarch64 Linaro Linux Litle-Endian
			linuxgnu	= {
				----amd64
				--version	= "8.3-2019.03",
				--arch		= "amd64",
				--url		= "https://developer.arm.com/-/media/Files/downloads/gnu-a/8.3-2019.03/binrel",
				--content0	= "gcc-arm-8.3-2019.03-x86_64-aarch64-linux-gnu.tar.xz",
				--content1	= "gcc-arm-8.3-2019.03-x86_64-aarch64-linux-gnu.tar.xz.asc",
				--rootpath	= "gcc-arm-x86_64-aarch64-linux-gnu",
				--prefix	= "amd64-linux-gnu-",
				----i686
				version		= "7.5.0-2019.12",
				arch		= "amd64",
				url		= "https://releases.linaro.org/components/toolchain/binaries/latest-7/aarch64-linux-gnu",
				content0	= "gcc-linaro-7.5.0-2019.12-i686_aarch64-linux-gnu.tar.xz",
				content1	= "gcc-linaro-7.5.0-2019.12-i686_aarch64-linux-gnu.tar.xz.asc",
				rootpath	= "gcc-linaro-7.5.0-2019.12-i686_aarch64-linux-gnu",
				prefix		= "amd64-linux-gnu-",
				--- Linux-Gnu Functions
				--
				---- Get Kernel Cross Compiling linux-Gnu ToolChain
				getCrossToolChain		= function ( self,  base_path )
					local path = base_path .. "/ToolChains"
					print( "\27[1;35m---- DownLoad Gnu/Linux Cross Compiller Toolchain, Version< " .. self.version .. " >\27[0m!" )
					print( "\27[1;35m---  Files to DownLoad\27[0m:\n\27[1;35m--   \27[0m" .. self.content0 .. "\n\27[1;35m--   \27[0m" .. self.content1 .. "\27[0m" )
					local EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.rename( path, path )
					if( EXIT_CODE == nil )
					then
						os.execute( "mkdir " .. path )
					end
					-- Get toolchain only if it doesn't exist already..
					EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.rename( path .. "/" .. self.rootpath, path .. "/" .. self.rootpath )
					if(  EXIT_CODE == nil )
					then
						EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "cd " .. path .. " && curl -L -X GET -O " .. self.url .. "/" .. self.content0 .. " -O " .. self.url .. "/" ..  self.content1 )
						if ( EXIT_CODE == true )
						then
							print( "\27[1;35m--   \27[0mDownLoad of Gnu/Linux Cross Compiller Toolchain ended \27[1;32mSUCCESSFULLY\27[0m.. !!\n" )
							-- Finally Prepare the toolchain ( uncompress it.. )
							EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "cd " .. path .. " && tar -xJf " .. self.content0 )
							if ( EXIT_CODE == true )
							then
								print( "\27[1;35m--   \27[0mGnu/Linux Cross Compiller Toolchain Instalation ended \27[1;32mSUCCESSFULLY\27[0m.. !!\n" )
								
							else
								print( "\27[1;31m--   \27[0mSomething went wrong.. in Gnu/Linux Cross Compiller Toolchain Installation, \27[1;31mFAILLED\27[0m !!\n" )
							end
						else
							print( "\27[1;31m--   \27[0mSomething went wrong.. in Gnu/Linux Cross Compiller Toolchain DownLoad, \27[1;31mFAILLED\27[0m !!\n" )
						end
					else
						-- No need to download the toolchain has it already exists..
						print( "\27[1;35m--   \27[0mGnu/Linux Cross Compiller Toolchain already in place.. \27[1;32mOK\27[0m.. !!\n" )
						EXIT_SIGNAL = 0
					end

					return EXIT_SIGNAL
				end
			}
		}
	}
	-- TODO: Implement ppc64 tooclaihn..
	-- https://github.com/advancetoolchain/advance-toolchain
	-- TODO: GET Mentor toolchain for mips32r5 or even for mips64( at least mips64r2 )..
	--toolchains.mips = {}
}
return toolchains
