
local bootloaders = {
	rootpath	= "BootLoaders",
	getRootPath	= function ( self )
		print( self.rootpath )
	end,
	arm = {
		--- ARM Trusted Firmware
		--
		--  maybe only BL31 will be needed in Uboot Compilation fase..
		armtrustedfirmware	= {
			url			= "https://github.com/ARM-software/arm-trusted-firmware.git",
			branch			= "v2.4",--v2.3
			rootpath		= "arm-trusted-firmware",
			binary			= "bl31.bin",

			--- Auxiliary ARM Trusted Firmware Functions
			--
			-- Get ARM Trusted Firmware
			getFirmware		= function ( self, base_path )
				local path = base_path .. self.rootpath .. "_" .. self.branch

				-- Check if path Already exists
				local EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.rename( path, path )
				if ( EXIT_CODE ~= true )
				then
					-- Arm Trusted Firmware path for current board, doesn't exist
					print( "\27[1;35m--- ARM-Trusted-FirmWare: Downloading ARM-Trusted-FirmWare Version < " .. self.branch .. " > ..\27[0m!!" )
					EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "git clone --branch " .. self.branch .. " " .. self.url )
					if ( EXIT_CODE == true )
					then
						print( "\27[1;35m--  ARM-Trusted-FirmWare: download Complete \27[1;32mSUCCESSFULLY\27[0m...!!" )
						-- Create Destination path
						print( path )
						EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "mkdir -pv " .. path )
						if ( EXIT_CODE == true )
						then
							-- Move DownLoaded Code to Destination path
							EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "mv -Tv " .. self.rootpath .. " " .. path )
						end
					else
						-- Problems encountered during download fase, ..trying to delete parcial code downloaded ..
						EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "rm -rf ./" .. self.rootpath )
					end
					if ( EXIT_CODE ~= true )
					then
						-- Some problem apears during Arm Trusted Firmware DownLoad/Instalation fase, report error..
						print( "Exit Code : " .. EXIT_CODE .. "\nExit Status : " .. EXIT_STATUS .. "\nExit Signal :" .. EXIT_SIGNAL .. "\n" )
					end
				else
					-- Path exists, so no Download/Install process..
					EXIT_SIGNAL = 0
				end
				return EXIT_SIGNAL
			end,
			-- Clear ARM Trusted Platform Environment
			cleanTrustedFW = function ( self, path, platform, prefix )
				-- Check if path Exists
				local EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.rename( path, path )
				if ( EXIT_CODE == true )
				then
					EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "make -C " .. path .. " CROSS_COMPILE=" .. prefix .. " PLAT=" .. platform .. " DEBUG=0 distclean" )
				else
					EXIT_SIGNAL = 0
				end
				return EXIT_SIGNAL
			end,
			buildTrustedFW = function ( self, base_path, board_path, tee, prefix, patchs, do_patchs, ncores )
				local EXIT_SIGNAL
				-- this is redundant..
				if ( type( tee ) == "table" )
				then
					EXIT_SIGNAL = self:getFirmware( base_path )
					if ( EXIT_SIGNAL == 0 )
					then
						local  EXIT_CODE, EXIT_STATUS
						local path = base_path .. self.rootpath .. "_" .. self.branch
	
						-- If there are ATF patchs to apply, then apply them..
						if ( type( patchs.atf ) == "table" and do_patchs == true )
						then
							print( "\27[1;35m---  Applying Patchs.. !!\27[0m" )
							for k,patch in ipairs( patchs.atf )
							do
								print( "\27[1;35m--   \27[0mApply patch \'" .. patch .. "\' !" )
								EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "cd " .. path .. " && patch -p1 < " .. "../../reference/patchs/atf/" .. patch )
								if ( EXIT_CODE == true )
								then
									print( "     patch \'" .. patch .. "\', applied with \27[1;32mSUCCESS\27[0m.. !!" )
								else
									print( "     patch \'" .. patch .. "\', was \27[1;31m**not** Applied\27[0m.. !!" )
									print( "Exit Code : " .. EXIT_CODE .. "\nExit Status : " .. EXIT_STATUS .. "\nExit Signal :" .. EXIT_SIGNAL .. "\n" )
									return EXIT_SIGNAL
								end
							end
						else
							print( "\27[1;35m--   \27[1;33mNo Patchs found for " .. board_path .. " or patchs not requested..\27[1;35m!!\27[0m" )
						end
					
						print( "\27[1;35m--- ARM-Trusted-FirmWare: Prepare to build \'" .. self.rootpath .. "_" .. self.branch .. " - ".. tee.stage .. "\' stage ...!!\27[0m" )
						--local path = base_path .. self.rootpath .. "_" .. self.branch
						--local EXIT_CODE, EXIT_STATUS

						EXIT_SIGNAL = self:cleanTrustedFW( path, tee.platform, prefix )
						if ( EXIT_SIGNAL == 0 )
						then
							EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "make -C " .. path .. " CROSS_COMPILE=" .. prefix .. " PLAT=" .. tee.platform .. " DEBUG=0 " .. tee.stage .. " -j" .. ncores )
							if ( EXIT_CODE == true )
							then
								print( "\27[1;35m--  ARM-Trusted-FirmWare: Compilled \27[1;32mSUCCESSFULLY\27[0m!!" )
							end
						else
							EXIT_CODE = true
						end
						if ( EXIT_CODE ~= true )
						then
							print( "Exit Code : " .. EXIT_CODE .. "\nExit Status : " .. EXIT_STATUS .. "\nExit Signal :" .. EXIT_SIGNAL .. "\n" )
						end
					end
				else
					EXIT_SIGNAL = 0
				end
				return EXIT_SIGNAL
			end
		},
		--- Open Portable Trusted Execution Environment Firmware
		--
		--  OPTEE
		optee	= {
			url			= "https://github.com/OP-TEE/build.git",
			branch			= "3.7.0",
			rootpath		= "optee-firmware",
			binary			= "tee.elf",

			--- Auxiliary Open Portable Trusted Execution Environment Firmware Functions
			--
			-- Get ARM Trusted Firmware
			getFirmware		= function ( self, base_path )
				local path = base_path .. self.rootpath

				-- Check if path Already exists
				local EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.rename( path, path )
				if ( EXIT_CODE ~= true )
				then
					-- Arm Trusted Firmware path for current board, doesn't exist
					print( "\27[1;35m--- OPTEE-FirmWare: Downloading OPTEE-FirmWare < " .. self.branch .. " > ..\27[0m!!" )
					EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "git clone --branch " .. self.branch .. " " .. self.url )
					if ( EXIT_CODE == true )
					then
						print( "\27[1;35m--  OPTEE-FirmWare: download Complete \27[1;32mSUCCESSFULLY\27[0m...!!" )
						-- Create Destination path
						print( path )
						EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "mkdir -pv " .. path )
						if ( EXIT_CODE == true )
						then
							-- Move DownLoaded Code to Destination path
							EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "mv -Tv " .. self.rootpath .. " " .. path )
						end
					else
						-- Problems encountered during download fase, ..trying to delete parcial code downloaded ..
						EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "rm -rf ./" .. self.rootpath )
					end
					if ( EXIT_CODE ~= true )
					then
						-- Some problem apears during Arm Trusted Firmware DownLoad/Instalation fase, report error..
						print( "Exit Code : " .. EXIT_CODE .. "\nExit Status : " .. EXIT_STATUS .. "\nExit Signal :" .. EXIT_SIGNAL .. "\n" )
					end
				else
					-- Path exists, so no Download/Install process..
					EXIT_SIGNAL = 0
				end
				return EXIT_SIGNAL
			end,
			-- Clear Open Portable Trusted Execution Environment
			cleanTrustedFW = function ( self, path, platform, prefix )
				-- Check if path Exists
				local EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.rename( path, path )
				if ( EXIT_CODE == true )
				then
					EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "make -C " .. path .. " CROSS_COMPILE=" .. prefix .. " PLAT=" .. platform .. " DEBUG=0 clean" )
				else
					EXIT_SIGNAL = 0
				end
				return EXIT_SIGNAL
			end,
			buildTrustedFW = function ( self, base_path, board_path, tee, prefix, patchs, do_patchs, ncores )
				local EXIT_SIGNAL
				if ( platform ~= nil and stage ~= nil )
				then
					EXIT_SIGNAL = self:getFirmware( base_path )
					if ( EXIT_SIGNAL == 0 )
					then
						print( "\27[1;35m--- OPTEE-FirmWare: Prepare to build \'" .. stage .. "\' stage ...!!\27[0m" )
						local path = base_path .. self.rootpath
						local EXIT_CODE, EXIT_STATUS

						EXIT_SIGNAL = self:cleanOPTEEFW( path, platform, prefix )
						if ( EXIT_SIGNAL == 0 )
						then
							EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "make -C " .. path .. " CROSS_COMPILE=" .. prefix .. " PLAT=" .. platform .. " DEBUG=0 " .. stage .. " -j" .. ncores )
							if ( EXIT_CODE == true )
							then
								print( "\27[1;35m--  OPTEE-FirmWare: Compilled \27[1;32mSUCCESSFULLY\27[0m!!" )
							end
						else
							EXIT_CODE = true
						end
						if ( EXIT_CODE ~= true )
						then
							print( "Exit Code : " .. EXIT_CODE .. "\nExit Status : " .. EXIT_STATUS .. "\nExit Signal :" .. EXIT_SIGNAL .. "\n" )
						end
					end
				else
					EXIT_SIGNAL = 0
				end
				return EXIT_SIGNAL
			end
		},
		--- UBoot "Mainline" BootLoader
		--
		-- UBoot
		uboot	= {
			url		= "git://git.denx.de/u-boot.git",
			branch		= "v2020.10",
			rootpath	= "u-boot",

			--- Auxiliary UBoot "Mainline" BootLoader Functions
			--
			-- Get UBoot "Mainline" BootLoader
			getUboot	= function ( self, base_path )
				local path = base_path .. self.rootpath .. "_" .. self.branch

				-- Check if path Already exists
				local EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.rename( path, path )
				if ( EXIT_CODE ~= true )
				then
					-- UBoot path for current board, doesn't exist
					print( "\27[1;35m--- Uboot: Download Uboot Version < " .. self.branch .. " >  ...!!\27[0m" )
					EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "git clone --branch " .. self.branch .. " --single-branch --depth 1 " .. self.url )
					if ( EXIT_CODE == true )
					then
						print( "\27[1;35m--  Uboot: download Completed \27[1;32mSUCCESSFULLY\27[0m" )
						-- Create Destination Uboot path
						EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "mkdir -pv " .. path )
						if ( EXIT_CODE == true )
						then
							-- Move DownLoaded Code to Destination path
							EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "mv -Tv " .. self.rootpath .. " " .. path )
						end
					else
						-- Problems encountered during download fase, ..trying to delete parcial code downloaded ..
						EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "rm -rf ./" .. self.rootpath )
					end
					if ( EXIT_CODE ~= true )
					then
						-- Some problem apears during UBoot DownLoad/Instalation fase, report error..
						print( "Exit Code : " .. EXIT_CODE .. "\nExit Status : " .. EXIT_STATUS .. "\nExit Signal :" .. EXIT_SIGNAL .. "\n" )
					end
				else
					-- Path exists, so no Download/Install process..
					EXIT_SIGNAL = 0
				end
				return EXIT_SIGNAL
			end,
			-- Dist Clear Uboot Environment
			distcleanUboot = function ( self, path, prefix )
				local EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "make -C " .. path .. " ARCH=arm CROSS_COMPILE=" .. prefix .. " distclean" )
				return EXIT_SIGNAL
			end,
			buildUboot = function ( self, base_path, board_path, trustedfw_path, trustedfw_stage, trustedfw_bin, defconfig, prefix, Kconfig, patchs, do_patchs, ncores )
				local EXIT_SIGNAL = self:getUboot( base_path )
				if ( EXIT_SIGNAL == 0 )
				then
	
					local  EXIT_CODE, EXIT_STATUS
					local path = base_path .. self.rootpath .. "_" .. self.branch
					-- If there are Kernel patchs to apply, then apply them..
					if ( type( patchs.uboot ) == "table" and do_patchs == true )
					then
						print( "\27[1;35m---  Applying Patchs.. !!\27[0m" )
						for k,patch in ipairs( patchs.uboot )
						do
							print( "\27[1;35m--   \27[0mApply patch \'" .. patch .. "\' !" )
							EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "cd " .. path .. " && patch -p1 < " .. "../../reference/patchs/u-boot/" .. patch )
							if ( EXIT_CODE == true )
							then
								print( "     patch \'" .. patch .. "\', applied with \27[1;32mSUCCESS\27[0m.. !!" )
							else
								print( "     patch \'" .. patch .. "\', was \27[1;31m**not** Applied\27[0m.. !!" )
								print( "Exit Code : " .. EXIT_CODE .. "\nExit Status : " .. EXIT_STATUS .. "\nExit Signal :" .. EXIT_SIGNAL .. "\n" )
								return EXIT_SIGNAL
							end
						end
					else
						print( "\27[1;35m--  \27[1;33mNo Patchs found for " .. board_path .. " or patchs not requested..\27[1;35m!!\27[0m" )
					end
	
					print( "\27[1;35m--- Uboot: Prepare to build \'" .. self.rootpath .. "_" .. self.branch .. "\' ...!!\27[0m" )
					--local path = base_path .. self.rootpath .. "_" .. self.branch
					-- Uboot distclean first Environment
					local EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL
					EXIT_SIGNAL = self:distcleanUboot( path, prefix )
					if ( EXIT_SIGNAL == 0 )
					then
						if ( trustedfw_stage ~= nil and trustedfw_bin ~= nil )
						then
							-- Add ARM TrustedFirmware Stage to Uboot for arm64..
							if ( trustedfw_path ~=nil and trustedfw_stage ~= nil  )
							then
								EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "mkdir -pv " .. path .. ";find " .. trustedfw_path .. "/build -name " .. trustedfw_bin .. " -exec cp -v {} " .. path .. " \\;" )
								if ( EXIT_CODE == true )
								then
									-- for ARM aarch64
									if( Kconfig == "defconfig" )
									then
										EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "export BL31=\"" .. path .. "/" .. trustedfw_bin .. "\";make -C " .. path .. " ARCH=arm CROSS_COMPILE=" .. prefix .. " " .. defconfig )
									elseif( Kconfig == "oldconfig" or Kconfig == "menuconfig" )
									then
										-- Copy Reference .config file to path
										EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "cp " .. string.gsub( base_path, "/BootLoaders/", "" ) .. "/reference/.config/uboots/" .. defconfig .. " " .. path .. "/.config" )
										EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "export BL31=\"" .. path .. "/" .. trustedfw_bin .. "\";make -C " .. path .. " ARCH=arm CROSS_COMPILE=" .. prefix .. " " .. Kconfig )
									end
									if ( EXIT_CODE == true )
									then
										EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "make -C " .. path .. " ARCH=arm CROSS_COMPILE=" .. prefix .. " -j" .. ncores )
										if ( EXIT_CODE == true )
										then
											print( "\27[1;35m--  Uboot: Compilled \27[1;32mSUCCESSFULLY\27[0m!!" )
										end
									end
								end
							end
						else
							-- for ARM aarch32
							if( Kconfig == "defconfig" )
							then
								EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "make -C " .. path .. " ARCH=arm CROSS_COMPILE=" .. prefix .. " " .. defconfig )
							elseif( Kconfig == "oldconfig" or Kconfig == "menuconfig" )
							then
								-- Copy Reference .config file to path
								EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "cp " .. string.gsub( base_path, "/BootLoaders/", "" ) .. "/reference/.config/uboots/" .. defconfig .. " " .. path .. "/.config" )
								EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "make -C " .. path .. " ARCH=arm CROSS_COMPILE=" .. prefix .. " " .. Kconfig )
							end
							if ( EXIT_CODE == true )
							then
								EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "make -C " .. path .. " ARCH=arm CROSS_COMPILE=" .. prefix .. " -j" .. ncores )
								if ( EXIT_CODE == true )
								then
									print( "\27[1;35m--  Uboot: Compilled \27[1;32mSUCCESSFULLY\27[0m!!" )
								end
							end
						end
					else
						EXIT_CODE = false
					end
					if ( EXIT_CODE ~= true )
					then
						print( "Exit Code : " .. EXIT_CODE .. "\nExit Status : " .. EXIT_STATUS .. "\nExit Signal :" .. EXIT_SIGNAL .. "\n" )
					end
				end
				return EXIT_SIGNAL
			end
		},
		--- Public Build Functions
		--
		buildBootLoader	= function ( self, base_path, board_path, tee, defconfig, prefix, Kconfig, bootImage, patchs, do_patchs, ncores )
			local path		= base_path .. "/BootLoaders/"
			local trustedfw_path	= nil

			print( "\27[1;35m---- BootLoader: Prepare to build BootLoader ...!!\27[0m" )
			local EXIT_SIGNAL
			-- Check if for the current board a Trusted Execution Environment is needed..
			if( type( tee ) == "table" )
			then
				-- 2 options supported..
				if( tee.name == "armtrustedfirmware" )
				then
					trustedfw_path = path .. self.armtrustedfirmware.rootpath .. "_" .. self.armtrustedfirmware.branch
				elseif( tee.name == "optee" )
				then
					trustedfw_path = path .. self.optee.rootpath .. "_" .. self.optee.branch
				end
				-- Build First the Trusted Execution Environment
				EXIT_SIGNAL	= self[ tee.name ]:buildTrustedFW( path, board_path, tee, prefix, patchs, do_patchs, ncores )
			else
				-- In this case we don't need a Trusted Execution Environment..armv{ 6 | 7 } ??
				EXIT_SIGNAL = 0
			end
			if ( EXIT_SIGNAL == 0 )
			then
				local EXIT_CODE, EXIT_STATUS
				if( type( tee ) == "table" )
				then
					EXIT_SIGNAL = self.uboot:buildUboot( path, board_path, trustedfw_path, tee.stage, tee.image, defconfig, prefix, Kconfig, patchs, do_patchs, ncores )
				else
					EXIT_SIGNAL = self.uboot:buildUboot( path, board_path, nil, nil, nil, defconfig, prefix, Kconfig, patchs, do_patchs, ncores )
				end
				if ( EXIT_SIGNAL == 0 )
				then
					-- aarch32 doesn't use ATF.. so nothing to clean..
					if( type( tee ) == "table" )
					then
						-- Clean Trusted Execution Environment
						self[ tee.name ]:cleanTrustedFW( trustedfw_path , tee.platform, prefix )
					end

					-- Copy Already Compiled bootloader to Builds path
					for key,file in ipairs( bootImage )
					do
						EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "mkdir -pv " .. base_path .. "/Builds;find " .. path .. self.uboot.rootpath .. "_" .. self.uboot.branch .. " -name " .. file .. " -type f -exec mv -v {} " .. base_path .. "/Builds/" .. board_path .. "_" .. file .. " \\;" )
						if ( EXIT_CODE ~= true )
						then
							-- distclean Uboot environment
							print( "\27[1;35m--- BootLoader: Ended with \27[1;31mERRORS\27[0m!!" )
							return EXIT_SIGNAL
						end
					end
					-- distclean Uboot environment
					self.uboot:distcleanUboot( path .. self.uboot.rootpath .. "_" .. self.uboot.branch , prefix )
					print( "\27[1;35m--- BootLoader: Finished \27[1;32mSUCCESSFULLY\27[0m!!" )
				end
			end
			return EXIT_SIGNAL
		end
	}
	-- TODO: GET mips bootloaders for mips32r5 or even for mips64, loongson?..
	--bootloaders.mips = {}
}
return bootloaders
