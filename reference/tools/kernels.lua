
local kernels = {
	rootpath	= "Kernels",
	getRootPath	= function( self )
		return self.rootpath
	end,
	mainline	= {
		--- Signatures
		--
		-- Developers
		devkeys		= "torvalds@kernel.org gregkh@kernel.org",
		-- Autosigner
		shakeys		= "autosigner@kernel.org",

		--- Kernel Versions
		--
		-- 5.10.43 Stable release
		lts 		= {
			version		= "5.10.43",
			url		= "https://cdn.kernel.org/pub/linux/kernel/v5.x",
			kernfile	= "linux-5.10.43.tar.xz",
			signkernfile	= "linux-5.10.43.tar.sign",
			shafile		= "sha256sums.asc",
			rootpath	= "linux-5.10.43"
		},
		--  5.12.10 Stable release
		current 		= {
			version		= "5.12.10",
			url		= "https://cdn.kernel.org/pub/linux/kernel/v5.x",
			kernfile	= "linux-5.12.10.tar.xz",
			signkernfile	= "linux-5.12.10.tar.sign",
			shafile		= "sha256sums.asc",
			rootpath	= "linux-5.12.10"
		},
		-- Mainline Branch
		mainline 		= {
			version		= "mainline",
			url		= nil,
			kernfile	= nil,
			signkernfile	= nil,
			shafile		= nil,
			rootpath	= "linux-mainline"
		},
		--- Auxiliary Kernel "Mainline" BootLoader Functions
		--
		--- This Function supports mainline Stable kernels v3.x.x and above
		getVerifyStableKernel		= function ( self, base_path, ver )
			local path = base_path

			-- Check if path Already exists
			local EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.rename( path, path )
			if ( EXIT_CODE ~= true )
			then
				-- Attemp to get and verify Mainline Kernel Version X
				print( "\27[1;35m---- DownLoad and Verify Kernel Signatures for Kernel Version< " .. self[ ver ].version .. " >\27[0m!" )
				print( "\27[1;35m---  Files to DownLoad\27[0m:\n\27[1;35m--   \27[0m" .. self[ ver ].kernfile .. "\n\27[1;35m--   \27[0m" .. self[ ver ].signkernfile .. "\n\27[1;35m--   \27[0m" .. self[ ver ].shafile .. "\n" )

				--- Kill Anny Previous PGP Agents Running..
				print( "\27[1;35m--- Kill Previous 'gpg-agents', if anny..\27[0m" )
				local EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "pkill -9 gpg-agent && sleep 2;" )
				if ( EXIT_CODE == true )
				then
					print( "\27[1;35m--  \27[0mKill Previous 'gpg-agents' ended \27[1;32mSUCCESSFULLY\27[0m.. !!\n" )
				else
					print( "\27[1;35m--  \27[0mNo 'gpg-agents' were running, \27[1;32mOK\27[0m.. !!\n" )
				end

				-- Create safe temporary directory for intermediates
				print( "\27[1;35m--- Create safe **Untrusted** temporary directory for intermediates..\27[0m" )
				local handle = io.popen( "mktemp -d $PWD/linux-tarball-verify.XXXXXXXXX.untrusted | tr -d '\\n'" )
				if ( handle ~= nil )
				then
					local tmpdir = handle:read( "*a" )
					handle:close()
					print( "\27[1;35m--  \27[0mTemporary Untrusted Directory for Intermediate Keys created \27[1;32mSUCCESSFULLY\27[0m.. :\n\27[1;35m--  \27[0m" .. tmpdir .. "\n" )

					-- Create GNUPGHOME
					local gnupghome = tmpdir .. "/gnupg"
					print( "\27[1;35m--- Create GNUPGHOME Verification keys Directory.. :\27[0m\n\27[1;35m--  \27[0m" .. gnupghome )
					EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "mkdir -p -m 0700 " .. gnupghome .. ";" )
					if ( EXIT_CODE == true )
					then
						print( "\27[1;35m--  \27[0mGNUPGHOME Verification keys Directory created \27[1;32mSUCCESSFULLY\27[0m.. !!\n" )

						print( "\27[1;35m--- Fetching Public Keys to 'GLOBAL' GNUPGHOME\27[0m:\n\27[1;35m--  \27[0m" .. gnupghome )
						EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "gpg --batch --quiet --homedir " .. gnupghome .. " --auto-key-locate wkd --locate-keys " .. self.devkeys .. " " .. self.shakeys .. ";" )
						if ( EXIT_CODE == true )
						then
							print( "\27[1;35m--  \27[0mFetching Public Keys ended \27[1;32mSUCCESSFULLY\27[0m.. !!" )

							-- Make a keyring and set USEKEYRING to it
							local usekeyring = tmpdir .. "/usekeyring.gpg"
							print( "\27[1;35m--- Create USEKEYRING 'GLOBAL' keyring ( AutoSigner + Kernel Developers ) created\27[0m:\n\27[1;35m--  \27[0m" .. usekeyring )
							EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "gpg --batch --homedir " .. gnupghome .. " --export " .. self.devkeys .. " " .. self.shakeys .. " > " .. usekeyring .. ";" )
							if ( EXIT_CODE == true )
							then
								print( "\27[1;35m--  \27[0mUSEKEYRING 'GLOBAL' keyring ( AutoSigner + Kernel Developers ) created \27[1;32mSUCCESSFULLY\27[0m.. !!" )

								--- Make 2 keyrings
								-- One for the autosigner, and the other for kernel developers...

								-- Make a temporary keyring for autosigner
								local shakeyring = tmpdir .. "/shakeyring.gpg"
								EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "gpg --batch --no-default-keyring --keyring " .. usekeyring .. " --export " .. " " .. self.shakeys .. " > " .. shakeyring )
								if ( EXIT_CODE == true )
								then
									print( "\27[1;35m--- Keyring for AutoSigner only, created\27[0m:\n\27[1;35m--  \27[0m" .. shakeyring )

									-- Make a temporary keyring for kernel Developers..
									local devkeyring = tmpdir .. "/devkeyring.gpg"
									EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "gpg --batch --no-default-keyring --keyring " .. usekeyring .. " --export " .. " " .. self.devkeys .. " > " .. devkeyring )
									if ( EXIT_CODE == true )
									then
										print( "\27[1;35m--- Keyring for Kernel Developers only, created\27[0m:\n\27[1;35m--  \27[0m" .. devkeyring .. "\n")

										-- Get Kernel CheckSum File
										EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "curl --silent --location --output " .. tmpdir .. "/" .. self[ ver ].shafile .. " " .. self[ ver ].url .. "/" .. self[ ver ].shafile .. ";" )
										if ( EXIT_CODE == true )
										then
											print( "\27[1;35m--- Got CheckSums signed by autosigner '" .. self.shakeys .. "'\27[0m:\n\27[1;35m--  \27[0m" .. tmpdir .. "/" .. self[ ver ].shafile .. "\n\27[1;35m--  From\27[0m: " .. self[ ver ].url .. "/" .. self[ ver ].shafile )

											-- Verify the Authenticity of Checksum file against autosigner key
											handle = io.popen( "gpg --verify --keyring " .. shakeyring .. " --status-fd=1 " .. tmpdir .. "/" .. self[ ver ].shafile .. " 2>/dev/null 3>&2 | grep -cE '^\\[GNUPG:\\] (GOODSIG|VALIDSIG)' | tr -d '\\n';" )
											result = handle:read("*a")
											if ( tonumber( result ) > 1 )
											then
												print("\27[1;35m--  \27[0mSignature of '".. self[ ver ].shafile .. "'\27[1;32m **Is GOOD**\27[0m( Against the AutoSigner '" .. self.shakeys .. "' )")

												-- Get the Line with hash of Kernel we will download,  to other file, separated from 'sha256sums.asc'
												local shakernfile = "sha256sum" .. self[ ver ].kernfile
												os.execute( "grep \"" .. self[ ver ].kernfile .. "\" " .. tmpdir .. "/" .. self[ ver ].shafile .. " > " .. tmpdir .. "/" .. shakernfile )

												-- Get Signature file for Kernel
												EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "curl --silent --location --output " .. tmpdir .. "/" .. self[ ver ].signkernfile .. " " .. self[ ver ].url .. "/" .. self[ ver ].signkernfile .. ";" )
												if ( EXIT_CODE == true )
												then
													print( "\27[1;35m--- Got Signature for file '" .. self[ ver ].kernfile .. "'\27[0m:\n\27[1;35m--  \27[0m" .. tmpdir .. "/" .. self[ ver ].signkernfile .. "\n\27[1;35m--  From\27[0m: " .. self[ ver ].url .. "/" .. self[ ver ].signkernfile )

													print( "\27[1;35m--- Downloading Kernel file '" .. self[ ver ].kernfile .. "'\27[0m:\n\27[1;35m--  From\27[0m: " .. self[ ver ].url )
													EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "curl -X GET --output " .. self[ ver ].kernfile .. " --location " .. self[ ver ].url .. "/" .. self[ ver ].kernfile )
													if ( EXIT_CODE == true )
													then
														print( "\27[1;35m--- Calculate Checksum of '" .. self[ ver ].kernfile .. "', and verify against previously AutoSigner Signed CheckSum file '" .. tmpdir .. "/" .. self[ ver ].shafile .. "'\27[0m" )
														handle = io.popen( "sha256sum --check " .. tmpdir .. "/" .. shakernfile .. " " .. self[ ver ].kernfile .. " | grep -cE \"(" .. self[ ver ].kernfile .. ".*OK)\" | tr -d '\\n';" )
														if ( handle ~= nil )
														then
															result = handle:read("*a")
															handle:close()
															if ( tonumber( result ) == 1 )
															then
																print( "\27[1;35m--  \27[0mCheckSum \27[1;32m**MATCH**\27[0m!\n" )

																print( "\27[1;35m--- Verify Kernel Tarball Against Kernel Developers keys '" .. self.devkeys .. "'\27[0m:" )
																handle = io.popen( "xz -cd " .. self[ ver ].kernfile .. " | gpg --verify --keyring " .. devkeyring .. " --status-fd=1 " .. tmpdir .. "/" .. self[ ver ].signkernfile .. " - | grep -c -E '^\\[GNUPG:\\] (GOODSIG|VALIDSIG)' | tr -d '\\n';" )
																if ( handle ~= nil )
																then
																	result = handle:read("*a")
																	handle:close()
																	if ( tonumber( result ) > 1 )
																	then
																		print("\27[1;35m--  \27[0mSignature of '".. self[ ver ].kernfile .. "'\27[1;32m **IS GOOD**\27[0m( Against the Kernel Developers '" .. devkeyring .. "' )\n")
																		EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "mkdir -pv " .. path )
																		if ( EXIT_CODE == true )
																		then
																			EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "tar -xJf " .. self[ ver ].kernfile .. " --strip-components=1 -C" .. path )																				EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "mkdir -pv " .. path )
																			if ( EXIT_CODE == true )
																			then
																				--  remove tempdir with keyrings and such..
																				EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "rm -rf " .. tmpdir )
																				if ( EXIT_CODE == true )
																				then
																					-- Remove doesnloaded kernel file
																					EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "rm " .. self[ ver ].kernfile )
																					if ( EXIT_CODE == true )
																					then
																						-- Prepare Environment to Build deb-Packages..
																						self:setKernelPkgEnvironment( path )

																						return EXIT_SIGNAL
																					end
																				end
																			end
																		end
																		-- Remove kernel directory
																		os.execute( "rm -i -rf " .. path ) 
																	end
																	print("\27[1;31m--  \27[0mSignature of ".. self[ ver ].kernfile .. " is \27[1;31m**NOT** good\27[0m( Against the Kernel Developers '" .. devkeyring .. "' ).\n\27[1;31m--  \27[0mRemoving " .. tmpdir .. " and " .. self[ ver ].kernfile .. "\n" )
																end
															else
																print( "\27[1;31m--  \27[0mCheckSum Doesn't Match( Exit Code " .. result .. " )..\n\27[1;31m--  \27[0mRemoving " .. tmpdir .. " and " .. self[ ver ].kernfile .. "\n" )
															end
														end
														EXIT_SIGNAL = 1
													end
													-- Remove kernel and Kernel Signature file that is in tmpdir
													os.execute( "rm " .. self[ ver ].kernfile ) 
													os.execute( "rm " .. tmpdir .. "/" .. self[ ver ].signkernfile )
												else
													-- Remove signkernfile   
													os.execute( "rm " .. tmpdir .. "/" .. self[ ver ].signkernfile )
													-- Remove shakernfile 
													os.execute( "rm " .. tmpdir .. "/" .. shakernfile )
												end
											else
												print("\27[1;31m--  \27[0mSignature of ".. self[ ver ].shafile .. " is \27[1;31m**NOT** good\27[0m( Against the AutoSigner '" .. self.shakeys .. "' ).\n\27[1;31m--  \27[0mRemoving " .. tmpdir .. "/" .. self[ ver ].shafile .. "\n")
												-- Remove shafile
												os.execute( "rm -rf " .. tmpdir .. "/" .. self[ ver ].shafile )
												EXIT_SIGNAL = 1
											end
										end
										-- Remove shafile
										os.execute( "rm -rf " .. tmpdir .. "/" .. self[ ver ].shafile )
									end
									-- Remove tmpdir/devkeyring.gpg
									os.execute( "rm -rf " .. devkeyring )
								end
								-- Remove shakeyring
								os.execute( "rm -rf " .. shakeyring )
							else
								print( "\27[1;31m--  \27[0mSomething Went Wrong( Exit Code " .. EXIT_SIGNAL .. " )..\n    Deleting " .. gnupghome .. "\n" )
								-- Remove usekeyring
								os.execute( "rm -rf " .. gnupghome .. ";" )
							end
						end
						print( "\27[1;31m--  \27[0mSomething Went Wrong( Exit Code " .. EXIT_SIGNAL .. " )..\n    Deleting " .. gnupghome .. "\n" )
						os.execute( "rm -rf " .. gnupghome .. ";" )
					end
					-- Remove GNUPGHOME
					os.execute( "rm -rf " .. tmpdir .. "/gnupg;" )
				else
					-- Nothing to remove ..
					EXIT_SIGNAL = 1
				end
				-- -- Nothing to remove ..
				print( "Exit Code : " .. EXIT_CODE .. "\nExit Status : " .. EXIT_STATUS .. "\nExit Signal :" .. EXIT_SIGNAL .. "\n" )
			else
				-- Path exists, so no Download/Install process..
				EXIT_SIGNAL = 0
			end
			return EXIT_SIGNAL
		end,
		-- Set Correct Environment to Build deb-Packages..
		setKernelPkgEnvironment		= function ( self, base_path )
			local EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL
			-- scripts/package/mkdebian
			-- Set Package Maintainer info
			local path		=  base_path .. "/scripts/package/mkdebian"

			local user 	= "\'s/user=\\${KBUILD_BUILD_USER-\\$(id -nu)}/user=edev1/g\'"
			local email0	= "\'s/^email=\\${DEBEMAIL-$EMAIL}/email=edev1@devuan.org/g\' "
			local email1	= "\'s/email=\"\\$user@\\$buildhost\"/email=edev1@devuan.org/g\'"
			local name	= "\'s/name=\\${DEBFULLNAME-\\$user}/name=edev1/g\'"
			local buildhost	= "\'s/buildhost=\\${KBUILD_BUILD_HOST-\\$(hostname -f 2>\\/dev\\/null || hostname)}/buildhost=Devuan/g\'"

			EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL	= os.execute( "sed -i " .. user .. " " .. path  )
			EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL	= os.execute( "sed -i " .. email0 .. " " .. path  )
			EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL	= os.execute( "sed -i " .. email1 .. " " .. path  )
			EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL	= os.execute( "sed -i " .. name .. " " .. path  )
			EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL	= os.execute( "sed -i " .. buildhost .. " " .. path  )
			-- arch/arm64/Makefile
			-- Set Image Type - Compressed or Uncompressed{ Image.gz || Image }
			-- We will set Umcompressed Image type, for deb-pkg facility...due to problems booting compressed images in arm64
			path =  base_path .. "/arch/arm64/Makefile"

			local kbuild_image	= "\'s/KBUILD_IMAGE\\t:= \\$(boot)\\/Image.gz/KBUILD_IMAGE\\t:= \\$(boot)\\/Image/1\'"
			EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL	= os.execute( "sed -i " .. kbuild_image .. " " .. path  )
			-- default 'all' target
			local all_target	= "\'s/all:\\tImage.gz/all:\\tImage/1\'"
			EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL	= os.execute( "sed -i " .. all_target .. " " .. path  )
		end,
		-- Fix git unstable kernel( mainline )
		-- still unused..
		getgitKernel	= function ( self, base_path, ver )
			-- If path Already exists, adapt building dpkg-deb
			local EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.rename( base_path, base_path )
			if ( EXIT_CODE == true )
			then
				self:setKernelPkgEnvironment( base_path )
			end
		end,
		buildKernel		= function ( self, base_path, board_path, ver, arch, kerneldefconfig, dtb, prefix, Kconfig, patchs, do_patchs, ncores )
			--local path = base_path .. "/Kernels/" .. board_path .. "_" .. self[ ver ].rootpath
			local path = base_path .. "/Kernels/" .. self[ ver ].rootpath

			local EXIT_SIGNAL = self:getVerifyStableKernel( path, ver )
			if ( EXIT_SIGNAL == 0 )
			then
				local  EXIT_CODE, EXIT_STATUS
				-- If there are Kernel patchs to apply, then apply them..
				if ( type( patchs[ ver ] ) == "table" and do_patchs == true )
				then
					print( "\27[1;35m---  Applying Patchs.. !!\27[0m" )
					for k,patch in ipairs( patchs[ ver ] )
					do
						print( "\27[1;35m--   \27[0mApply patch \'" .. patch .. "\' !" )
						EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "cd " .. path .. " && patch -p1 < " .. "../../reference/patchs/kernel/" .. patch )
						if ( EXIT_CODE == true )
						then
							print( "     patch \'" .. patch .. "\', applied with \27[1;32mSUCCESS\27[0m.. !!" )
						else
							print( "     patch \'" .. patch .. "\', was \27[1;31m**not** Applied\27[0m.. !!" )
							print( "Exit Code : " .. EXIT_CODE .. "\nExit Status : " .. EXIT_STATUS .. "\nExit Signal :" .. EXIT_SIGNAL .. "\n" )
							return EXIT_SIGNAL
						end
					end
				else
					print( "\27[1;35m--  \27[1;33mNo Patchs found for " .. board_path .. " or patchs not requested..\27[1;35m!!\27[0m" )
				end

				-- Clean Build Environment first..
				print( "\27[1;35m--- Clean \'" .. self[ ver ].rootpath .. "\' ...!!\27[0m" )
				EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "make -C " .. path .. " ARCH=" .. arch .. " CROSS_COMPILE=" .. prefix .. " clean" )
				if ( EXIT_CODE == true )
				then
					print( "\27[1;35m--  \27[0mClean ended \27[1;32mSUCCESSFULLY\27[0m.. !!" )

					if ( kerneldefconfig ~= nil )
					then
						print( "\27[1;35m--- Copy Board defconfig \'" .. kerneldefconfig .. "\' file.. !!\27[0m" )
						if ( Kconfig == "oldconfig" )
						then
							EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "cp " .. base_path .. "/reference/.config/kernels/" .. kerneldefconfig .. " " .. path .. "/.config" )
						elseif ( Kconfig == "defconfig" or Kconfig == "bindeb-pkg" or Kconfig == "deb-pkg" )
						then
							EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "cp " .. base_path .. "/reference/.config/kernels/" .. kerneldefconfig .. " " .. path .. "/arch/" .. arch .. "/configs/" .. kerneldefconfig )
						elseif ( Kconfig == "menuconfig" )
						then
							-- Config will be created via Kconfig option..
							EXIT_CODE = true
						end
						if ( EXIT_CODE == true )
						then
							print( "\27[1;35m--  \27[0mCopy Board defconfig ended \27[1;32mSUCCESSFULLY\27[0m.. !!" )

							print( "\27[1;35m--- Generate .config file.. \27[0m" )
							-- 'Make oldconfig', 'Make with copied board defconfig' file, or  do a 'Make menuconfig' or even create deb-packages ?
							if ( Kconfig == "oldconfig" )
							then
								EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "make -C " .. path .. " ARCH=" .. arch .. " CROSS_COMPILE=" .. prefix .. " oldconfig" )
							elseif( Kconfig == "defconfig" or Kconfig == "bindeb-pkg" or Kconfig == "deb-pkg" )
							then
								EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "make -C " .. path .. " ARCH=" .. arch .. " CROSS_COMPILE=" .. prefix .. " " .. kerneldefconfig )
							elseif(  Kconfig == "menuconfig" )
							then
								EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "make -C " .. path .. " ARCH=" .. arch .. " CROSS_COMPILE=" .. prefix .. " menuconfig" )
							end
							if ( EXIT_CODE == true )
							then
								print( "\27[1;35m--  \27[0m.config generated \27[1;32mSUCCESSFULLY\27[0m.. !!" )

								print( "\27[1;35m--- Build { Image, dtb, modules } | { deb-packages } .. !!\27[0m" )
								if ( Kconfig == "bindeb-pkg" or Kconfig == "deb-pkg" )
								then
									EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "make -C " .. path .. " ARCH=" .. arch .. " CROSS_COMPILE=" .. prefix .. " " .. Kconfig .. " -j" .. ncores )
								else
									EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "make -C " .. path .. " ARCH=" .. arch .. " CROSS_COMPILE=" .. prefix .. " Image dtbs modules -j" .. ncores )
									if ( EXIT_CODE == true )
									then
										print( "\27[1;35m--  \27[0mBuild { Image, dtbs, modules } terminated \27[1;32mSUCCESSFULLY\27[0m.. !!" )

										-- Install Kernel Image file to Builds Image path
										print( "\27[1;35m--- Install Kernel Image.. !!\27[0m" )
										EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "mkdir -m 755 -pv " .. base_path .. "/Builds/" .. board_path .. "_KImage/boot;cp " .. path .. "/arch/" .. arch .. "/boot/Image " .. base_path .. "/Builds/" .. board_path .. "_KImage/boot/Image" )
										if ( EXIT_CODE == true )
										then
											print( "\27[1;35m--  \27[0mKernel Image instalation in 'Builds/" .. board_path .. "_KImage/boot' terminated \27[1;32mSUCCESSFULLY\27[0m.. !!" )
										else
											print( "\27[1;31m--  \27[0m Something went wrong.. in Kernel Image Installation, \27[1;31mFAILLED\27[0m !!" )
										end
										-- Install dtbs to Build dtbs path
										print( "\27[1;35m--- Install dtbs.. !!\27[0m" )
										EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "mkdir -pv " .. base_path .. "/Builds/" .. board_path .. "_dtbs/boot" )
										if ( EXIT_CODE == true )
										then
											EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "make -C " .. path .. " ARCH=" .. arch .. " CROSS_COMPILE=" .. prefix .. " INSTALL_DTBS_PATH=".. base_path .. "/Builds/" .. board_path .. "_dtbs/boot dtbs_install -j" .. ncores )
											if ( EXIT_CODE == true )
											then
												print( "\27[1;35m--  \27[0mDTBS instalation in 'Builds/" .. board_path .. "_dtbs' terminated \27[1;32mSUCCESSFULLY\27[0m.. !!" )
											else
												print( "\27[1;31m--  \27[0m Something went wrong.. in DTBS Installation, \27[1;31mFAILLED\27[0m !!" )
											end
										end
										-- Install all kernel modules to Build modules path
										print( "\27[1;35m--- Install modules.. !!\27[0m" )
										EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "mkdir -pv " .. base_path .. "/Builds/" .. board_path .. "_modules/lib" )
										if ( EXIT_CODE == true )
										then
											EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "make -C " .. path .. " ARCH=" .. arch .. " CROSS_COMPILE=" .. prefix .. " INSTALL_MOD_PATH=".. base_path .. "/Builds/" .. board_path .. "_modules modules_install -j" .. ncores )
											if ( EXIT_CODE == true )
											then
												print( "\27[1;35m--  \27[0mModules instalation in 'Builds/" .. board_path .. "_modules' terminated \27[1;32mSUCCESSFULLY\27[0m.. !!" )
											else
												print( "\27[1;31m--  \27[0m Something went wrong.. in Modules Installation, \27[1;31mFAILLED\27[0m !!" )
											end
										end
										-- Install sanitised kernel headers to Build headers path
										print( "\27[1;35m--- Install Headers.. !!\27[0m" )
										EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "mkdir -pv " .. base_path .. "/Builds/" .. board_path .. "_headers/usr" )
										if ( EXIT_CODE == true )
										then
											EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "make -C " .. path .. " ARCH=" .. arch .. " CROSS_COMPILE=" .. prefix .. " INSTALL_HDR_PATH=".. base_path .. "/Builds/" .. board_path .. "_headers/usr headers_install -j" .. ncores )
											if ( EXIT_CODE == true )
											then
												print( "\27[1;35m--  \27[0mHeaders instalation in 'Builds/" .. board_path .. "_headers' terminated \27[1;32mSUCCESSFULLY\27[0m.. !!" )
											else
												print( "\27[1;31m--  \27[0m Something went wrong.. in Headers Installation, \27[1;31mFAILLED\27[0m !!" )
											end
										end
									end
								end
								-- Finally, clean the Build Environment ..
								print( "\27[1;35m--- Clean \'" .. self[ ver ].rootpath .. "\' ...!!\27[0m" )
								EXIT_CODE, EXIT_STATUS, EXIT_SIGNAL = os.execute( "make -C " .. path .. " ARCH=" .. arch .. " CROSS_COMPILE=" .. prefix .. " clean" )
								if ( EXIT_CODE == true )
								then
									print( "\27[1;35m--  \27[0mClean ended \27[1;32mSUCCESSFULLY\27[0m.. !!" )
								else
									print( "\27[1;31m--  \27[0m Something went wrong.. in Environment Cleaning, \27[1;31mFAILLED\27[0m !!" )
								end
							else
								print( "\27[1;31m--  \27[0m Something went wrong.. in .config Generation, or wrong Board defconfig copying, \27[1;31mFAILLED\27[0m !!" )
							end
						end
					end
				end
				if ( EXIT_CODE ~= true )
				then
					print( "Exit Code : " .. EXIT_CODE .. "\nExit Status : " .. EXIT_STATUS .. "\nExit Signal :" .. EXIT_SIGNAL .. "\n" )
				end
			end
			return EXIT_SIGNAL
		end
	}
}
return kernels
