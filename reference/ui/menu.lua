-- Description for Menu Module
local menu = {
	newt	= require('lnewt'),
	roottext= "Embedded Devuan Builder",
	title	= "ed3v1 Build System",
	menustr	= nil,
	--InitUi		= function()
	--	--self.newt = require('lnewt')
	--	--self.newt.Init()
	--	--self.newt.Cls()
	--end,

	-- Main Menu
	-- Show Root Screen options.. not in use right now, used directly bellow in the code..
	showroottext	= function( self, cols, rows, message )
		self.newt.DrawRootText( ( cols - #message ) / 2, rows / 8, message )
		--self.newt.Refresh()
		self.newt.PushHelpLine(nil)
	end,
	-- Show Arch Option
	showarchs	= function( self, entries )
		menustr1	= "Welcome to Dev1 ARCHs Selection.."
		menustr2	= "Pls choose Architecture :"
		local menuitems = {}
		local hotkey	= {}
		local width	= 0
		for key, value in ipairs( entries ) do
			if( key <= 12 )
			then
				value = string.format( "[F%d] %s", key, value )
				table.insert( hotkey, self.newt[ 'KEY_F' .. key ] )
			end
			if( width < #value )
			then
				width = #value
			end
			menuitems[ key ] = value
		end
		self.newt.Init()
		self.newt.Cls()

		local cols, rows = self.newt.GetScreenSize()
		local height = #menuitems
		if( #menuitems > ( rows - 12 ) )
		then
			height = rows - 12
		end
		-- RootLabel will start at beguinint of the Centered Window,
		-- In the first col of the Window ( ( cols / 2 ) - hald window cols( -30 ) - start at beguininf first char( -1 ) )
		self.newt.DrawRootText( ( cols / 2 ) - 30 - 1, rows / 8, self.roottext )
		self.newt.PushHelpLine( "<Tab> Navigate between Components | [ ←,↑,↓,→ ] Select Entries or Buttons | <[F1 - F11]> Selects ListBox Entries, jump to next screen | <Select> next Screen | <BoBack> previous Screen" )

		self.newt.CenteredWindow( 60, 20, "3dev1 Builder" )
		local form	= self.newt.Form()

		-- Initial Label on Centered Window
		local label	= {
			self.newt.Label( 2, 1, menustr1 ),
			self.newt.Label( 2, 2, menustr2 )
		}

		-- List with Options
		-- Composed by HotKey and Label per each entry
		local flags	= self.newt.FLAG_RETURNEXIT
		if( height < #menuitems )
		then
			flags	= flags + self.newt.FLAG_SCROLL
		end
		local list	= self.newt.Listbox( 26, 4, height, flags )
		list:AppendEntry( menuitems )

		-- Buttons at the end of the Window
		button = {
			self.newt.Button( 16, 16, "[Select]" ),
			self.newt.Button( 34, 16, "[GoBack]" )
		}
		form:AddComponents( label, list, button )
		form:AddHotKey( hotkey )

		local reason, value = form:Run()
		local sel = nil
		---- Treat Keyboard Events

		if( reason == self.newt.EXIT_COMPONENT )
		then
			-- If <Enter> key was pressed when the selected Component was a ListBox
			if( value and value:ID() == list:ID() )
			then
				sel = list:GetCurrent()
			-- If [Select] Button is pressed
			elseif( value and value:Text() == '[Select]' )
			then
				sel = list:GetCurrent()
			-- If [GoBack] Button is pressed
			elseif( value and value:Text() == '[GoBack]' )
			then
				sel = "GoBack"
			end
		elseif( reason == self.newt.EXIT_HOTKEY )
		then
			-- if Escape kEY was pressed
			if( value == self.newt.KEY_ESCAPE )
			then
				sel = "GoBack"
			else
				-- if anny HotKey KEY was pressed other than <ESC>
				sel = value - self.newt.KEY_F1 + 1
			end
		end
		-- destroy form
		form:Destroy()
		-- Pop the windows created above..
		self.newt.PopWindow()
		--print( menuitems[ sel ] )
		--print( sel )
		self.newt.Finished()
		return sel
	end,
	-- Choose Vendor Option
	showvendors	= function( self, arch, entries )
		menustr1	= "Welcome to Dev1 \039" .. arch .. "\039 Manufacturer Selection.."
		menustr2	= "Pls choose Manufacturer :"
		--"  Welcome to Dev1 \039" .. ARCHS[ OPTION0 ] .. "\039 Manufacturer Selection..\n  Pls choose Manufacturer :"
		local menuitems = {}
		local hotkey	= {}
		local width	= 0
		for key, value in ipairs( entries ) do
			if( key <= 12 )
			then
				value = string.format( "[F%d] %s", key, value )
				table.insert( hotkey, self.newt[ 'KEY_F' .. key ] )
			end
			if( width < #value )
			then
				width = #value
			end
			menuitems[ key ] = value
		end
		self.newt.Init()
		self.newt.Cls()

		local cols, rows = self.newt.GetScreenSize()
		local height = #menuitems
		if( #menuitems > ( rows - 12 ) )
		then
			height = rows - 12
		end
		-- RootLabel will start at beguinint of the Centered Window,
		-- In the first col of the Window ( ( cols / 2 ) - hald window cols( -30 ) - start at beguininf first char( -1 ) )
		self.newt.DrawRootText( ( cols / 2 ) - 30 - 1, rows / 8, self.roottext )
		self.newt.PushHelpLine( "<Tab> Navigate between Components | [ ←,↑,↓,→ ] Select Entries or Buttons | <[F1 - F11]> Selects ListBox Entries, jump to next screen | <Select> next Screen | <BoBack> previous Screen" )

		self.newt.CenteredWindow( 60, 20, "3dev1 Builder" )
		local form	= self.newt.Form()

		-- Initial Label on Centered Window
		local label	= {
			self.newt.Label( 2, 1, menustr1 ),
			self.newt.Label( 2, 2, menustr2 )
		}

		-- List with Options
		-- Composed by HotKey and Label per each entry
		local flags	= self.newt.FLAG_RETURNEXIT
		if height < #menuitems
		then
			flags	= flags + self.newt.FLAG_SCROLL
		end
		local list = self.newt.Listbox( 26, 4, height, flags )
		list:AppendEntry( menuitems )

		-- Buttons at the end of the Window
		button = {
			self.newt.Button( 16, 16, "[Select]" ),
			self.newt.Button( 34, 16, "[GoBack]" )
		}
		form:AddComponents( label, list, button )
		form:AddHotKey( hotkey )

		local reason, value = form:Run()
		local sel = nil
		---- Treat Keyboard Events
		if( reason == self.newt.EXIT_COMPONENT  )
		then
			-- If <Enter> key was pressed when the selected Component was a ListBox
			if( value and value:ID() == list:ID() )
			then
				sel = list:GetCurrent()
			-- If [Select] Button is pressed
			elseif( value and value:Text() == '[Select]' )
			then
				sel = list:GetCurrent()
			-- If [GoBack] Button is pressed
			elseif( value and value:Text() == '[GoBack]' )
			then
				sel = "GoBack"
			end
		elseif( reason == self.newt.EXIT_HOTKEY )
		then
			-- if Escape kEY was pressed
			if( value == self.newt.KEY_ESCAPE )
			then
				sel = "GoBack"
			else
				-- if anny HotKey KEY was pressed other than <ESC>
				sel = value - self.newt.KEY_F1 + 1
			end
		end
		-- destroy form
		form:Destroy()
		-- Pop the windows created above..
		self.newt.PopWindow()
		--print( menuitems[ sel ] )
		--print( sel )
		self.newt.Finished()
		return sel
	end,
	-- Choose CPU option
	showcpus	= function( self, arch, vendor, entries )
		menustr1 = "Welcome to Dev1 \039" .. arch .. "\039 \039" .. vendor .. "\039 Selection..\n"
		menustr2 = "Pls choose CPU :"
		local menuitems = {}
		local hotkey	= {}
		local width	= 0
		for key, value in ipairs( entries ) do
			if( key <= 12 )
			then
				value = string.format( "[F%d] %s", key, value )
				table.insert( hotkey, self.newt[ 'KEY_F' .. key ] )
			end
			if( width < #value )
			then
				width = #value
			end
			menuitems[ key ] = value
		end
		self.newt.Init()
		self.newt.Cls()

		local cols, rows = self.newt.GetScreenSize()
		local height = #menuitems
		if( #menuitems > ( rows - 12 ) )
		then
			height = rows - 12
		end
		-- RootLabel will start at beguinint of the Centered Window,
		-- In the first col of the Window ( ( cols / 2 ) - hald window cols( -30 ) - start at beguininf first char( -1 ) )
		self.newt.DrawRootText( ( cols / 2 ) - 30 - 1, rows / 8, self.roottext )
		self.newt.PushHelpLine( "<Tab> Navigate between Components | [ ←,↑,↓,→ ] Select Entries or Buttons | <[F1 - F11]> Selects ListBox Entries, jump to next screen | <Select> next Screen | <BoBack> previous Screen" )

		self.newt.CenteredWindow( 60, 20, "3dev1 Builder" )
		local form	= self.newt.Form()

		-- Initial Label on Centered Window
		local label	= {
			self.newt.Label( 2, 1, menustr1 ),
			self.newt.Label( 2, 2, menustr2 )
		}

		-- List with Options
		-- Composed by HotKey and Label per each entry
		local flags	= self.newt.FLAG_RETURNEXIT
		if height < #menuitems
		then
			flags	= flags + self.newt.FLAG_SCROLL
		end
		local list = self.newt.Listbox( 26, 4, height, flags )
		list:AppendEntry( menuitems )

		-- Buttons at the end of the Window
		button = {
			self.newt.Button( 16, 16, "[Select]" ),
			self.newt.Button( 34, 16, "[GoBack]" )
		}
		form:AddComponents( label, list, button )
		form:AddHotKey( hotkey )

		local reason, value = form:Run()
		local sel = nil
		---- Treat Keyboard Events
		if( reason == self.newt.EXIT_COMPONENT  )
		then
			-- If <Enter> key was pressed when the selected Component was a ListBox
			if( value and value:ID() == list:ID() )
			then
				sel = list:GetCurrent()
			-- If [Select] Button is pressed
			elseif( value and value:Text() == '[Select]' )
			then
				sel = list:GetCurrent()
			-- If [GoBack] Button is pressed
			elseif( value and value:Text() == '[GoBack]' )
			then
				sel = "GoBack"
			end
		elseif( reason == self.newt.EXIT_HOTKEY )
		then
			-- if Escape kEY was pressed
			if( value == self.newt.KEY_ESCAPE )
			then
				sel = "GoBack"
			else
				-- if anny HotKey KEY was pressed other than <ESC>
				sel = value - self.newt.KEY_F1 + 1
			end
		end
		-- destroy form
		form:Destroy()
		-- Pop the windows created above..
		self.newt.PopWindow()
		--print( menuitems[ sel ] )
		--print( sel )
		self.newt.Finished()
		return sel
	end,
	-- Choose Board Option
	showboard	= function( self, cpu, entries )
		menustr1	= "Welcome to Dev1 \039" .. cpu .. "\039 Selection.."
		menustr2	= "Pls choose Board :"
		
		local menuitems = {}
		local hotkey	= {}
		local width	= 0
		for key, value in ipairs( entries ) do
			if( key <= 12 )
			then
				value = string.format( "[F%d] %s", key, value )
				table.insert( hotkey, self.newt[ 'KEY_F' .. key ] )
			end
			if( width < #value )
			then
				width = #value
			end
			menuitems[ key ] = value
		end
		self.newt.Init()
		self.newt.Cls()

		local cols, rows = self.newt.GetScreenSize()
		local height = #menuitems
		if( #menuitems > ( rows - 12 ) )
		then
			height = rows - 12
		end
		-- RootLabel will start at beguinint of the Centered Window,
		-- In the first col of the Window ( ( cols / 2 ) - hald window cols( -30 ) - start at beguining in first char( -1 ) )
		self.newt.DrawRootText( ( cols / 2 ) - 30 - 1, rows / 8, self.roottext )
		self.newt.PushHelpLine( "<Tab> Navigate between Components | [ ←,↑,↓,→ ] Select Entries or Buttons | <[F1 - F11]> Selects ListBox Entries, jump to next screen | <Select> next Screen | <BoBack> previous Screen" )

		self.newt.CenteredWindow( 60, 20, "3dev1 Builder" )
		local form	= self.newt.Form()

		-- Initial Label on Centered Window
		local label	= {
			self.newt.Label( 2, 1, menustr1 ),
			self.newt.Label( 2, 2, menustr2 )
		}

		-- List with Options
		-- Composed by HotKey and Label per each entry
		local flags	= self.newt.FLAG_RETURNEXIT
		if height < #menuitems
		then
			flags	= flags + self.newt.FLAG_SCROLL
		end
		local list = self.newt.Listbox( 26, 4, height, flags )
		list:AppendEntry( menuitems )

		-- Buttons at the end of the Window
		button = {
			self.newt.Button( 16, 16, "[Select]" ),
			self.newt.Button( 34, 16, "[GoBack]" )
		}
		form:AddComponents( label, list, button )
		form:AddHotKey( hotkey )

		local reason, value = form:Run()
		local sel = nil
		---- Treat Keyboard Events
		if( reason == self.newt.EXIT_COMPONENT  )
		then
			-- If <Enter> key was pressed when the selected Component was a ListBox
			if( value and value:ID() == list:ID() )
			then
				sel = list:GetCurrent()
			-- If [Select] Button is pressed
			elseif( value and value:Text() == '[Select]' )
			then
				sel = list:GetCurrent()
			-- If [GoBack] Button is pressed
			elseif( value and value:Text() == '[GoBack]' )
			then
				sel = "GoBack"
			end
		elseif( reason == self.newt.EXIT_HOTKEY )
		then
			-- if Escape kEY was pressed
			if( value == self.newt.KEY_ESCAPE )
			then
				sel = "GoBack"
			else
				-- if anny HotKey KEY was pressed other than <ESC>
				sel = value - self.newt.KEY_F1 + 1
			end
		end
		-- destroy form
		form:Destroy()
		-- Pop the windows created above..
		self.newt.PopWindow()
		--print( menuitems[ sel ] )
		--print( sel )
		self.newt.Finished()
		return sel
	end,
	-- Software Stack Options for any Board
	showsoftstack	= function( self, board, entries )
		menustr	= "Pls select \039" .. board .. "\039 build options:"

		local menuitems = {}
		local hotkey	= {}
		local width	= 0
		for key, value in ipairs( entries ) do
			if( key <= 12 )
			then
				value = string.format( "[F%d] %s", key, value )
				table.insert( hotkey, self.newt[ 'KEY_F' .. key ] )
			end
			if( width < #value )
			then
				width = #value
			end
			menuitems[ key ] = value
		end
		self.newt.Init()
		self.newt.Cls()

		local cols, rows = self.newt.GetScreenSize()
		local height = #menuitems
		if( #menuitems > ( rows - 12 ) )
		then
			height = rows - 12
		end
		-- RootLabel will start at beguinint of the Centered Window,
		-- In the first col of the Window ( ( cols / 2 ) - hald window cols( -30 ) - start at beguininf first char( -1 ) )
		self.newt.DrawRootText( ( cols / 2 ) - 30 - 1, rows / 8, self.roottext )
		self.newt.PushHelpLine( "<Tab> Navigate between Components | [ ←,↑,↓,→ ] Select Entries or Buttons | <[F1 - F11]> Selects ListBox Entries, jump to next screen | <Select> next Screen | <BoBack> previous Screen" )

		self.newt.CenteredWindow( 60, 20, "3dev1 Builder" )
		local form	= self.newt.Form()

		-- Initial Label on Centered Window
		local label	= self.newt.Label( 2, 1, menustr )

		-- List with Options
		-- Composed by HotKey and Label per each entry
		local flags	= self.newt.FLAG_RETURNEXIT
		if height < #menuitems
		then
			flags	= flags + self.newt.FLAG_SCROLL
		end
		local list = self.newt.Listbox( 5, 3, height, flags )
		list:AppendEntry( menuitems )

		-- Buttons at the end of the Window
		button = {
			self.newt.Button( 16, 16, "[Select]" ),
			self.newt.Button( 34, 16, "[GoBack]" )
		}
		form:AddComponents( label, list, button )
		form:AddHotKey( hotkey )

		local reason, value = form:Run()
		local sel = nil
		---- Treat Keyboard Events
		if( reason == self.newt.EXIT_COMPONENT  )
		then
			-- If <Enter> key was pressed when the selected Component was a ListBox
			if( value and value:ID() == list:ID() )
			then
				sel = list:GetCurrent()
			-- If [Select] Button is pressed
			elseif( value and value:Text() == '[Select]' )
			then
				sel = list:GetCurrent()
			-- If [GoBack] Button is pressed
			elseif( value and value:Text() == '[GoBack]' )
			then
				sel = "GoBack"
			end
		elseif( reason == self.newt.EXIT_HOTKEY )
		then
			-- if Escape kEY was pressed
			if( value == self.newt.KEY_ESCAPE )
			then
				sel = "GoBack"
			else
				-- if anny HotKey KEY was pressed other than <ESC>
				sel = value - self.newt.KEY_F1 + 1
			end
		end
		-- destroy form
		form:Destroy()
		-- Pop the windows created above..
		self.newt.PopWindow()
		--print( menuitems[ sel ] )
		--print( sel )
		self.newt.Finished()
		return sel
	end,
	-- SoftWare Stack Sub Menus
	-- RootFS Options
	showrootfs_options	= function( self, board, entries )
		menustr	= "Pls select \039" .. board .. "\039 build type:"

		local menuitems = {}
		local hotkey	= {}
		local width	= 0
		for key, value in ipairs( entries ) do
			if( key <= 12 )
			then
				value = string.format( "[F%d] %s", key, value )
				table.insert( hotkey, self.newt[ 'KEY_F' .. key ] )
			end
			if( width < #value )
			then
				width = #value
			end
			menuitems[ key ] = value
		end
		self.newt.Init()
		self.newt.Cls()

		local cols, rows = self.newt.GetScreenSize()
		local height = #menuitems
		if( #menuitems > ( rows - 12 ) )
		then
			height = rows - 12
		end
		-- RootLabel will start at beguinint of the Centered Window,
		-- In the first col of the Window ( ( cols / 2 ) - hald window cols( -30 ) - start at beguininf first char( -1 ) )
		self.newt.DrawRootText( ( cols / 2 ) - 30 - 1, rows / 8, self.roottext )
		self.newt.PushHelpLine( "<Tab> Navigate between Components | [ ←,↑,↓,→ ] Select Entries or Buttons | <[F1 - F11]> Selects ListBox Entries, jump to next screen | <Select> next Screen | <BoBack> previous Screen" )

		self.newt.CenteredWindow( 60, 20, "3dev1 Builder" )
		local form	= self.newt.Form()

		-- Initial Label on Centered Window
		local label	= self.newt.Label( 2, 1, menustr )

		-- List with Options
		-- Composed by HotKey and Label per each entry
		local flags	= self.newt.FLAG_RETURNEXIT
		if height < #menuitems
		then
			flags	= flags + self.newt.FLAG_SCROLL
		end
		local list	= self.newt.Listbox( 23, 3, height, flags )
		list:AppendEntry( menuitems )

		-- Buttons at the end of the Window
		button = {
			self.newt.Button( 16, 16, "[Select]" ),
			self.newt.Button( 34, 16, "[GoBack]" )
		}
		form:AddComponents( label, list, button )
		form:AddHotKey( hotkey )

		local reason, value = form:Run()
		local sel = nil
		---- Treat Keyboard Events
		if( reason == self.newt.EXIT_COMPONENT  )
		then
			-- If <Enter> key was pressed when the selected Component was a ListBox
			if( value and value:ID() == list:ID() )
			then
				sel = list:GetCurrent()
			-- If [Select] Button is pressed
			elseif( value and value:Text() == '[Select]' )
			then
				sel = list:GetCurrent()
			-- If [GoBack] Button is pressed
			elseif( value and value:Text() == '[GoBack]' )
			then
				sel = "GoBack"
			end
		elseif( reason == self.newt.EXIT_HOTKEY )
		then
			-- if Escape kEY was pressed
			if( value == self.newt.KEY_ESCAPE )
			then
				sel = "GoBack"
			else
				-- if anny HotKey KEY was pressed other than <ESC>
				sel = value - self.newt.KEY_F1 + 1
			end
		end
		-- destroy form
		form:Destroy()
		-- Pop the windows created above..
		self.newt.PopWindow()

		self.newt.Finished()
		return sel
	end,
	-- Bootloader Build Options
	showbootloader_options	= function( self, board, patchs, build_opts )
		menustr	= "Pls select \039" .. board .. "\039 BootLoader patchs/build options.."

		local width	= 0

		self.newt.Init()
		self.newt.Cls()

		local cols, rows = self.newt.GetScreenSize()
		-- Determine if necessary size is in place
		local height = #build_opts
		if( #build_opts > ( rows - 12 ) )
		then
			height = rows - 12
		end
		-- RootLabel will start at beguinint of the Centered Window,
		-- In the first col of the Window ( ( cols / 2 ) - hald window cols( -30 ) - start at beguining...so..first char( -1 ) )
		self.newt.DrawRootText( ( cols / 2 ) - 30 - 1, rows / 8, self.roottext )
		self.newt.PushHelpLine( "<Tab> Navigate between Components | [ ←,↑,↓,→ ] Navigate between Entries or Buttons | <Space> Selects Entries | <Select> next Screen | <BoBack> previous Screen" )

		self.newt.CenteredWindow( 60, 20, "3dev1 Builder" )
		local form	= self.newt.Form()

		-- Initial Label on Centered Window
		local label	= self.newt.Label( 2, 1, menustr )

		-- List with Options
		-- Composed by HotKey and Label per each entry
		local flags	= self.newt.FLAG_RETURNEXIT
		if height < 6
		then
			flags	= flags + self.newt.FLAG_SCROLL
		end

		-- Label for Patch options
		local patch_label = self.newt.Label( 6, 4, "Apply Patchs's:" )		
		-- Patchs Radiobutton options
		local patch	= {}
		patch[ 1 ]	= self.newt.Radiobutton( 8, 6, patchs[ 1 ], true )
		patch[ 2 ]	= self.newt.Radiobutton( 8, 7, patchs[ 2 ], false, patch[ 1 ] )

		-- Label for Kerne build options
		local build_label = self.newt.Label( 26, 4, "Build Option:" )
		-- Bootloader Build Radiobutton options	
		local build_opt = {}
		build_opt[ 1 ]	= self.newt.Radiobutton( 28, 6, build_opts[ 1 ], false )
		build_opt[ 2 ]	= self.newt.Radiobutton( 28, 7, build_opts[ 2 ], true, build_opt[ 1 ] )
		build_opt[ 3 ]	= self.newt.Radiobutton( 28, 8, build_opts[ 3 ], false, build_opt[ 2 ] )

		-- Buttons at the end of the Window
		local buttons = {
			self.newt.Button( 16, 16, "[Select]" ),
			self.newt.Button( 34, 16, "[GoBack]" )
		}
		form:AddComponents( label, patch_label, patch, build_label, build_opt, buttons )
		--form:AddHotKey( hotkey )

		local reason, value	= form:Run()
		local sel		= {}

		-- Get Selected RadioButtons..
		local values		= {}
		values[ 1 ]		= patch[ 1 ]:GetCurrent():Text()
		values[ 2 ]		= build_opt[ 1 ]:GetCurrent():Text()

		-- Treat Keyboard Events
		if( reason == self.newt.EXIT_COMPONENT )
		then
			-- If [Select] Button is pressed
			if( value and value:Text() == '[Select]' )
			then
				sel	= values
			-- If [GoBack] Button is pressed
			elseif( value and value:Text() == '[GoBack]' )
			then
				sel	= { "GoBack", "GoBack" }
			end
		elseif( reason == self.newt.EXIT_HOTKEY )
		then
			-- if Escape kEY was pressed
			if( value == self.newt.KEY_ESCAPE )
			then
				sel	= { "GoBack", "GoBack" }
			end
		end
		-- destroy form
		form:Destroy()
		-- Pop the windows created above..
		self.newt.PopWindow()

		self.newt.Finished()
		return sel
		--return values
	end,
	-- Kernel Building Options, including patching..
	showkernel_m_dtbs_options = function( self, board, patchs, build_opts )
		menustr	= "Pls select \039" .. board .. "\039 Kernel patchs/build options.."

		local width	= 0

		self.newt.Init()
		self.newt.Cls()

		local cols, rows = self.newt.GetScreenSize()
		-- Determine if necessary size is in place
		local height = #build_opts
		if( #build_opts > ( rows - 12 ) )
		then
			height = rows - 12
		end
		-- RootLabel will start at beguinint of the Centered Window,
		-- In the first col of the Window ( ( cols / 2 ) - hald window cols( -30 ) - start at beguining...so..first char( -1 ) )
		self.newt.DrawRootText( ( cols / 2 ) - 30 - 1, rows / 8, self.roottext )
		self.newt.PushHelpLine( "<Tab> Navigate between Components | [ ←,↑,↓,→ ] Navigate between Entries or Buttons | <Space> Selects Entries | <Select> next Screen | <BoBack> previous Screen" )

		self.newt.CenteredWindow( 60, 20, "3dev1 Builder" )
		local form	= self.newt.Form()

		-- Initial Label on Centered Window
		local label	= self.newt.Label( 2, 1, menustr )

		-- List with Options
		-- Composed by HotKey and Label per each entry
		local flags	= self.newt.FLAG_RETURNEXIT
		if height < 6
		then
			flags	= flags + self.newt.FLAG_SCROLL
		end

		-- Label for Patch options
		local patch_label = self.newt.Label( 6, 4, "Apply Patchs's:" )		
		-- Patchs Radiobutton options
		local patch	= {}
		patch[ 1 ]	= self.newt.Radiobutton( 8, 6, patchs[ 1 ], true )
		patch[ 2 ]	= self.newt.Radiobutton( 8, 7, patchs[ 2 ], false, patch[ 1 ] )

		-- Label for Kernel build options
		local build_label = self.newt.Label( 26, 4, "Build Option:" )
		-- Kernel Build Radiobutton options	
		local build_opt = {}
		build_opt[ 1 ]	= self.newt.Radiobutton( 28, 6, build_opts[ 1 ], false )
		build_opt[ 2 ]	= self.newt.Radiobutton( 28, 7, build_opts[ 2 ], false, build_opt[ 1 ] )
		build_opt[ 3 ]	= self.newt.Radiobutton( 28, 8, build_opts[ 3 ], false, build_opt[ 2 ] )
		build_opt[ 4 ]	= self.newt.Radiobutton( 28, 9, build_opts[ 4 ], true, build_opt[ 3 ] )
		build_opt[ 5 ]	= self.newt.Radiobutton( 28, 10, build_opts[ 5 ], false, build_opt[ 4 ] )

		-- Buttons at the end of the Window
		local buttons = {
			self.newt.Button( 16, 16, "[Select]" ),
			self.newt.Button( 34, 16, "[GoBack]" )
		}
		form:AddComponents( label, patch_label, patch, build_label, build_opt, buttons )
		--form:AddHotKey( hotkey )

		local reason, value	= form:Run()
		local sel		= {}

		-- Get Selected RadioButtons..
		local values		= {}
		values[ 1 ]		= patch[ 1 ]:GetCurrent():Text()
		values[ 2 ]		= build_opt[ 1 ]:GetCurrent():Text()

		-- Treat Keyboard Events
		if( reason == self.newt.EXIT_COMPONENT )
		then
			-- If [Select] Button is pressed
			if( value and value:Text() == '[Select]' )
			then
				sel	= values
			-- If [GoBack] Button is pressed
			elseif( value and value:Text() == '[GoBack]' )
			then
				sel	= { "GoBack", "GoBack" }
			end
		elseif( reason == self.newt.EXIT_HOTKEY )
		then
			-- if Escape kEY was pressed
			if( value == self.newt.KEY_ESCAPE )
			then
				sel	= { "GoBack", "GoBack" }
			end
		end
		-- destroy form
		form:Destroy()
		-- Pop the windows created above..
		self.newt.PopWindow()

		self.newt.Finished()
		return sel
		--return values
	end,
	-- WriteMedia
	-- TODO: Implement UI to write media [F6]
	-- it will have a  list with results of 'lsblk' command
	-- a entry field to choose the block device to use
	showmediaCard_options = function( self, board )
		menustr	= "Pls select \039" .. board .. "\039 MediaCard options.."

		local width	= 0

		self.newt.Init()
		self.newt.Cls()

		local cols, rows = self.newt.GetScreenSize()
		-- Determine if necessary size is in place
		--local height = #build_opts
		--if( #build_opts > ( rows - 12 ) )
		--then
		--	height = rows - 12
		--end

		-- RootLabel will start at beguinint of the Centered Window,
		-- In the first col of the Window ( ( cols / 2 ) - hald window cols( -30 ) - start at beguining...so..first char( -1 ) )
		self.newt.DrawRootText( ( cols / 2 ) - 30 - 1, rows / 8, self.roottext )
		self.newt.PushHelpLine( "<Tab> Navigate between Components | [ ←,↑,↓,→ ] Navigate between Entries or Buttons | <Space> Selects Entries | <Select> next Screen | <BoBack> previous Screen" )

		self.newt.CenteredWindow( 60, 20, "3dev1 Builder" )
		local form	= self.newt.Form()

		-- Initial Label on Centered Window
		local label	= self.newt.Label( 2, 1, menustr )

		-- List with Options
		-- Composed by HotKey and Label per each entry
		local flags	= self.newt.FLAG_RETURNEXIT
		--if height < 6
		--then
		--	flags	= flags + self.newt.FLAG_SCROLL
		--end

		-- Label for ListBlock's options
		local block_label = self.newt.Label( 2, 5, "Block Device( sd{a,b,c} etc ):" )		
		-- ListBlock's Label Options
		local block_opt = self.newt.Entry( 3, 10, "/dev/", 10 )

		-- Label for Card Options
		local card_label = self.newt.Label( 35, 5, "Card Option:" )
		-- Cards's Label Operations	
		local card_opt = {}
		card_opt[ 1 ]	= self.newt.Checkbox( 35, 8, "Generate boot.cmd" )
		card_opt[ 2 ]	= self.newt.Checkbox( 35, 9, "Erase BootLoader" )
		card_opt[ 3 ]	= self.newt.Checkbox( 35, 10, "Write BootLoader" )
		card_opt[ 4 ]	= self.newt.Checkbox( 35, 11, "Partition Table" )
		card_opt[ 5 ]	= self.newt.Checkbox( 35, 12, "Format FS")
		card_opt[ 6 ]	= self.newt.Checkbox( 35, 13, "Create OS" )

		-- Buttons at the end of the Window
		local buttons = {
			self.newt.Button( 16, 16, "[Select]" ),
			self.newt.Button( 34, 16, "[GoBack]" )
		}
		form:AddComponents( label, block_label, block_opt, card_label, card_opt, buttons )
		--form:AddHotKey( hotkey )

		local reason, value	= form:Run()
		local sel		= {}

		-- Get Selected RadioButtons..
		local values		= {}
		values[ 1 ]		= nil
		values[ 2 ]		= block_opt:GetValue()
		values[ 3 ]		= card_opt[ 1 ]:GetValue()
		values[ 4 ]		= card_opt[ 2 ]:GetValue()
		values[ 5 ]		= card_opt[ 3 ]:GetValue()
		values[ 6 ]		= card_opt[ 4 ]:GetValue()
		values[ 7 ]		= card_opt[ 5 ]:GetValue()
		values[ 8 ]		= card_opt[ 6 ]:GetValue()

		-- Treat Keyboard Events
		if( reason == self.newt.EXIT_COMPONENT )
		then
			-- If [Select] Button is pressed
			if( value and value:Text() == '[Select]' )
			then
				values[ 1 ]	= "Select"
				sel		= values
			-- If [GoBack] Button is pressed
			elseif( value and value:Text() == '[GoBack]' )
			then
				sel	= { "GoBack" }
			end
		elseif( reason == self.newt.EXIT_HOTKEY )
		then
			-- if Escape kEY was pressed
			if( value == self.newt.KEY_ESCAPE )
			then
				sel	= { "GoBack" }
			end
		end
		-- destroy form
		form:Destroy()
		-- Pop the windows created above..
		self.newt.PopWindow()

		self.newt.Finished()
		return sel
		--return values
	end,
}
return menu
