-- Image Lua Module
mediadisk = {
	os		= require "reference.os.base",
	-- Generate boot.cmd script file
	genBootcmd	= function( self, pwd, board_name, bootcmd  )
		print( "--- GenerateBoot.cmd" )
		local filename	= pwd .. "/Builds/" .. board_name .. "/" .. board_name .. "_" .. "boot.cmd"
		local nrlines	= 0
		-- Verify if table cmd, has content..
		if ( bootcmd[ 1 ] ~= nil )
		then
			-- Write content to file..
			local handler = io.open( filename, "w+")
			for key,value in ipairs( bootcmd )
			do
				handler:write( value )
				nrlines = nrlines + 1
			end
			handler:flush()
			handler:close()
			print( "--  Writen " .. nrlines .. " lines to file: " .. filename )
		else
			print( "--  No \'boot.cmd\' data to write.." )
		end
	end,
	-- Clean disk, from sector seek till last Sector of bootloader..
	eraseBLoader	= function( self, disk, bootloaders )
		print( "--- EraseBLoader" )
		for key,value in ipairs( bootloaders )
		do
			-- check if value is a table
			if( type( value ) == "table" )
			then
				print( "--  dd if=/dev/zero of=" .. disk .. " bs=" .. value.ssize .. " seek=" .. value.sseek .. " count=" .. value.scount .. " conv=" .. value.conv )
				self.os:shell( "dd if=/dev/zero of=" .. disk .. " bs=" .. value.ssize .. " seek=" .. value.sseek .. " count=" .. value.scount .. " conv=" .. value.conv )
			end
			
		end
	end,
	-- Write bootLoader to disk
	writeBLoader	= function( self, pwd, board_name, disk, bootloaders, images )
		-- If there are no bootloader section on disk, do nothing..
		if( #bootloaders ~= 0 )
		then
			print( "--- writeBLoader" )
			for key,value in ipairs( images )
			do
					print( "--  dd if=" .. pwd .. "/Builds/" .. board_name .. "/" .. board_name .. "_" .. images[ key ] .. " of=" .. disk .. " bs=" .. bootloaders[ key ].ssize .. " seek=" .. bootloaders[ key ].sseek .. " conv=" .. bootloaders[ key ].conv )
					self.os:shell( "dd if=" .. pwd .. "/Builds/" .. board_name .. "/" .. board_name .. "_" .. images[ key ] .. " of=" .. disk .. " bs=" .. bootloaders[ key ].ssize .. " seek=" .. bootloaders[ key ].sseek .. " conv=" .. bootloaders[ key ].conv )	
			end
		end
	end,
	-- Write Partition to disk
	partDisk	= function( self, disk, partitions )
		print( "--- partDisk" )
		print( "--  parted " .. disk .. partitions )
		self.os:shell( "parted " .. disk .. partitions )
	end,
	-- Format Partitions
	formatFS	= function( self, disk, formatfs )
		print( "--- formatFS" )
		-- ie: "yes | mkfs.ext2 /dev/sdb1" 
		for key,value in pairs( formatfs )
		do
			print("--  sleep 3 && yes | mkfs." .. key .. " " .. disk .. value )
			self.os:shell( "sleep 3 && yes | mkfs." .. key .. " " .. disk .. value )
		end
	end,
	-- Explore the possibility of this funtion being created here..
	-- List Block Devices
	--lsblkDev	= function( self )
	--	print( "--- formatFS" )
	--	-- ie: "yes | mkfs.ext2 /dev/sdb1" 
	--	for key,value in pairs( formatfs )
	--	do
	--		print("--  sleep 3 && yes | mkfs." .. key .. " " .. disk .. value )
	--		self.os:shell( "sleep 3 && yes | mkfs." .. key .. " " .. disk .. value )
	--	end
	--end
}
return mediadisk
